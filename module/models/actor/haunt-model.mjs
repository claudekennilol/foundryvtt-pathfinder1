/**
 * Haunt actor data model
 */
export class HauntModel extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      attributes: new fields.SchemaField({
        hp: new fields.SchemaField({
          base: new fields.NumberField({
            initial: 0,
            integer: true,
            nullable: false,
          }),
          offset: new fields.NumberField({
            initial: 0,
            integer: true,
            nullable: false,
          }),
        }),
        init: new fields.SchemaField({
          value: new fields.NumberField({
            initial: 10,
            integer: true,
            nullable: false,
          }),
        }),
      }),
      templates: new fields.ArrayField(new fields.StringField({ blank: false })),
      traits: new fields.SchemaField({
        size: new fields.StringField({
          required: true,
          initial: "med",
          choices: () => pf1.config.actorSizes,
        }),
      }),
      details: new fields.SchemaField({
        alignment: new fields.StringField({
          required: true,
          initial: "tn",
          choices: () => pf1.config.alignments,
        }),
        area: new fields.SchemaField({
          size: new fields.NumberField({
            initial: 0,
            integer: true,
            min: 0,
            nullable: false,
          }),
          type: new fields.StringField({
            initial: "circle",
            required: true,
            choices: () => pf1.utils.internal.getTemplateTypes(),
          }),
        }),
        aura: new fields.SchemaField({
          custom: new fields.BooleanField({
            initial: false,
          }),
          school: new fields.StringField(),
        }),
        cl: new fields.NumberField({
          initial: 1,
          integer: true,
          min: 1,
          nullable: false,
        }),
        cr: new fields.SchemaField({
          base: new fields.NumberField({
            initial: 0,
            nullable: false,
          }),
        }),
        destruction: new fields.HTMLField(),
        effect: new fields.HTMLField(),
        notes: new fields.HTMLField(),
        notice: new fields.SchemaField({
          dc: new fields.NumberField({
            initial: 0,
            integer: true,
            min: 0,
            nullable: false,
          }),
          desc: new fields.StringField(),
          value: new fields.StringField(),
        }),
        reset: new fields.SchemaField({
          value: new fields.StringField(),
          units: new fields.StringField({
            blank: true,
            choices: () => pf1.config.timePeriods,
          }),
        }),
        trigger: new fields.StringField(),
        weakness: new fields.StringField(),
      }),
    };
  }

  static migrateData(source) {
    // Migrate Alignment info
    const alignment = source.details?.alignment?.toLowerCase();
    if (alignment && Object.keys(pf1.config.alignments).includes(alignment)) {
      source.details.alignment = alignment;
    }

    // Migrate Aura info
    if (typeof source.details?.aura === "string") {
      source.details.aura = {
        custom: true,
        school: source.details.aura,
      };
    }

    // Migrate Area info
    if (typeof source.details?.area === "string") {
      source.details.area = {
        size: source.details.area.match(/\d+/)?.[0] ?? 0,
        type: "circle",
      };
    }

    // Migrate Reset info
    if (typeof source.details?.reset === "string") {
      source.details.reset = {
        value: source.details.reset,
        units: "spec",
      };
    }

    // Migrate Notice info
    if (typeof source.details?.notice === "string") {
      source.details.notice = {
        value: source.details.notice,
        dc: 0,
      };
    }

    // Migrate Notes
    if (typeof source.details?.notes?.value === "string") {
      source.details.notes = source.details.notes.value;
    }
  }
}
