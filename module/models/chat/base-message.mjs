import { CompactingMixin } from "@models/mixins/compacting-mixin.mjs";

export class BaseMessageModel extends CompactingMixin(foundry.abstract.TypeDataModel) {
  static defineSchema() {
    const fields = foundry.data.fields;
    const nonEmpty = { initial: undefined, blank: false, nullable: false }; // Either undefined or something, not something but nothing
    return {
      subject: new fields.ObjectField(), // Message subject data
      reference: new fields.StringField({ ...nonEmpty }), // UUID reference to whatever prompted this message
      combat: new fields.StringField(), // Combat ID
      config: new fields.ObjectField(), // Details about the message use scenario
    };
  }

  static pruneData(data) {
    for (const [key, sv] of Object.entries(data.subject ?? {})) {
      if (typeof sv === "object") delete data.subject[key];
    }
    if (foundry.utils.isEmpty(data.subject)) delete data.subject;
    if (!data.reference) delete data.reference;
    if (!data.combat) delete data.combat;
    if (foundry.utils.isEmpty(data.config)) delete data.config;
  }

  /**
   * Additional shim for metadata shim
   *
   * @deprecated - Use `msg.system.config.rank` instead.
   * @returns {{rank:number}} - Skill data
   */
  get skill() {
    foundry.utils.logCompatibilityWarning(
      "ChatMessagePF.flags.pf1.metadata.skill.rank has been deprecated in favor of ChatMessagePF.system.config.rank",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );

    return {
      rank: this.config.rank,
    };
  }
}
