import { BaseMessageModel } from "./base-message.mjs";

/**
 * Data Model for item cards.
 */
export class ItemMessageModel extends BaseMessageModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      ...super.defineSchema(),
      actor: new fields.StringField(), // Actor UUID (is this actually ever different from card speaker?)
      template: new fields.StringField(), // Template UUID
      item: new fields.SchemaField({
        id: new fields.StringField(), // Item ID,
        name: new fields.StringField(),
        description: new fields.HTMLField(),
        identified: new fields.BooleanField({ initial: true }),
      }),
    };
  }

  static migrateData(source) {
    if (typeof source.item === "string") {
      source.item = { id: source.item };
    }

    return super.migrateData(source);
  }

  static pruneData(data) {
    super.pruneData(data);

    if (!data.actor) delete data.actor;
    if (!data.template) delete data.template;

    if (data.item) {
      if (!data.item.id) delete data.item.id;
      if (data.item.identified) delete data.item.identified;
      if (!data.item.description) delete data.item.description;
      if (!data.item.name) delete data.item.name;

      if (foundry.utils.isEmpty(data.item)) delete data.item;
    }
  }
}
