import { MigrationDialog } from "@app/migration/migration-dialog.mjs";
import { MigrationState } from "./migration/migration-state.mjs";

export { movedAssets as assets } from "./migration/asset-moves.mjs";

export const UPDATE_CHUNK_SIZE = 50;

/**
 * Moved compendium content.
 */
export const moved = /** @type {const} */ ({
  // Trapfinding (v10)
  "Compendium.pf1.class-abilities.Item.OhHKCLQXoMlYNodk": "Compendium.pf1.class-abilities.Item.pEODJDoTk7uhCZY7",
  // Trap Sense (v10)
  "Compendium.pf1.class-abilities.Item.fb00TzBa32sSisGb": "Compendium.pf1.class-abilities.Item.BoEkMviJrW0PKmhj",
  // Danger Sense (v10)
  "Compendium.pf1.class-abilities.Item.4bcGnKYf9beV0nfa": "Compendium.pf1.class-abilities.Item.sTlu3zgAEDdJnER5",
  // Fast Movement (v10)
  "Compendium.pf1.class-abilities.Item.dvQdP8QfrDA9Lxzk": "Compendium.pf1.class-abilities.Item.9EX00obqhGHcrOdp",
  // A Thousand Faces (v11)
  "Compendium.pf1.class-abilities.Item.cBwQdqQ4KmVBck3t": "Compendium.pf1.class-abilities.Item.MXj2WoyW7vLxiVYw",
  // Poison Use (v11)
  "Compendium.pf1.class-abilities.Item.kKaz5A6XbuxgVvhO": "Compendium.pf1.class-abilities.Item.A54sLfcyoR5HGNbP",
  "Compendium.pf1.class-abilities.Item.AmsnoKelAxLttUbj": "Compendium.pf1.class-abilities.Item.A54sLfcyoR5HGNbP",
  // Trap Sense (V11)
  "Compendium.pf1.class-abilities.Item.KbhRBQE5ZyYedJo6": "Compendium.pf1.class-abilities.Item.BoEkMviJrW0PKmhj",
});

export const compendiumRenames = /** @type {const}*/ ({
  racialhd: "racial-hd",
  commonbuffs: "buffs",
  mythicpaths: "mythic-paths",
  "pf1e-rules": "rules",
  "sample-macros": "macros",
});

export function registerRedirects() {
  for (const [oldUuid, newUuid] of Object.entries(moved)) {
    CONFIG.compendium.uuidRedirects[oldUuid] = newUuid;
  }

  const prefix = `Compendium.${game.system.id}`;
  for (const [oldName, newName] of Object.entries(compendiumRenames)) {
    CONFIG.compendium.uuidRedirects[`${prefix}.${oldName}`] = `${prefix}.${newName}`;
  }
}

/**
 * Material ID changes done with v10.5
 */
const materialChanges = {
  nexavarianSteel: "nexavaranSteel",
  alchemicalsilver: "alchemicalSilver",
  angelskin: "angelSkin",
  bloodcrystal: "bloodCrystal",
  coldiron: "coldIron",
  darkleafcloth: "darkleafCloth",
  eelhide: "eelHide",
  elysianbronze: "elysianBronze",
  fireforgedsteel: "fireForgedSteel",
  frostforgedsteel: "frostForgedSteel",
  griffonmane: "griffonMane",
  liquidglass: "liquidGlass",
  livingsteel: "livingSteel",
  singingsteel: "singingSteel",
  spiresteel: "spireSteel",
};

const contextMarker = () => ({ pf1: { action: "migration" } });

/**
 * An indicator for whether the system is currently migrating the world.
 *
 * @type {boolean}
 */
// As the `pf1` global does not use this ES module but a cloned copy, this value
// only exists for the documentation. Always use `pf1.migrations.isMigrating` instead!
export let isMigrating = false; // eslint-disable-line prefer-const -- pf1.migrations.isMigrating is changed at runtime

/**
 * Test if provided document has been migrated to current version.
 *
 * @remarks
 * - This only tests migration flag. If the document has been updated with outdated data since, this will return false positive.
 *
 * @param {foundry.abstract.Document} doc - Any document to test
 * @returns {boolean} - Migration matches current version
 */
export function isMigrated(doc) {
  const migration = doc.flags?.pf1?.migration; // No getFlag() to support pack index entries
  if (migration) {
    const v = pf1.utils.SemanticVersion.fromString(migration);
    return !v.isLowerThan(pf1.utils.SemanticVersion.fromString(game.system.version));
  }
  return false;
}

/**
 * Initialize {@link MigrationState} and {@link MigrationDialog}
 *
 * @param {MigrationState} [state] - State tracker
 * @param {string} [label] - Label
 * @param {object} [dialog=null] - If falsy, disable dialog. Otherwise options for the dialog.
 * @returns {MigrationState} - Original state or newly initialized one.
 */
async function initializeStateAndDialog(state, label, dialog = null) {
  state ??= new MigrationState(label);
  if (dialog) await MigrationDialog.initialize(state, label, dialog);

  return state;
}

/**
 * Perform a system migration for the entire World,
 * applying migrations for Actors, Items, Scenes, Tokens and Compendium packs
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.unlock=false] - If false, locked compendiums are ignored.
 * @param {boolean} [options.systemPacks=false] - Migrate system packs.
 * @param {MigrationState} [options.state] - Migration state tracker
 * @param {object} [options.dialog={}] - Progress dialog options. Set to falsy to disable the dialog.
 * @param {boolean} [options.fast=true] - Skip documents marked as migrated for current version.
 * @returns {Promise<void>} - A Promise which resolves once the migration is completed
 */
export async function migrateWorld({ unlock = false, fast = true, systemPacks = false, state, dialog = {} } = {}) {
  const isGM = game.user.isGM;

  if (systemPacks) {
    foundry.utils.logCompatibilityWarning(
      "pf1.migrations.migrateWorld() systemPacks parameter is deprecated in favor of pf1.migrations.migrateSystem()",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );
  }

  // Deny migration if migration is in progress and there's an active GM,
  // otherwise assume it's an error and allow migration to start anew.
  // Don't check for the setting to avoid migration state getting stuck, only trust the in-memory state
  if (pf1.migrations.isMigrating && game.users.activeGM) {
    return void ui.notifications.error(game.i18n.localize("PF1.Migration.InProgress"));
  }

  if (isGM) await game.settings.set("pf1", "migrating", true);

  pf1.migrations.isMigrating = true;
  Hooks.callAll("pf1MigrationStarted", { scope: "world" });

  state = await initializeStateAndDialog(state, "PF1.Migration.Category.World", dialog);
  state.unlock = unlock;
  state.fast = fast;

  state.start();

  const startMessage = game.i18n.format("PF1.Migration.Start", { version: game.system.version });
  const smsgId = ui.notifications.info(startMessage, { permanent: true, console: false });
  console.log("PF1 | Migration | Starting...");

  if (isGM) {
    await _migrateWorldSettings();
  }

  // Pre-register  migration categories so they all show on the dialog immediately
  if (dialog) {
    state.createCategory("actors", "PF1.Migration.Category.Actors", true);
    state.createCategory("items", "PF1.Migration.Category.Items", true);
    state.createCategory("chat", "PF1.Migration.Category.Chat", true);
    state.createCategory("packs", "PF1.Migration.Category.Packs", true);
    state.createCategory("scenes", "PF1.Migration.Category.Scenes", true);

    // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
    await new Promise((resolve) => setTimeout(resolve, 100));
  }

  // Migrate World Actors
  const actorMigration = migrateActors({ state, fast, noHooks: true });

  // Migrate World Items
  const itemMigration = migrateItems({ state, fast, noHooks: true });

  let packMigration = Promise.resolve(),
    chatMigration = Promise.resolve();

  if (isGM) {
    // Migrate chat messages
    chatMigration = migrateMessages({ state, fast, noHooks: true });

    // Migrate Compendium Packs
    const packs = game.packs.filter((p) => {
      const source = p.metadata.packageType;
      // Ignore modules, adventures, etc.
      if (!["world", "system"].includes(source)) return false;
      // Ignore system packs unless configured to include them
      if (source === "system" && !systemPacks) return false;
      // Ignore unsupported pack types
      return ["Actor", "Item", "Scene"].includes(p.metadata.type);
    });

    packMigration = migrateCompendiums(packs, { unlock, fast, state, noHooks: true });
  }

  // Migrate Unlinked Actors
  await actorMigration; // Base actors must be migrated before unlinked actors are migrated
  const sceneMigration = migrateScenes({ state, fast, noHooks: true });

  // Wait for migrations to finish
  await Promise.allSettled([itemMigration, packMigration, sceneMigration, chatMigration]);

  // Remove start message
  ui.notifications.remove(smsgId);

  // Remove migration notification
  ui.notifications.info(game.i18n.format("PF1.Migration.End", { version: game.system.version }), { console: false });
  console.log("PF1 | Migration | Completed!");

  if (isGM) {
    // Set the migration as complete
    await game.settings.set("pf1", "systemMigrationVersion", game.system.version);

    await game.settings.set("pf1", "migrating", false);
  }

  state.finish();

  Hooks.callAll("pf1MigrationFinished", { scope: "world" });
}

/**
 * Migrate actors directory.
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.fast] - If true, skip already migrated documents.
 * @param {MigrationState} [options.state] - Internal only. State tracker.
 * @param {object} [options.dialog=null] - Dialog configuration.
 * @param {boolean} [options.noHooks=false] - If true, no migration hooks will be fired.
 * @returns {Promise<void>}
 */
export async function migrateActors({ fast, state, dialog = null, noHooks = false } = {}) {
  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "actors" });

  // Locally generated state tracker
  const localState = !state;
  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Actors", dialog);

  if (localState) state.start();

  const tracker = state.createCategory("actors", "PF1.Migration.Category.Actors", true);

  console.log("PF1 | Migration | Actors directory starting...");
  tracker.start();

  tracker.setTotal(game.actors.size);
  tracker.setInvalid(game.actors.invalidDocumentIds.size);

  // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
  if (dialog) await new Promise((resolve) => setTimeout(resolve, 100));

  for (const actor of game.actors) {
    if (!actor.isOwner || (fast && isMigrated(actor))) {
      tracker.ignoreEntry(actor);
      continue;
    }

    tracker.startEntry(actor);

    try {
      const updateData = await migrateActorData(actor.toObject(), undefined, { actor });
      if (!foundry.utils.isEmpty(updateData)) {
        console.log(`PF1 | Migration | Actor: ${actor.name} | Applying updates`);
        await actor.update(updateData, contextMarker());
      }
    } catch (err) {
      tracker.recordError(actor, err);
      console.error(`PF1 | Migration | Actor: ${actor.name} | Error`, err);
    }
    tracker.finishEntry(actor);
  }

  console.log("PF1 | Migration | Actors directory complete!");
  tracker.finish();
  if (localState) state.finish();

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "actors" });
}

/**
 * Migrate items directory.
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.fast=true] - Ignore already migrated documents.
 * @param {MigrationState} [options.state] - Internal only.
 * @param {object} [options.dialog=null] - Dialog configuration.
 * @param {boolean} [options.noHooks=false] - If true, no migration hooks will be fired.
 * @returns {Promise<void>}
 */
export async function migrateItems({ fast = true, state, dialog = null, noHooks = false } = {}) {
  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "items" });

  // Locally generated state tracker
  const localState = !state;
  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Items", dialog);

  if (localState) state.start();

  const tracker = state.createCategory("items", "PF1.Migration.Category.Items", true);

  console.log("PF1 | Migration | Items directory starting...");
  tracker.start();

  tracker.setTotal(game.items.size);
  tracker.setInvalid(game.items.invalidDocumentIds.size);

  // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
  if (dialog) await new Promise((resolve) => setTimeout(resolve, 100));

  for (const item of game.items) {
    if (!item.isOwner || (fast && isMigrated(item))) {
      tracker.ignoreEntry(item);
      continue;
    }

    tracker.startEntry(item);

    try {
      const updateData = await migrateItemData(item.toObject());
      if (!foundry.utils.isEmpty(updateData)) {
        console.log(`PF1 | Migration | Item: ${item.name} | Applying updates`);
        await item.update(updateData, contextMarker());
      }
    } catch (err) {
      tracker.recordError(item, err);
      console.error(`PF1 | Migration | Item: ${item.name} | Error`, err);
    }
    tracker.finishEntry(item);
  }

  tracker.finish();
  if (localState) state.finish();
  console.log("PF1 | Migration | Items directory complete!");

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "items" });
}

/**
 * Migrate all scenes.
 *
 * @see {@link migrateScene}
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.fast=true] - Skip already migrated documents.
 * @param {MigrationState} [options.state] - Internal only. State tracker.
 * @param {boolean} [options.noHooks=false] - If true, no migration hooks will be fired.
 * @param {object} [options.dialog=null] - Dialog configuration.
 * @returns {Promise<void>}
 */
export async function migrateScenes({ fast, state, noHooks = false, dialog = null } = {}) {
  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "scenes" });

  // Locally generated state tracker
  const localState = !state;
  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Scenes", dialog);

  if (localState) state.start();

  const tracker = state.createCategory("scenes", "PF1.Migration.Category.Scenes", true);

  console.log("PF1 | Migration | Scene directory starting...");
  tracker.start();

  tracker.setTotal(game.scenes.size);
  tracker.setInvalid(game.scenes.invalidDocumentIds.size);

  // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
  if (dialog) await new Promise((resolve) => setTimeout(resolve, 100));

  for (const scene of game.scenes) {
    tracker.startEntry(scene);
    await migrateScene(scene, { fast, state, tracker });
    tracker.finishEntry(scene);
  }

  tracker.finish();

  if (localState) state.finish();
  console.log("PF1 | Migration | Scene directory complete!");

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "scenes" });
}

/**
 * Migrate compendiums.
 *
 * @see {@link migrateCompendium}
 *
 * @param {Array<string|WorldCollection>|null} [packs=null] - Array of pack IDs or packs to migrate. If null, all packs will be migrated.
 * @param {object} [options={}] - Additional options to pass along.
 * @param {boolean} [options.unlock=false] - If false, locked compendiums are ignored.
 * @param {boolean} [options.noHooks=false] - If true, no migration hooks will be fired.
 * @param {boolean} [options.fast=true] - Skip documents marked as migrated for current version.
 * @param {MigrationState} [options.state] - Migration state tracker
 * @param {object} [options.dialog=null] - Display migration dialog. Falsy disables.
 * @returns {Promise<void>} - Promise that resolves once all migrations are complete.
 * @throws {Error} - If defined pack is not found.
 */
export async function migrateCompendiums(
  packs = null,
  { unlock = false, fast = true, state, noHooks = false, dialog = null } = {}
) {
  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "packs", packs: foundry.utils.deepClone(packs) });

  if (packs === null) packs = [...game.packs];

  // Locally generated state tracker
  const localState = !state;
  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Packs", dialog);
  if (state) {
    state.unlock = unlock;
    state.fast = fast;
  }

  if (localState) state?.start();

  const tracker = state?.createCategory("packs", "PF1.Migration.Category.Packs", true);

  tracker?.start();
  tracker?.setTotal(packs.length);

  // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
  if (dialog) await new Promise((resolve) => setTimeout(resolve, 100));

  for (const pack of packs) {
    if (!unlock && pack.locked) {
      tracker?.ignoreEntry(pack);
      continue;
    }

    tracker?.startEntry(pack);

    try {
      await migrateCompendium(pack, { unlock, fast, noHooks: true, tracker });
    } catch (err) {
      console.error(`PF1 | Migration | Pack: ${pack.collection} | Error\n`, err);
      tracker?.recordError({ name: game.i18n.localize(pack.metadata.label), uuid: pack.metadata.id }, err);
    }

    tracker?.finishEntry(pack);
  }

  tracker?.finish();
  if (localState) state?.finish();

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "packs", packs: foundry.utils.deepClone(packs) });
}

/**
 * Migrate system compendia.
 *
 * Convenience wrapper for migrateCompendiums.
 *
 * @see {@link migrateCompendiums}
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.unlock] - Unlock compendiums
 * @param {boolean} [options.fast=false] - Skip unmigrated documents.
 * @param {boolean} [options.server] - Call server-side migration on data. Developers only.
 * @param {MigrationState} [options.state] - Internal only. State tracker.
 * @param {object} [options.dialog={}] - Migration dialog options. Falsy disables the dialog.
 * @returns {Promise<void>}
 */
export async function migrateSystem({ unlock = true, fast = false, server = true, state, dialog = {} } = {}) {
  Hooks.callAll("pf1MigrationStarted", { scope: "system" });

  state = await initializeStateAndDialog(state, "PF1.Migration.Category.System", dialog);
  state.unlock = unlock;
  state.fast = fast;

  state.start();
  console.debug("PF1 | Migration | System migration starting...");

  const packs = game.packs.filter((p) => p.metadata.packageType === "system");

  await migrateCompendiums(packs, { unlock, fast, server, state, dialog: false, noHooks: true });

  console.debug("PF1 | Migration | System migration complete!");
  state.finish();

  Hooks.callAll("pf1MigrationFinished", { scope: "system" });
}

/**
 * Migrate module compendia.
 *
 * Convenience wrapper for migrateCompendiums.
 *
 * @see {@link migrateCompendiums}
 *
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.unlock] - Unlock compendiums
 * @param {boolean} [options.fast=true] - Skip documents marked as migrated for current version.
 * @param {boolean} [options.server=true] - Call server-side migration. Developers only.
 * @param {object} [options.dialog={}] - Dialog options. Falsy disables the dialog.
 * @param {MigrationState} [options.state] - Internal only. State tracker instance.
 * @returns {Promise<void>}
 */
export async function migrateModules({ unlock = true, server = true, fast = true, state, dialog = {} } = {}) {
  Hooks.callAll("pf1MigrationStarted", { scope: "modules" });

  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Modules", dialog);
  state.unlock = unlock;
  state.fast = fast;

  state.start();
  console.debug("PF1 | Migration | Module migration starting...");

  const packs = game.packs.filter((p) => p.metadata.packageType === "module");
  await migrateCompendiums(packs, { unlock, fast, server, state, noHooks: true });

  console.debug("PF1 | Migration | Module migration complete!");
  state.finish();

  Hooks.callAll("pf1MigrationFinished", { scope: "modules" });
}

/* -------------------------------------------- */

/**
 * Apply migration rules to all Documents within a single Compendium pack
 *
 * @param {CompendiumCollection|string} pack - Compendium (or its ID) to migrate
 * @param {object} [options={}] - Additional options
 * @param {boolean} [options.unlock=false] - If false, locked compendium will be ignored.
 * @param {boolean} [options.fast=true] - Skip documents marked as migrated for current version.
 * @param {boolean} [options.noHooks=false] - If true, no migration hooks will be fired.
 * @param {MigrationCategory} [options.tracker] - Internal only. Tracker instance.
 * @param {boolean} [options.marker=true] - Add migration marker to migrated documents.
 * @param {boolean} [options.server=false] - Call server-side migration. Developers only.
 * @returns {Promise<void>} - Promise that resolves once migration is complete.
 * @throws {Error} - If defined pack is not found.
 */
export async function migrateCompendium(
  pack,
  { unlock = false, fast = true, server = false, marker = true, noHooks = false, tracker } = {}
) {
  if (typeof pack === "string") {
    pack = game.packs.get(pack);
    if (!pack) throw new Error(`Compendium "${pack}" not found.`);
  }

  if (pack.locked && !unlock) return;

  const docType = pack.metadata.type;
  if (!["Actor", "Item", "Scene", "RollTable", "Macro"].includes(docType)) return;

  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "pack", collection: pack });

  // Iterate over compendium entries - applying fine-tuned migration functions
  console.log(`PF1 | Migration | Pack: ${pack.collection} | Starting...`);

  const wasLocked = pack.locked;

  // Server-side migration; only useful for development
  if (server) {
    if (wasLocked) await pack.configure({ locked: false });
    console.debug(`PF1 | Migration | Pack: ${pack.collection} | Server-side migration starting...`);
    await pack.migrate();
    console.debug(`PF1 | Migration | Pack: ${pack.collection} | Server-side migration done!`);
    ui.notifications.clear(); // Do aggressive clearing
  }

  console.debug(`PF1 | Migration | Pack: ${pack.collection} | Fetching ${pack.index.size} document(s)`);

  /** @type {Actor[]|JournalEntry[]|Item[]} */
  const documents = await pack.getDocuments();

  console.debug(`PF1 | Migration | Pack: ${pack.collection} | Building update data...`);

  // Collect updates
  const updates = [];

  async function applyUpdates() {
    console.debug(`PF1 | Migration | Pack: ${pack.collection} | Applying update(s) to ${updates.length} document(s)`);

    if (pack.locked) await pack.configure({ locked: false });

    // Commit updates
    try {
      await getDocumentClass(docType).updateDocuments(updates, { pack: pack.collection, ...contextMarker() });
    } catch (err) {
      console.error(`PF1 | Migration | Pack: ${pack.collection} | Error:`, err);
      tracker?.recordError({ name: game.i18n.localize(pack.metadata.label), uuid: pack.metadata.id }, err);
    }
  }

  while (documents.length) {
    const document = documents.shift();
    if (fast && isMigrated(document)) continue;

    try {
      let updateData;
      switch (docType) {
        case "Item":
          updateData = await migrateItemData(document.toObject(), undefined, { item: document });
          break;
        case "Actor":
          updateData = await migrateActorData(document.toObject(), undefined, { actor: document });
          break;
        case "Scene": {
          await migrateScene(document);
          break;
        }
      }

      if (updateData && !foundry.utils.isEmpty(updateData)) {
        updateData._id = document.id;
        updates.push(updateData);
      }
    } catch (err) {
      tracker?.recordError(document, err);
      console.error(`PF1 | Migration | Pack: ${pack.collection} | Error!`, err);
    }

    if (updates.length >= pf1.migrations.UPDATE_CHUNK_SIZE) {
      await applyUpdates();
      updates.length = 0; // Clear update array
    }
  }

  if (updates.length) {
    await applyUpdates();
  } else {
    console.debug(`PF1 | Migration | Pack: ${pack.collection} | No updates needed`);
  }

  if (wasLocked) await pack.configure({ locked: true });

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "pack", collection: pack });

  console.log(`PF1 | Migration | Pack: ${pack.collection} | Complete!`);
}

/**
 * Migrates world settings.
 */
async function _migrateWorldSettings() {}

/* -------------------------------------------- */
/*  Document Type Migration Helpers               */
/* -------------------------------------------- */

/**
 * Migrate data in tokens that is no longer used.
 *
 * @param {object} tokenData Token data
 * @param {object} [options] - Additional options
 * @param {TokenDocument} [options.token] - Token document
 * @param {boolean} [options.marker=true] - Add migration marker
 */
export async function migrateTokenData(tokenData, { token, marker = true }) {
  const flags = tokenData.flags?.pf1 ?? {};

  const updateData = {};

  // Remove obsolete flags
  if (flags.lowLightVision !== undefined) {
    updateData["flags.pf1.-=lowLightVision"] = null;
  }
  if (flags.lowLightVisionMultiplier !== undefined) {
    updateData["flags.pf1.-=lowLightVisionMultiplier"] = null;
  }
  if (flags.lowLightVisionMultiplierBright !== undefined) {
    updateData["flags.pf1.-=lowLightVisionMultiplierBright"] = null;
  }

  // Remove disabled but still in use flags
  if (flags.disableLowLight === false) {
    updateData["flags.pf1.-=disableLowLight"] = null;
  }
  if (flags.staticSize === false) {
    updateData["flags.pf1.-=staticSize"] = null;
  }
  if (flags.customVisionRules === false) {
    updateData["flags.pf1.-=customVisionRules"] = null;
  }

  // Remove data from v9 vision handling
  // Added with PF1 v9.4
  if (!flags.customVisionRules) {
    let visionMode, range;
    // Attempt to preserve vision range after migration
    if (tokenData.sight.visionMode !== "basic") {
      if (tokenData.sight.range !== 0) range = 0;
      visionMode = "basic";
    }

    // HACK: Foundry V13 does not allow deleting these to reset them, so we reset them via temporary document
    const { saturation, brightness, attenuation, contrast } = new TokenDocument().toObject().sight;
    updateData.sight = { saturation, brightness, attenuation, contrast, range, visionMode };

    if (tokenData.detectionModes?.length) updateData["detectionModes"] = [];
  }

  // Record migrated version
  if (marker && !foundry.utils.isEmpty(updateData)) {
    updateData["flags.pf1.migration"] = game.system.version;
  }

  return updateData;
}

/**
 * Migrate token.
 *
 * @param {TokenDocument} token - Token to migrate
 * @returns {Promise<TokenDocument|null>} - Promise to updated document,. or null if no update was done.
 */
export async function migrateToken(token) {
  const tokenData = token.toObject();
  const updateData = await migrateTokenData(tokenData, { token });
  if (!foundry.utils.isEmpty(updateData)) {
    return token.update(foundry.utils.expandObject(updateData), contextMarker());
  }
}

/**
 * Migrate singular actor document.
 *
 * @param {Actor} actor - Actor to migrate.
 * @returns {Promise<void>}
 */
export async function migrateActor(actor) {
  console.debug("PF1 | Migration | Starting |", actor.name, actor.uuid);

  const updateData = await migrateActorData(actor.toObject(), actor.token, { actor });
  if (!foundry.utils.isEmpty(updateData)) {
    await actor.update(updateData, contextMarker());
  }

  await migrateActiveEffectsToItems(actor);

  console.debug("PF1 | Migration | Finished |", actor.name, actor.uuid);
}

/**
 * Migrate active effects from actor to items that should own them instead.
 *
 * Added with PF1 v10
 *
 * @param {ActorPF} actor
 */
export async function migrateActiveEffectsToItems(actor) {
  const p = [];

  for (const ae of actor.effects) {
    // Ignore all cases that would indicate this is not the old tracker AE
    // Essentially verify the AE does absolutely nothing on its own, as the old AEs had no good markers.
    if (ae.statuses.size > 0) continue;
    if (ae.transfer) continue;
    if (ae.disabled) continue;
    if (ae.type !== "base") continue;
    if (!ae.origin) continue;
    if (ae.changes.length !== 0) continue;
    if (ae.description.length > 0) continue;
    if (Object.keys(ae.flags).some((k) => !["pf1"].includes(k))) continue; // Non-PF1 flags

    const item = await fromUuid(ae.origin, { relative: actor });
    if (!item) continue;
    if (item.name !== ae.name) continue;

    // Transfer tracker AE to item
    if (item.effect?.type !== "buff") {
      const aeData = ae.toObject();
      aeData.transfer = true;
      aeData.type = "buff";

      const p0 = ActiveEffect.implementation.create(aeData, { parent: item });
      p.push(p0);
    }

    // Delete old in all cases.
    // CAUTION: This is technically destructive as the above criteria is NOT foolproof.
    p.push(ae.delete());
  }

  await Promise.all(p);
}

/**
 * Migrate a single Actor document to incorporate latest data model changes
 * Return an Object of updateData to be applied
 *
 * @param {ActorData} actorData   The actor data to derive an update from
 * @param {TokenDocument} token
 * @param {object} [options] - Additional options
 * @param {Actor} [options.actor] - Associated actor document
 * @param {boolean} [options.marker=true] - Add migration marker
 * @returns {object} - The updateData to apply
 */
export async function migrateActorData(actorData, token, { actor, marker = true } = {}) {
  // Ignore module introduced types
  if (actorData.type.indexOf(".") !== -1) return {};

  const updateData = {};

  _migrateActorEncumbrance(actorData, updateData);
  _migrateActorNoteArrays(actorData, updateData);
  _migrateActorSpeed(actorData, updateData);
  _migrateActorSpellbookCL(actorData, updateData);
  _migrateActorSpellbookSlots(actorData, updateData);
  _migrateActorSpellbookPrep(actorData, updateData);
  _migrateActorSpellbookKind(actorData, updateData, actor);
  _migrateActorConcentration(actorData, updateData);
  _migrateActorBaseStats(actorData, updateData);
  _migrateUnusedActorCreatureType(actorData, updateData);
  _migrateActorSpellbookDCFormula(actorData, updateData);
  _migrateActorHPAbility(actorData, updateData);
  _migrateActorCR(actorData, updateData);
  _migrateAttackAbility(actorData, updateData);
  _migrateActorDefenseAbility(actorData, updateData);
  _migrateActorSpellbookUsage(actorData, updateData);
  _migrateActorNullValues(actorData, updateData);
  _migrateActorSpellbookDomainSlots(actorData, updateData);
  _migrateActorStatures(actorData, updateData);
  _migrateActorInitAbility(actorData, updateData);
  _migrateActorChangeRevamp(actorData, updateData);
  _migrateActorCMBRevamp(actorData, updateData);
  _migrateCarryBonus(actorData, updateData);
  _migrateBuggedValues(actorData, updateData);
  _migrateSpellbookUsage(actorData, updateData);
  _migrateActorHP(actorData, updateData);
  _migrateActorSenses(actorData, updateData, token);
  _migrateActorInvaliddSkills(actorData, updateData);
  _migrateActorSkillRanks(actorData, updateData);
  _migrateActorSkillJournals(actorData, updateData);
  _migrateActorSubskillData(actorData, updateData);
  _migrateActorUnusedData(actorData, updateData);
  _migrateActorDRandER(actorData, updateData);
  _migrateActorTraits(actorData, updateData);
  _migrateActorProficiencies(actorData, updateData);
  _migrateActorFlags(actorData, updateData);

  // Migrate Owned Items
  const items = [];
  for (const item of actorData.items ?? []) {
    // Migrate the Owned Item
    const itemData = item instanceof Item ? item.toObject() : item;
    const itemDoc = actor?.items.get(itemData._id);
    try {
      const itemUpdate = await migrateItemData(itemData, actor, { item: itemDoc });

      // Update the Owned Item
      if (!foundry.utils.isEmpty(itemUpdate)) {
        itemUpdate._id = itemData._id;
        items.push(foundry.utils.expandObject(itemUpdate));
      }
    } catch (err) {
      console.error(
        `Error migrating Item "${item.name}" [${itemDoc?.uuid ?? itemData._id}] on actor "${actorData.name}" [${
          actor?.uuid ?? actorData._id
        }]`
      );
      throw new Error(`Item "${item.name}" [${itemData._id}] failed migration: ${err.message}`, { cause: err });
    }
  }
  if (items.length > 0) updateData.items = items;

  // Active Effects
  await _migrateActorActiveEffects(actorData, updateData, actor);

  // Record migrated version
  if (marker && !foundry.utils.isEmpty(updateData)) {
    updateData["flags.pf1.migration"] = game.system.version;
  }

  return foundry.utils.expandObject(updateData);
}

/* -------------------------------------------- */

/**
 *  Migrate singular item document.
 *
 * @param {Item} item - Item document to update.
 * @returns {Promise<Item|null>} - Promise to updated item document, or null if no update was performed.
 */
export async function migrateItem(item) {
  let updated = false;
  const updateData = await migrateItemData(item.toObject(), item.actor, { item });
  if (!foundry.utils.isEmpty(updateData)) {
    updated = true;
    await item.update(updateData, contextMarker());
  }

  await migrateItemActiveEffects(item);

  return updated ? item : null;
}

/**
 * Migrate active effects on an item.
 *
 * @param {Item} item
 * @returns {Promise<ActiveEffect[]|null>} - Updated active effects, or null if no updates were needed.
 */
export async function migrateItemActiveEffects(item) {
  const itemData = item.toObject();

  const updateData = {};
  await migrateItemDataActiveEffects(itemData, updateData);

  if (updateData.effects?.length) {
    return item.updateEmbeddedDocuments("ActiveEffect", updateData.effects);
  }

  return null;
}

/**
 * Migrate active effects in item data
 *
 * @param {object} itemData - Item data
 * @param {object} updateData - Object to hold update data
 */
async function migrateItemDataActiveEffects(itemData, updateData) {
  const { effects = [] } = itemData;

  const updates = [];
  for (const effect of effects) {
    const updateData = {};

    // Convert method of tracker identification
    if (itemData.type === "buff") {
      if (effect.flags?.pf1?.tracker && effect.type === "base") {
        updateData.type = "buff";
        updateData["flags.pf1.-=tracker"] = null;
      }
    }

    const duration = effect.flags?.pf1?.duration;
    if (duration) {
      // Transfer initiative value to system data
      const init = effect.flags?.pf1?.duration?.initiative;
      if (init !== undefined) {
        if (effect.system?.initiative === undefined) updateData["system.initiative"] = init;
      }

      // End timing
      const endTiminig = effect.flags?.pf1?.duration?.end;
      if (endTiminig !== undefined) {
        if (!effect.system?.end) updateData["system.end"] = endTiminig;
      }

      updateData["flags.pf1.-=duration"] = null;
    }

    if (!foundry.utils.isEmpty(updateData)) {
      updateData._id = effect._id;
      updates.push(updateData);
    }
  }

  if (updates.length) {
    updateData.effects = updates;
  }
}

/**
 * Migrate a single Item document to incorporate latest data model changes
 *
 * @param {object} itemData    The item data to derive an update from
 * @param {Actor} actor - Parent actor document
 * @param {object} [options] - Additional options
 * @param {number} [options._depth=0] - Internal only. Recursion depth tracking.
 * @param {Item} [options.item] - Item document
 * @param {boolean} [options.marker=true] - Add migration marker
 * @returns {object} - The updateData to apply
 */
export async function migrateItemData(itemData, actor = null, { item, _depth = 0, marker = true } = {}) {
  const updateData = {};

  // Migrate data to system
  if (itemData.system == null && itemData.data != null) {
    itemData = foundry.utils.deepClone(itemData);
    itemData.system = itemData.data;
    delete itemData.data;
  }

  // Ignore module introduced types
  if (!Object.keys(game.system.documentTypes.Item).includes(itemData.type)) return {};

  _migrateItemFlags(itemData, updateData);
  _migrateItemArrayTypes(itemData, updateData);
  _migrateFlagsArrayToObject(itemData, updateData);
  _migrateWeaponImprovised(itemData, updateData);
  _migrateItemSpellDescription(itemData, updateData);
  _migrateClassDynamics(itemData, updateData);
  _migrateClassType(itemData, updateData);
  _migrateClassCasting(itemData, updateData);
  _migrateSpellDivineFocus(itemData, updateData);
  _migrateWeaponCategories(itemData, updateData);
  _migrateArmorCategories(itemData, updateData);
  _migrateArmorMaxDex(itemData, updateData);
  _migrateItemSize(itemData, updateData);
  _migrateItemFeatAbilityTypes(itemData, updateData);
  _migrateClassLevels(itemData, updateData);
  _migrateSavingThrowTypes(itemData, updateData);
  _migrateCR(itemData, updateData);
  _migrateItemChanges(itemData, updateData);
  _migrateItemChangeFlags(itemData, updateData);
  _migrateItemContextNotes(itemData, updateData);
  _migrateEquipmentSize(itemData, updateData);
  _migrateSpellCosts(itemData, updateData);
  _migrateSpellPreparation(itemData, updateData, { item });
  _migrateLootEquip(itemData, updateData);
  await _migrateItemLinks(itemData, updateData, { item, actor });
  _migrateItemProficiencies(itemData, updateData);
  _migrateItemNotes(itemData, updateData);
  _migrateScriptCalls(itemData, updateData);
  _migrateItemActionToActions(itemData, updateData, { item, actor });
  _migrateItemChargeCost(itemData, updateData);
  _migrateItemLimitedUses(itemData, updateData);
  _migrateItemWeight(itemData, updateData);
  _migrateItemHealth(itemData, updateData);
  _migrateContainerReduction(itemData, updateData);
  _migrateContainerPrice(itemData, updateData);
  _migrateItemType(itemData, updateData);
  _migrateItemLearnedAt(itemData, updateData);
  _migrateItemTuples(itemData, updateData);
  _migrateEquipmentCategories(itemData, updateData);
  _migrateSpellDescriptors(itemData, updateData);
  _migrateItemTraits(itemData, updateData);
  _migrateRaceItemCreatureType(itemData, updateData);
  _migrateItemChangeFlags(itemData, updateData);
  _migrateItemMaterials(itemData, updateData);
  _migrateSpellSubschool(itemData, updateData);
  _migrateItemDefaultAmmo(itemData, updateData);
  _migrateItemDurability(itemData, updateData);
  _migrateItemHeld(itemData, updateData);
  _migrateItemUnusedData(itemData, updateData);

  // Migrate action data
  const alreadyHasActions = itemData.system.actions instanceof Array && itemData.system.actions.length > 0;
  const itemActionData = alreadyHasActions ? itemData.system.actions : updateData["system.actions"];
  if (itemActionData instanceof Array) {
    const newActionData = itemActionData.map((action) => migrateItemActionData(action, updateData, { itemData, item }));
    // Update only if something changed. Bi-directional testing for detecting deletions.
    if (
      !foundry.utils.isEmpty(foundry.utils.diffObject(itemActionData, newActionData)) ||
      !foundry.utils.isEmpty(foundry.utils.diffObject(newActionData, itemActionData))
    ) {
      updateData["system.actions"] = newActionData;
    }
  }

  // Migrate container .inventoryItems array to .items map
  // Introduced with PF1 v10
  if (itemData.system?.inventoryItems instanceof Array) {
    updateData["system.items"] = {};
    for (const sitem of itemData.system.inventoryItems) {
      sitem._id ||= foundry.utils.randomID(16);

      // Deal with corrupt items or v9 or older items
      sitem.system ??= {};
      try {
        if ("data" in sitem) {
          sitem.system = foundry.utils.mergeObject(sitem.data, sitem.system, { inplace: false });
          delete sitem.data;
        }

        const subItem = new Item.implementation(sitem);

        const itemUpdateData = await migrateItemData(subItem.toObject(), actor, { _depth: _depth + 1 });
        subItem.updateSource(itemUpdateData);

        updateData["system.items"][sitem._id] = subItem.toObject();
      } catch (err) {
        console.error("Failed to migrate container content:", { item: sitem, parent: item, actor });
      }
    }

    updateData["system.-=inventoryItems"] = null;
  }

  // Migrate container items
  const migrateContainerItems = async (items) => {
    if (!items) return;
    for (const [itemId, itemData] of Object.entries(items)) {
      try {
        // Basic validation
        const subItem = new Item.implementation(itemData);

        // Migrate
        const subUpdate = await migrateItemData(subItem.toObject(), actor, { item: subItem, _depth: _depth + 1 });

        if (!foundry.utils.isEmpty(subUpdate)) {
          const diff = subItem.updateSource(subUpdate);
          updateData["system.items"] ??= {};
          updateData["system.items"][itemId] = diff;
        }
      } catch (err) {
        console.error("PF1 | Migration | Error", err, item);
      }
    }
  };

  await migrateContainerItems(itemData.system.items);

  await migrateItemDataActiveEffects(itemData, updateData);

  // Record migrated version
  if (marker && !foundry.utils.isEmpty(updateData)) {
    updateData["flags.pf1.migration"] = game.system.version;
  }

  // Return the migrated update data
  return updateData;
}

// Added with PF1 v10
function _migrateActionLimitedUses(action, itemData) {
  // Only physical items can be single use
  const isPhysical = CONFIG.Item.documentClasses[itemData.type]?.isPhysical;
  if (!isPhysical) {
    if (action.uses?.self?.per === "single") {
      action.uses.self.per = "charges";
      action.uses.self.maxFormula = "1";
    }
  }
}

/**
 * Migrates a single action within an item.
 *
 * @param {object} actionData - The action's data, which also serves as the update data to pass on.
 * @param {object} updateData - Item update data
 * @param {object} [options] - Additional options
 * @param {Item} [options.item=null] - Parent item document this action is in.
 * @param {object} options.itemData - Parent item data
 * @returns {object} The resulting action data.
 */
export function migrateItemActionData(actionData, updateData, { itemData, item = null } = {}) {
  actionData = foundry.utils.deepClone(actionData);

  const action = item?.actions?.get(actionData._id);

  // Migrations that must be done before datamodel cleans the needed data out
  _migrateActionDamageType(actionData, itemData);
  _migrateActionLimitedUses(actionData, itemData);
  _migrateActionExtraAttacks(actionData, itemData);
  _migrateActionAmmunitionUsage(actionData, itemData, updateData);
  _migrateActionDuration(actionData, itemData);
  _migrateActionMaterials(actionData, itemData);

  actionData = new pf1.components.ItemAction(actionData).toObject();

  _migrateActionConditionals(actionData, itemData, { action });
  _migrateActionHarmlessSpell(actionData, itemData);

  // Return the migrated update data
  return actionData;
}

/* -------------------------------------------- */

/**
 * Migrate singular scene document.
 *
 * @param {Scene} scene - Scene document to update.
 * @param {object} [options] - Additional options
 * @param {MigrationState} [options.state=null]
 * @param {MigrationCategory} [options.tracker=null]
 * @param {boolean} [options.fast] - Skip already migrated
 * @returns {Promise<void>}
 */
export async function migrateScene(scene, { fast = true, state, tracker } = {}) {
  console.log(`PF1 | Migration | Scene: ${scene.name} | Starting...`);
  try {
    await migrateSceneTokens(scene, { fast, state, tracker });
    await migrateSceneActors(scene, { fast, state, tracker });

    // Mark last migrated version
    if (game.user.isGM) await scene.setFlag("pf1", "migration", game.system.version);
  } catch (err) {
    tracker?.recordError(scene, err);
    console.error(`PF1 | Migration | Scene: ${scene.name} | Error`, err);
  }
  console.log(`PF1 | Migration | Scene: ${scene.name} | Complete!`);
}

/**
 * Migrate tokens in a single scene.
 *
 * @param {Scene} scene - The Scene to Update
 * @param {object} [options] - Additional options
 * @param {boolean} [options.fast=true] - Ignore already migrated documents.
 * @param {MigrationState} [options.state=null]
 * @param {MigrationCategory} [options.tracker=null]
 */
export async function migrateSceneTokens(scene, { fast = true, state = null, tracker = null } = {}) {
  for (const token of scene.tokens) {
    if (!token.isOwner) continue;

    if (fast && isMigrated(token)) continue;

    try {
      await migrateToken(token);
    } catch (err) {
      tracker?.recordError(token, err);
      console.error(`PF1 | Migration | Scene: ${scene.name} | Token: ${token.id} | Error`, token, err);
    }
  }
}

/**
 * Migrate unlinked actors on a single scene.
 *
 * @param {Scene} scene - Scene to migrate actors in.
 * @param {object} [options] - Additional options
 * @param {boolean} [options.fast=true] - Ignore already migrated documents.
 * @param {MigrationState} [options.state=null]
 * @param {MigrationCategory} [options.tracker=null]
 * @returns {Promise<void>}
 */
export async function migrateSceneActors(scene, { fast = true, state = null, tracker = null } = {}) {
  for (const token of scene.tokens) {
    if (token.isLinked) continue;
    const actor = token.actor;
    if (!actor?.isOwner) continue;

    if (fast && isMigrated(actor)) continue;

    try {
      const updateData = await migrateActorData(actor.toObject(), token, { actor });
      if (!foundry.utils.isEmpty(updateData)) {
        if (!(updateData.items?.length > 0)) delete updateData.items;
        if (!(updateData.effects?.length > 0)) delete updateData.effects;
        if (!foundry.utils.isEmpty(updateData)) await actor.update(updateData, contextMarker());
      }
    } catch (err) {
      tracker?.recordError(token, err);
      console.error(`PF1 | Migration | Scene: ${scene.name} | Token: ${token.id} | Error`, token, err);
    }
  }
}

/* -------------------------------------------- */

function _migrateActorEncumbrance(ent, updateData) {
  const arr = {
    "system.attributes.encumbrance.level": "attributes.encumbrance.-=level",
    "system.attributes.encumbrance.levels.light": "attributes.encumbrance.levels.-=light",
    "system.attributes.encumbrance.levels.medium": "attributes.encumbrance.levels.-=medium",
    "system.attributes.encumbrance.levels.heavy": "attributes.encumbrance.levels.-=heavy",
    "system.attributes.encumbrance.levels.carry": "attributes.encumbrance.levels.-=carry",
    "system.attributes.encumbrance.levels.drag": "attributes.encumbrance.levels.-=drag",
    "system.attributes.encumbrance.carriedWeight": "attributes.encumbrance.-=carriedWeight",
  };
  for (const [key, updateKey] of Object.entries(arr)) {
    const value = foundry.utils.getProperty(ent, key);
    if (value !== undefined) {
      updateData["system." + updateKey] = null;
    }
  }
}

/**
 * Convert array based flags into object.
 *
 * @param ent
 * @param updateData
 */
function _migrateFlagsArrayToObject(ent, updateData) {
  const flags = ent.system.flags;
  if (!flags) return;
  const bflags = flags.boolean,
    dflags = flags.dictionary;

  if (Array.isArray(bflags)) {
    // Compatibility with old data: Convert old array into actual dictionary.
    updateData["system.flags.boolean"] = bflags.reduce((flags, flag) => {
      flags[flag] = true;
      return flags;
    }, {});
  }

  if (Array.isArray(dflags)) {
    updateData["system.flags.dictionary"] = dflags.reduce((flags, [key, value]) => {
      flags[key] = value;
      return flags;
    }, {});
  }
}

function _migrateActorNoteArrays(ent, updateData) {
  const list = ["system.attributes.acNotes", "system.attributes.cmdNotes", "system.attributes.srNotes"];
  for (const k of list) {
    const value = foundry.utils.getProperty(ent, k);
    const hasValue = foundry.utils.hasProperty(ent, k);
    if (hasValue && value instanceof Array) {
      updateData[k] = value.join("\n");
    }
  }
}

function _migrateActorSpeed(ent, updateData) {
  const arr = [
    "attributes.speed.land",
    "attributes.speed.climb",
    "attributes.speed.swim",
    "attributes.speed.fly",
    "attributes.speed.burrow",
  ];
  for (const k of arr) {
    let value = foundry.utils.getProperty(ent.system, k);
    if (typeof value === "string") value = parseInt(value);
    if (typeof value === "number") {
      updateData[`system.${k}.base`] = value;
    } else if (value === null) {
      updateData[`system.${k}.base`] = 0;
    } else if (value?.total !== undefined) {
      // Delete derived data
      updateData[`system.${k}.-=total`] = null;
    }

    // Add maneuverability
    if (k === "attributes.speed.fly" && foundry.utils.getProperty(ent.system, `${k}.maneuverability`) === undefined) {
      updateData[`system.${k}.maneuverability`] = "average";
    }
  }

  // Remove bad fly maneuverability entry if it exists
  if (ent.system.attributes?.speed?.flyManeuverability !== undefined) {
    updateData["system.attributes.speed.-=flyManeuverability"] = null;
  }
}

function _migrateActorSpellbookSlots(ent, updateData) {
  for (const spellbookSlot of Object.keys(
    foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {}
  )) {
    if (
      foundry.utils.getProperty(ent.system, `attributes.spells.spellbooks.${spellbookSlot}.autoSpellLevels`) == null
    ) {
      updateData[`system.attributes.spells.spellbooks.${spellbookSlot}.autoSpellLevels`] = true;
    }

    for (let a = 0; a < 10; a++) {
      const baseKey = `system.attributes.spells.spellbooks.${spellbookSlot}.spells.spell${a}.base`;
      const maxKey = `system.attributes.spells.spellbooks.${spellbookSlot}.spells.spell${a}.max`;
      const base = foundry.utils.getProperty(ent, baseKey);
      const max = foundry.utils.getProperty(ent, maxKey);

      if (base === undefined) {
        if (typeof max === "number" && max > 0) {
          updateData[baseKey] = max.toString();
        }
      } else {
        const newBase = parseInt(base);
        if (newBase > 0) {
          if (newBase !== base) updateData[baseKey] = newBase;
        } else {
          // Remove pointless default value not present in new actors either
          updateData[`system.attributes.spells.spellbooks.${spellbookSlot}.spells.spell${a}.-=base`] = null;
        }
      }
    }
  }
}

// Remove inconsistently used .spontaneous permanently recorded boolean
// Added with PF1 v10
function _migrateActorSpellbookPrep(actorData, updateData) {
  for (const [bookId, book] of Object.entries(
    foundry.utils.getProperty(actorData.system, "attributes.spells.spellbooks") || {}
  )) {
    const wasSpontaneous = book.spontaneous;
    if (wasSpontaneous === undefined) continue;

    const usesAuto = book.autoSpellLevelCalculation ?? false;
    const usesSpellpoints = book.spellPoints.useSystem === true;
    if (!usesAuto && !usesSpellpoints) {
      // Set prep type to match old spontaneous toggle
      updateData[`system.attributes.spells.spellbooks.${bookId}.spellPreparationMode`] = wasSpontaneous
        ? "spontaneous"
        : "prepared";

      // Set progression type to high to match old behaviour
      updateData[`system.attributes.spells.spellbooks.${bookId}.casterType`] = "high";
    }

    updateData[`system.attributes.spells.spellbooks.${bookId}.-=spontaneous`] = null;
  }
}

/**
 * Migrate spellbook kind (arcane, divine, psychic, ...)
 *
 * @param {object} actorData - Actor data
 * @param {object} updateData - Update data
 * @param {Actor} actor - Actor document
 */
function _migrateActorSpellbookKind(actorData, updateData, actor) {
  for (const [bookId, book] of Object.entries(
    foundry.utils.getProperty(actorData.system, "attributes.spells.spellbooks") || {}
  )) {
    if (book.kind === undefined && book.inUse) {
      // Attempt to get data from class
      const castingClass =
        !!book.class && book.class !== "_hd" ? actor.itemTypes.class.find((i) => i.system.tag === book.class) : null;
      let kind = castingClass?.system.casting?.spells;

      if (!kind) {
        // Attempt to determine kind without access to source class
        kind = "arcane"; // Default to arcane if all else fails
        if (book.arcaneSpellFailure) kind = "arcane";
        else if (book.psychic) kind = "psychic";
        else if (book.spellPreparationMode === "prepared" && book.ability === "int") kind = "alchemy";
        else if (book.class !== "_hd") kind = "divine";
      }

      updateData[`system.attributes.spells.spellbooks.${bookId}.kind`] = kind;
    }
  }
}

function _migrateActorBaseStats(actorData, updateData) {
  const deleteEntries = {
    "system.attributes.hd.base": "system.attributes.hd.-=base",
    "system.attributes.savingThrows.fort.value": "system.attributes.savingThrows.fort.-=value",
    "system.attributes.savingThrows.ref.value": "system.attributes.savingThrows.ref.-=value",
    "system.attributes.savingThrows.will.value": "system.attributes.savingThrows.will.-=value",
  };

  for (const [path, dPath] of Object.entries(deleteEntries)) {
    if (foundry.utils.getProperty(actorData, path) !== undefined) {
      updateData[dPath] = null;
    }
  }

  // Delete HP base if it's not non-zero or actor has any classes
  if (actorData.system.attributes?.hp?.base !== undefined) {
    if (!(actorData.system.attributes?.hp?.base > 0) || actorData.items.some((i) => i.type === "class")) {
      updateData["system.attributes.hp.-=base"] = null;
    }
  }
}

function _migrateUnusedActorCreatureType(ent, updateData) {
  const type = foundry.utils.getProperty(ent.system, "attributes.creatureType");
  if (type != undefined) updateData["system.attributes.-=creatureType"] = null;
}

function _migrateActorSpellbookDCFormula(ent, updateData) {
  const spellbooks = Object.keys(foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {});

  for (const k of spellbooks) {
    const key = `system.attributes.spells.spellbooks.${k}.baseDCFormula`;
    const curFormula = foundry.utils.getProperty(ent, key);
    if (!curFormula) updateData[key] = "10 + @sl + @ablMod";
  }
}

function _migrateActorSpellbookName(ent, updateData) {
  const spellbooks = Object.entries(foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {});
  for (const [bookId, book] of spellbooks) {
    if (book.altName !== undefined) {
      const key = `system.attributes.spells.spellbooks.${bookId}`;
      updateData[`${key}.-=altName`] = null;
      if (book.altName.length) updateData[`${key}.name`] = book.altName;
    }
  }
}

function _migrateActorSpellbookCL(ent, updateData) {
  const spellbooks = Object.keys(foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {});

  for (const k of spellbooks) {
    const key = `system.attributes.spells.spellbooks.${k}.cl`;
    const curBase = parseInt(foundry.utils.getProperty(ent, key + ".base"));
    const curFormula = foundry.utils.getProperty(ent, key + ".formula");
    if (curBase > 0) {
      if (curFormula.length > 0) updateData[`${key}.formula`] = curFormula + " + " + curBase;
      else updateData[`${key}.formula`] = curFormula + curBase;
      updateData[`${key}.base`] = 0;
    }
  }
}

function _migrateActorConcentration(ent, updateData) {
  const spellbooks = Object.keys(foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {});
  for (const k of spellbooks) {
    // Delete unused .concentration from old actors
    const key = `system.attributes.spells.spellbooks.${k}`;
    const oldValue = foundry.utils.getProperty(ent, `${key}.concentration`);
    const isString = typeof oldValue === "string";
    if (Number.isFinite(oldValue) || isString) updateData[`${key}.-=concentration`] = null;
    if (isString) {
      // Assume erroneous bonus formula location and combine it with existing bonus formula.
      const formulaKey = `${key}.concentrationFormula`;
      const formula = [oldValue];
      formula.push(foundry.utils.getProperty(ent, formulaKey) || "");
      updateData[formulaKey] = formula.filter((f) => f !== 0 && f?.toString().trim().length).join(" + ");
    }
  }
}

function _migrateActorHPAbility(ent, updateData) {
  // Set HP ability
  if (foundry.utils.getProperty(ent.system, "attributes.hpAbility") === undefined) {
    updateData["system.attributes.hpAbility"] = "con";
  }

  // Set Fortitude save ability
  if (foundry.utils.getProperty(ent.system, "attributes.savingThrows.fort.ability") === undefined) {
    updateData["system.attributes.savingThrows.fort.ability"] = "con";
  }

  // Set Reflex save ability
  if (foundry.utils.getProperty(ent.system, "attributes.savingThrows.ref.ability") === undefined) {
    updateData["system.attributes.savingThrows.ref.ability"] = "dex";
  }

  // Set Will save ability
  if (foundry.utils.getProperty(ent.system, "attributes.savingThrows.will.ability") === undefined) {
    updateData["system.attributes.savingThrows.will.ability"] = "wis";
  }
}

function _migrateItemArrayTypes(ent, updateData) {
  const conditionals = ent.system.conditionals;
  if (conditionals != null && !(conditionals instanceof Array)) {
    updateData["system.conditionals"] = [];
  }

  const contextNotes = ent.system.contextNotes;
  if (contextNotes != null && !(contextNotes instanceof Array)) {
    if (contextNotes instanceof Object) updateData["system.contextNotes"] = Object.values(contextNotes);
    else updateData["system.contextNotes"] = [];
  }
}

function _migrateWeaponImprovised(ent, updateData) {
  if (ent.type !== "weapon") return;

  const value = foundry.utils.getProperty(ent.system, "weaponType");
  if (value === "improv") {
    updateData["system.weaponType"] = "misc";
    updateData["system.properties.imp"] = true;
  }
}

// Migrates the weird .shortDescription back to .description.value
// Added with PF1 v10
function _migrateItemSpellDescription(itemData, updateData) {
  if (itemData.type !== "spell") return;

  if (itemData.system.shortDescription) {
    updateData["system.-=shortDescription"] = null;

    // If description.value exists, it's the older oversized pre-rendered version that is unwanted
    updateData["system.description.value"] = itemData.system.shortDescription;
  }
}

function _migrateSpellDivineFocus(item, updateData) {
  if (item.type !== "spell") return;

  const df = foundry.utils.getProperty(item.system, "components.divineFocus");
  if (typeof df === "boolean") updateData["system.components.divineFocus"] = Number(df);
}

function _migrateClassDynamics(ent, updateData) {
  if (ent.type !== "class") return;

  const bab = ent.system.bab;
  if (typeof bab === "number") updateData["system.bab"] = "low";

  const stKeys = ["system.savingThrows.fort.value", "system.savingThrows.ref.value", "system.savingThrows.will.value"];
  for (const key of stKeys) {
    const value = foundry.utils.getProperty(ent, key);
    if (typeof value === "number") updateData[key] = "low";
  }
}

function _migrateClassType(ent, updateData) {
  if (ent.type !== "class") return;

  if (ent.system.classType !== undefined && ent.system.subType === undefined) {
    updateData["system.subType"] = "base";
  }
}

// Added with PF1 v10
function _migrateClassCasting(itemData, updateData) {
  const casting = itemData.system?.casting;
  if (!casting) return;

  if (!casting.type) {
    updateData["system.-=casting"] = null;
    return;
  }

  // domainSlots -> domain
  if (casting.domainSlots !== undefined) {
    updateData["system.casting.domain"] = casting.domain ?? casting.domainSlots ?? 1;
    updateData["system.casting.-=domainSlots"] = null;
  }
}

function _migrateWeaponCategories(ent, updateData) {
  if (ent.type !== "weapon") return;

  // Change category
  const type = ent.system.weaponType;
  if (type === "misc") {
    updateData["system.weaponType"] = "misc";
    updateData["system.weaponSubtype"] = "other";
  } else if (type === "splash") {
    updateData["system.weaponType"] = "misc";
    updateData["system.weaponSubtype"] = "splash";
  }

  const changeProp = ["simple", "martial", "exotic"].includes(type);
  if (changeProp && ent.system.weaponSubtype == null) {
    updateData["system.weaponSubtype"] = "1h";
  }

  // Change light property
  const lgt = foundry.utils.getProperty(ent.system, "properties.lgt");
  if (lgt != null) {
    updateData["system.properties.-=lgt"] = null;
    if (lgt === true && changeProp) {
      updateData["system.weaponSubtype"] = "light";
    }
  }

  // Change two-handed property
  const two = foundry.utils.getProperty(ent.system, "properties.two");
  if (two != null) {
    updateData["system.properties.-=two"] = null;
    if (two === true && changeProp) {
      updateData["system.weaponSubtype"] = "2h";
    }
  }

  // Change melee property
  const melee = foundry.utils.getProperty(ent.system, "weaponData.isMelee");
  if (melee != null) {
    updateData["system.weaponData.-=isMelee"] = null;
    if (melee === false && changeProp) {
      updateData["system.weaponSubtype"] = "ranged";
    }
  }
}

function _migrateArmorCategories(ent, updateData) {
  if (ent.type !== "equipment") return;

  const oldType = foundry.utils.getProperty(ent.system, "armor.type");
  if (oldType == null) return;

  if (oldType === "clothing") {
    updateData["system.equipmentType"] = "misc";
    updateData["system.equipmentSubtype"] = "clothing";
  } else if (oldType === "shield") {
    updateData["system.equipmentType"] = "shield";
    updateData["system.equipmentSubtype"] = "lightShield";
  } else if (oldType === "misc") {
    updateData["system.equipmentType"] = "misc";
    updateData["system.equipmentSubtype"] = "wondrous";
  } else if (["light", "medium", "heavy"].includes(oldType)) {
    updateData["system.equipmentType"] = "armor";
    updateData["system.equipmentSubtype"] = `${oldType}Armor`;
  }

  updateData["system.armor.-=type"] = null;
}

/**
 * Convert string armor max dex to number.
 *
 * Introduced with PF1 v10
 *
 * @param item
 * @param updateData
 */
function _migrateArmorMaxDex(item, updateData) {
  if (item.type !== "equipment") return;

  let maxDex = item.system.armor?.dex;
  // Skip valid states
  if (maxDex === undefined || maxDex === null) return;
  if (typeof maxDex === "number") return;

  // Convert string to number
  maxDex = parseInt(maxDex);
  if (Number.isInteger(maxDex)) {
    updateData["system.armor.dex"] = maxDex;
  }
  // Assume corrupt value otherwise
  else {
    updateData["system.armor.-=dex"] = null;
  }
}

function _migrateEquipmentCategories(item, updateData) {
  if (item.type !== "equipment") return;

  const subtype = updateData["system.subType"] ?? item.system.subType;
  if (subtype !== "misc") return;

  switch (item.system.equipmentSubtype) {
    case "wondrous":
      updateData["system.subType"] = "wondrous";
      updateData["system.-=equipmentSubtype"] = null;
      break;
    case "clothing":
      updateData["system.subType"] = "clothing";
      updateData["system.-=equipmentSubtype"] = null;
      break;
    case "other":
      updateData["system.subType"] = "other";
      updateData["system.-=equipmentSubtype"] = null;
      break;
  }
}

function _migrateSpellDescriptors(item, updateData) {
  if (item.type !== "spell" || item.system.types === undefined) return;

  const current = item.system.types
    .split(",")
    .flatMap((x) => x.split(";"))
    .filter((x) => x)
    .map((x) => x.trim());

  const value = [];
  const custom = [];
  const entries = Object.entries(pf1.config.spellDescriptors);
  current.forEach((c) => {
    const exists = entries.find(([k, v]) => c.toLowerCase() === k.toLowerCase() || c.toLowerCase() === v.toLowerCase());
    if (exists) {
      value.push(exists[0]);
    } else {
      custom.push(c);
    }
  });

  updateData["system.-=types"] = null;
  updateData["system.descriptors.value"] = value;
  updateData["system.descriptors.custom"] = custom.join("; ");
}

function _migrateSpellSubschool(item, updateData) {
  if (item.type !== "spell" || item.system.subschool === undefined || typeof item.system.subschool !== "string") return;

  // Split current custom string into options
  const customSubschools = item.system.subschool
    .split(",")
    .flatMap((x) => x.split(";"))
    .map((x) => x.trim())
    .filter((x) => x);

  // Attempt to parse custom entries
  const values = [];
  const entries = Object.entries(pf1.config.spellSubschools);
  ssLoop: for (const subschool of customSubschools) {
    for (const [ssId, ssLabel] of entries) {
      if (subschool in pf1.config.spellSubschools) {
        values.push(ssId);
        continue ssLoop;
      }
      const re = new RegExp(ssLabel, "i");
      if (re.test(subschool)) {
        values.push(ssId);
        continue ssLoop;
      }
    }
    values.push(subschool);
  }

  updateData["system.subschool"] = values;
}

function _migrateItemDefaultAmmo(itemData, updateData) {
  const ammoId = itemData.flags?.pf1?.defaultAmmo;
  if (!ammoId) return;
  if (!itemData.system?.ammo?.default) {
    updateData["system.ammo.default"] = ammoId;
  }
  updateData["flags.pf1.-=defaultAmmo"] = null;
}

function _migrateRaceItemCreatureType(itemData, updateData) {
  if (itemData.type !== "race") return;

  if (itemData.system.creatureType) {
    if (!itemData.system.creatureTypes?.length) updateData["system.creatureTypes"] = [itemData.system.creatureType];
    updateData["system.-=creatureType"] = null;
  }

  if (Array.isArray(itemData.system.subTypes)) {
    const subtypes = [];
    const oldTypes = itemData.system.subTypes.flat();
    for (const subtype of oldTypes) {
      if (!subtype) continue; // Should not happen
      const lowerCaseKey = subtype.charAt(0).toLowerCase() + subtype.slice(1).replace(" ", "");
      if (lowerCaseKey in pf1.config.creatureSubtypes) {
        subtypes.push(lowerCaseKey);
      } else {
        subtypes.push(subtype);
      }
    }
    if (!itemData.system.creatureSubtypes?.length) updateData["system.creatureSubtypes"] = subtypes;
    updateData["system.-=subTypes"] = null;
  }
}

function _migrateItemDurability(itemData, updateData) {
  const isPhysical = CONFIG.Item.documentClasses[itemData.type]?.isPhysical;
  if (!isPhysical) return;

  const max = itemData.system?.hp?.max;
  if (max !== undefined) {
    if (itemData.system?.hp?.base === undefined) {
      updateData["system.hp.base"] = Number(max ?? 10);
    }
    updateData["system.hp.-=max"] = null;
  }
  const value = itemData.system?.hp?.value;
  if (value !== undefined) {
    if (itemData.system?.hp?.offset === undefined) {
      // Attempt to provide offset from old data
      updateData["system.hp.offset"] = max > 0 ? value - max : 0;
    }
    updateData["system.hp.-=value"] = null;
  }
}

function _migrateItemSize(ent, updateData) {
  // Convert custom sizing in weapons
  if (ent.type === "weapon") {
    const wdSize = foundry.utils.getProperty(ent.system, "weaponData.size");
    if (wdSize) {
      // Move old
      updateData["system.size"] = wdSize;
      updateData["system.weaponData.-=size"] = null;
      return;
    }
  }

  const oldSize = ent.system.size;
  if (["spell", "class", "buff", "feat"].includes(ent.type)) {
    // Remove size from abstract items
    if (oldSize !== undefined) {
      updateData["system.-=size"] = null;
    }
  } else {
    // Add default size to everything else if not present
    if (oldSize === undefined) {
      updateData["system.size"] = "med";
    }
  }
}

function _migrateItemFeatAbilityTypes(itemData, updateData) {
  if (itemData.type !== "feat") return;

  const type = itemData.system.abilityType;
  // Convert "none" and other invalid values (e.g. null or "n/a") to "na"
  // Added with PF1 v10
  if (pf1.config.abilityTypes[type] === undefined) {
    updateData["system.abilityType"] = "na";
  }
}

function _migrateClassLevels(ent, updateData) {
  const level = ent.system.levels;
  if (typeof level === "number" && ent.system.level == null) {
    updateData["system.level"] = level;
    updateData["system.-=levels"] = null;
  }
}

function _migrateSavingThrowTypes(ent, updateData) {
  if (
    foundry.utils.getProperty(ent.system, "save.type") == null &&
    typeof foundry.utils.getProperty(ent.system, "save.description") === "string"
  ) {
    const desc = foundry.utils.getProperty(ent.system, "save.description");
    if (desc.match(/REF/i)) updateData["system.save.type"] = "ref";
    else if (desc.match(/FORT/i)) updateData["system.save.type"] = "fort";
    else if (desc.match(/WILL/i)) updateData["system.save.type"] = "will";
  }
}

function _migrateCR(ent, updateData) {
  // Migrate CR offset
  const crOffset = ent.system.crOffset;
  if (typeof crOffset === "number") {
    updateData["system.crOffset"] = crOffset.toString();
  }
}

function _migrateItemChanges(itemData, updateData) {
  // Migrate changes
  const changes = itemData.system.changes;
  if (Array.isArray(changes)) {
    const newChanges = [];
    for (const c of changes) {
      let cd;
      if (Array.isArray(c)) {
        cd = {
          formula: c[0],
          target: c[1],
          subTarget: c[2],
          type: c[3],
        };
      } else {
        cd = foundry.utils.deepClone(c); // Avoid mutating source data so diff works properly
      }
      // Value should not exist, yet it was added previously by using derived data for updates.
      delete cd.value;

      newChanges.push(new pf1.components.ItemChange(cd).toObject());
    }

    // Alter the changes list, but only if changes actually occurred. Bidirectional to detect deletions.
    if (
      !foundry.utils.isEmpty(foundry.utils.diffObject(changes, newChanges)) ||
      !foundry.utils.isEmpty(foundry.utils.diffObject(newChanges, changes))
    ) {
      updateData["system.changes"] = newChanges;
    }
  }

  const oldChanges = updateData["system.changes"] ?? itemData.system?.changes ?? [];
  const newChanges = [];
  let updateChanges = false;
  for (const change of oldChanges) {
    const cd = new pf1.components.ItemChange(foundry.utils.deepClone(change)).toObject(true, true);
    const diff = foundry.utils.diffObject(change, cd);
    if (!foundry.utils.isEmpty(diff)) {
      updateChanges = true;
    }

    // Replace targets with .subSkills. for ones without
    // @since PF1 v10
    if (/\.subSkills\./.test(cd.target)) {
      cd.target = cd.target.replace(".subSkills.", ".");
      updateChanges = true;
    }
    // Remove use of penalty bonus type
    // @since PF1 v10
    if (cd.type === "penalty") {
      // Convert the special ability score case to specific target
      if (["str", "dex", "con", "int", "wis", "cha"].includes(cd.target)) {
        cd.target = `${cd.target}Pen`;
      }
      // Convert all to untyped changes
      cd.type = "untyped";
      updateChanges = true;
    }
    newChanges.push(cd);
  }
  if (updateChanges) {
    updateData["system.changes"] = newChanges;
  }
}

function _migrateItemContextNotes(itemData, updateData) {
  // Migrate context notes
  const oldNotes = itemData.system.contextNotes;
  if (Array.isArray(oldNotes) && oldNotes?.length > 0) {
    const newNotes = [];

    for (const oldNote of oldNotes) {
      let newNote = foundry.utils.deepClone(oldNote);

      // Transform old tuple.
      if (Array.isArray(oldNote)) {
        newNote = { text: oldNote[0], target: oldNote[1], subTarget: oldNote[2] };
      }

      newNote = new pf1.components.ContextNote(newNote).toObject();

      newNotes.push(newNote);
    }

    // Alter the context note list, but only if changes actually occurred. Bidirectional to detect deletions.
    if (
      !foundry.utils.isEmpty(foundry.utils.diffObject(oldNotes, newNotes)) ||
      !foundry.utils.isEmpty(foundry.utils.diffObject(newNotes, oldNotes))
    ) {
      updateData["system.contextNotes"] = newNotes;
    }
  }

  const notes = updateData["system.contextNotes"] ?? oldNotes ?? [];
  let updateNotes = false;
  const newNotes = [];
  for (const note of notes) {
    const newNote = { ...note };
    // Replace targets with .subSkills. for ones without
    // @since PF1 v10
    if (/^skill\..+\.subSkills\..+$/.test(note.target)) {
      newNote.target = note.target.replace(".subSkills.", ".");
      updateNotes = true;
    }
    newNotes.push(newNote);
  }
  if (updateNotes) {
    updateData["system.contextNotes"] = newNotes;
  }
}

function _migrateItemChangeFlags(item, updateData) {
  const flags = item.system?.changeFlags;
  if (!flags) return;

  // Dwarf-like encumbrance to distinct no medium/heavy encumbrance
  if (flags.noEncumbrance !== undefined) {
    if (flags.noEncumbrance === true) {
      updateData["system.changeFlags.noMediumEncumbrance"] = true;
      updateData["system.changeFlags.noHeavyEncumbrance"] = true;
    }
    updateData["system.changeFlags.-=noEncumbrance"] = null;
  }
}

function _migrateEquipmentSize(ent, updateData) {
  if (ent.type !== "equipment") return;

  const size = ent.system.size;
  if (!size) {
    updateData["system.size"] = "med";
  }
}

// Migrate .weight number to .weight.value
// Migrate .baseWeight that was briefly introduced in 0.81
function _migrateItemWeight(ent, updateData) {
  const baseWeight = ent.system.baseWeight,
    weight = ent.system.weight;

  // Skip items of inappropriate type
  const isPhysical = CONFIG.Item.documentClasses[ent.type]?.isPhysical;
  if (!isPhysical) {
    if (weight !== undefined) {
      // Ensure inappropriate items don't have spurious weight, which breaks data prep
      updateData["system.-=weight"] = null;
    }
    return;
  }

  if (Number.isFinite(weight)) {
    updateData["system.weight.value"] = weight;
  } else if (weight == null || typeof weight !== "object") {
    // Convert any other values to just 0 weight, e.g. null
    updateData["system.weight.value"] = 0;
  }

  // If baseWeight exists and looks reasonable, use it for base weight instead
  if (baseWeight !== undefined) {
    if (Number.isFinite(baseWeight) && baseWeight > 0) {
      updateData["system.weight.value"] = baseWeight;
    }
    updateData["system.-=baseWeight"] = null;
  }
}

function _migrateItemHealth(item, updateData) {
  const isPhysical = CONFIG.Item.documentClasses[item.type]?.isPhysical;

  const hp = item.system.hp;
  if (isPhysical) {
    if (hp) {
      // Fix invalid data type
      if (typeof hp.max === "string") updateData["system.hp.max"] = parseInt(hp.max);
      if (typeof hp.value === "string") updateData["system.hp.value"] = parseInt(hp.value);
    } else {
      // Restore missing HP data
      updateData["system.hp.value"] = 10;
      updateData["system.hp.max"] = 10;
    }
  } else if (item.type !== "class" && hp !== undefined) {
    updateData["system.-=hp"] = null;
  }
}

function _migrateSpellCosts(ent, updateData) {
  if (ent.type !== "spell") return;

  const spellPointCost = foundry.utils.getProperty(ent.system, "spellPoints.cost");
  if (spellPointCost == null) {
    updateData["system.spellPoints.cost"] = "1 + @sl";
  }

  const slotCost = ent.system.slotCost;
  if (slotCost == null) {
    updateData["system.slotCost"] = 1;
  }

  const autoDeduct = ent.system.preparation?.autoDeductCharges;
  if (autoDeduct !== undefined) {
    if (autoDeduct === false) {
      updateData["system.uses.autoDeductChargesCost"] = "0";
    }
    updateData["system.preparation.-=autoDeductCharges"] = null;
  }
}

/**
 * Migrate spell preparation
 *
 * Added with PF1 v10
 *
 * @param {object} itemData
 * @param {object} updateData
 * @param {object} context
 * @param {Item} [context.item=null]
 */
function _migrateSpellPreparation(itemData, updateData, { item = null } = {}) {
  if (itemData.type !== "spell") return;

  const spellbook = item?.spellbook;
  const prepMode = spellbook?.spellPreparationMode || "prepared";
  const usesSpellPoints = spellbook?.spellPoints?.useSystem ?? false;
  const isPrepared = usesSpellPoints ? false : prepMode === "prepared";

  const prep = itemData.system.preparation ?? {};
  if (prep.maxAmount !== undefined) {
    if (!(prep.max > 0)) {
      // Migrate even older non number max amount
      if (typeof prep.maxAmount !== "number") prep.maxAmount = 0;
      updateData["system.preparation.max"] = prep.maxAmount ?? 0;
    }
    updateData["system.preparation.-=maxAmount"] = null;
  }
  if (prep.spontaneousPrepared !== undefined) {
    if (!(prep.value > 0) && !isPrepared) {
      updateData["system.preparation.value"] = prep.spontaneousPrepared ? 1 : 0;
    }
    updateData["system.preparation.-=spontaneousPrepared"] = null;
  }
  if (prep.preparedAmount !== undefined) {
    if (!(prep.value > 0) && isPrepared) {
      updateData["system.preparation.value"] = Math.max(
        prep.preparedAmount,
        updateData["system.preparation.value"] || 0
      );
    }
    updateData["system.preparation.-=preparedAmount"] = null;
  }
}

function _migrateLootEquip(ent, updateData) {
  if (ent.type === "loot" && typeof ent.system.equipped !== "boolean") {
    updateData["system.equipped"] = false;
  }
}

const _migrateItemLinks = async function (itemData, updateData, { item, actor }) {
  const linkData = itemData.system.links ?? {};
  for (const [linkType, oldLinks] of Object.entries(linkData)) {
    let updated = false;
    const links = foundry.utils.deepClone(oldLinks);
    for (const link of links) {
      const type = link.dataType;
      if (type !== undefined) {
        if (type === "data") {
          delete link.dataType;
        } else if (type === "world") {
          // Reconstruct world item UUID
          link.uuid = link.id.replace(/^world\./, "Item.");
          delete link.id;
          delete link.dataType;
        } else if (type === "compendium") {
          // Reconstruct compendium UUID
          link.uuid = `Compendium.${link.id}`;
          delete link.id;
          delete link.dataType;
        }
        delete link.img;
        updated = true;
      }

      // Convert ID to relative UUID
      if (link.id !== undefined && actor) {
        link.uuid = actor.items?.get(link.id)?.getRelativeUUID(actor);
        if (link.uuid) {
          delete link.id;
          updated = true;
        }
      }

      if (actor && link.uuid) {
        let linked = await fromUuid(link.uuid, { relative: actor });
        // Attempt to recover bad links to other actors
        if (linked?.actor) {
          // Attempt to adjust owned item
          if (linked.actor !== actor) linked = actor.items.get(linked.id);
          const newLink = linked?.getRelativeUUID(actor);
          // Successful recovery?
          if (linked && newLink !== link.uuid) {
            link.uuid = newLink;
            updated = true;
          }
        }
      }

      // Transform non-relative UUIDs to new format
      if (link.uuid?.[0] !== "." && link.uuid) {
        const uuid = pf1.utils.internal.uniformUuid(link.uuid, actor);
        if (link.uuid !== uuid) {
          link.uuid = uuid;
          updated = true;
        }
      }

      // Handle moved compendium content
      if (link.uuid) {
        const movedUuid = moved[link.uuid];
        if (movedUuid) {
          link.uuid = movedUuid;
          updated = true;
        }
      }

      // Remove unused data
      if (link._index !== undefined || link.hiddenLinks !== undefined) {
        delete link._index;
        delete link.hiddenLinks;
        updated = true;
      }

      // Clear out invalid links with just a name or no info at all
      const keys = Object.keys(link);
      if (keys.length < 1 || (keys.length === 1 && keys.includes("name"))) {
        link._delete = true;
        updated = true;
      }
    }

    if (updated) {
      updateData[`system.links.${linkType}`] = links.filter((i) => i._delete !== true);
    }
  }
};

function _migrateItemProficiencies(item, updateData) {
  // Added with PF1 v10
  // Migrate sim/mar to simple/martial
  const wprofmap = {
    sim: "simple",
    mar: "martial",
  };

  const oldKeys = Object.keys(wprofmap);
  if (item.system.weaponProf?.value?.some((p) => oldKeys.includes(p))) {
    const nwprof = item.system.weaponProf.value.map((p) => wprofmap[p] || p);
    updateData["system.weaponProf.value"] = nwprof;
  }
}

function _migrateItemNotes(ent, updateData) {
  const list = ["system.attackNotes", "system.effectNotes"];
  for (const k of list) {
    const value = foundry.utils.getProperty(ent, k);
    const hasValue = foundry.utils.hasProperty(ent, k);
    if (hasValue && !(value instanceof Array)) {
      updateData[k] = [];
      if (typeof value === "string" && value.length > 0) {
        updateData[k] = value.trim().split(/[\n\r]/);
      }
    }
  }
}

function _migrateItemActionToActions(itemData, updateData, { actor, item }) {
  const hasOldAction =
    !!itemData.system.actionType || !!itemData.system.activation?.type || !!itemData.system.measureTemplate;

  // Cleanup old data
  if (itemData.system.actionType !== undefined) updateData["system.-=actionType"] = null;
  if (itemData.system.activation !== undefined) updateData["system.-=activation"] = null;
  if (itemData.system.measureTemplate !== undefined) updateData["system.-=measureTemplate"] = null;

  // Already has actions
  if (itemData.system.actions?.length) return;

  // Proceed with old action cleanup

  // The following is very blunt force migration that includes lots of bad data that shouldn't be incldued
  if (!hasOldAction) return;

  // Transfer data to an action
  const actionData = {};
  for (const k of Object.keys(itemData.system)) {
    actionData[k] = itemData.system[k];
  }

  // Transfer name and image
  if (["weapon", "attack"].includes(itemData.type)) {
    actionData.name = game.i18n.localize("PF1.Attack");
  } else {
    actionData.name = game.i18n.localize("PF1.Use");
  }

  // Clear description
  delete actionData.description;

  // Add spell data
  // Make sure it has an activation type
  actionData.activation ??= {};
  actionData.activation.type ||= "standard";
  actionData.activation.unchained ??= {};
  actionData.activation.unchained.type ||= "action";
  actionData.activation.unchained.cost ??= 2;

  if (itemData.type === "spell") {
    // Transfer spell duration
    actionData.duration ??= {};
    actionData.duration.value = itemData.system.spellDuration;

    // Transfer spell point cost
    if (actor) {
      const spellbookKey = itemData.system.spellbook;
      const spellbook = actor.system.attributes?.spells?.spellbooks?.[spellbookKey];
      if (spellbook.spellPoints?.useSystem) {
        const oldSpellPointCostFormula = itemData.system.spellPoints?.cost;
        if (oldSpellPointCostFormula) actionData.uses.autoDeductChargesCost = oldSpellPointCostFormula;
      }
    }
  }

  // Fix power attack multiplier being non-number
  const paMult = actionData.powerAttack?.multiplier;
  if (typeof paMult === "string") {
    if (paMult === "") delete actionData.powerAttack.multiplier;
    else actionData.powerAttack.multiplier = parseInt(paMult);
  }

  // Clean out old attack and effect notes
  updateData["system.attackNotes"] = [];
  updateData["system.effectNotes"] = [];

  updateData["system.actions"] = [new pf1.components.ItemAction(actionData).toObject()];
}

function _migrateScriptCalls(item, updateData) {
  if (!(item.system.scriptCalls?.length > 0)) return;
  let updated = false;

  // Clear out unused name and image for linked macros.
  const scripts = foundry.utils.deepClone(item.system.scriptCalls);
  for (const script of scripts) {
    if (script.type == "macro") {
      if (script.name || script.img) {
        script.name = "";
        script.img = "";
        updated = true;
      }
    }
  }

  if (updated) {
    updateData["system.scriptCalls"] = scripts;
  }
}

/**
 * Convert tuple learnedAt values into key:value pairs directly in the object.
 *
 * @param item
 * @param updateData
 */
function _migrateItemLearnedAt(item, updateData) {
  const learnedAt = item.system.learnedAt ?? {};
  for (const [category, value] of Object.entries(learnedAt)) {
    if (Array.isArray(value)) {
      updateData[`system.learnedAt.${category}`] = value.reduce((learned, [classId, level]) => {
        // Skip invalid entries
        if (typeof classId !== "string" || classId.length == 0) return learned;
        // Split combined entries and transform them to object format
        for (let clsId of classId.split("/")) {
          clsId = clsId.trim().replace(".", "-"); // Sanitize
          if (clsId) learned[clsId] = level;
        }
        return learned;
      }, {});
    }
  }
}

/**
 * Migrate action..
 * - ... usesAmmo boolean away
 * - ... ammoType to item.system.ammo.type
 *
 * @param {object} action - Action data
 * @param {object} itemData - Parent item data
 * @param {object} updateData - Item update data
 */
function _migrateActionAmmunitionUsage(action, itemData, updateData) {
  if (action.usesAmmo === false) {
    delete action.ammoType;
  }
  if (action.usesAmmo === true) {
    if (!itemData.system.ammo?.type && !updateData["system.ammo.type"]) {
      updateData["system.ammo.type"] = action.ammoType;
      action.ammoType = ""; // Inherit from item
    }

    // Same as base item
    if (itemData.system.ammo?.type == action.ammoType) delete action.ammoType;
  }

  // Migrate .ammoType to .ammo.type
  if (action.ammoType) {
    action.ammo ??= {};
    action.ammo.type = action.ammoType;
  }
  delete action.ammoType;

  // Delete empty ammo type (inherited)
  if (action.ammo && !action.ammo.type) {
    delete action.ammo.type;
  }

  // Uses ammo is no longer used
  delete action.usesAmmo;
}

// Migrate harmless from save descriptor to the harmless toggle.
// Added with PF1 v10
function _migrateActionHarmlessSpell(action, itemData) {
  if (!action.save?.description) return;

  if (/\bharmless\b/.test(action.save.description)) {
    action.save.description = action.save.description
      .replace(/\s*\bharmless\b\s*/, "")
      .replace(/\(,\s*/, "(")
      .replace(/\s*,\)/, ")")
      .replace("()", "")
      .trim();
    action.save.harmless = true;
  }
}

// Action duration
// Added with PF1 v10
function _migrateActionDuration(action, itemData) {
  action.duration ??= {};

  // Something has caused "null" string durations for some people, this clears it.
  if (action.duration.value === "null") action.duration.value = "";

  // Swap units to "special" if undefined and formula exists
  if (!action.duration.units && !!action.duration.value) {
    action.duration.units = "spec";
  }

  // Swap "instantaneous" formula to instantaneous unit
  if (action.duration.value === "instantaneous") {
    delete action.duration.value;
    action.duration.units = "inst";
  }

  // Convert easy special values to actual duration info
  if (action.duration.units === "spec") {
    const value = action.duration.value || "";

    switch (value) {
      case "1 round":
      case "1 full round":
        action.duration.value = "1";
        action.duration.units = "round";
        break;
      case "1 min.":
      case "1 minute":
        action.duration.value = "1";
        action.duration.units = "minute";
        break;
      case "1 hour":
        action.duration.value = "1";
        action.duration.units = "hour";
        break;
      case "8 hours":
        action.duration.value = "8";
        action.duration.units = "hour";
        break;
      case "24 hours":
        action.duration.value = "24";
        action.duration.units = "hour";
        break;
      case "1 day":
        action.duration.value = "1";
        action.duration.units = "day";
        break;
      case "permanent":
        delete action.duration.value;
        action.duration.units = "perm";
        break;
      case "see below":
      case "see text":
        delete action.duration.value;
        action.duration.units = "seeText";
        break;
    }
  }
}

function _migrateActionMaterials(action, itemData) {
  let addons = action.material?.addon;
  if (addons) {
    // Since PF1 v10.5
    if (addons.some((ma) => !!materialChanges[ma])) {
      action.material.addon = action.material.addon.map((ma) => materialChanges[ma] || ma);
      addons = action.material.addon; // Ensure following code gets updated addons
    }

    // Convert Throneglass into non-addon material
    // Since PF1 v10.3
    const tg = "throneglass";
    if (addons.includes(tg)) {
      action.material.addon = action.material.addon.filter((ma) => ma !== tg);
      action.material.normal.value ||= tg;
    }
  }

  // Since PF1 v10.5
  const newMat = materialChanges[action.material?.normal?.value];
  if (newMat) action.material.normal.value = newMat;
}

/**
 * Added with PF1 v10
 *
 * @param {object} action
 * @param {object} itemData
 */
function _migrateActionExtraAttacks(action, itemData) {
  // Convert tuples into objects
  if (action.attackParts?.length) {
    const parts = action.attackParts ?? [];
    if (parts.some((p) => Array.isArray(p))) {
      action.attackParts = parts.map((part) => (Array.isArray(part) ? { formula: part[0], name: part[1] } : part));
    }

    // Ensure formulas are strings
    for (const part of action.attackParts) part.formula = `${part.formula}`;
  }

  // Unify extra attacks structure
  action.extraAttacks ??= {};

  if (action.attackParts !== undefined) {
    action.extraAttacks.manual = action.attackParts ?? [];
    delete action.attackParts;
  }

  if (action.formulaicAttacks !== undefined) {
    action.extraAttacks.formula ??= {};
    action.extraAttacks.formula.count = action.formulaicAttacks?.count?.formula || "";
    action.extraAttacks.formula.bonus = action.formulaicAttacks?.bonus?.formula || "";
    action.extraAttacks.formula.label = action.formulaicAttacks?.label || "";
    delete action.formulaicAttacks;
  }

  if (!action.extraAttacks.type) {
    // Convert existing formulas to standard options
    if (
      action.extraAttacks.formula?.count === "min(3, ceil(@attributes.bab.total / 5) - 1)" &&
      action.extraAttacks.formula?.bonus === "@formulaicAttack * -5"
    ) {
      action.extraAttacks.type = "standard";
      delete action.extraAttacks.formula.count;
      delete action.extraAttacks.formula.bonus;
      delete action.extraAttacks.formula.label;

      if (action.extraAttacks.manual?.length) {
        action.extraAttacks.type = "advanced";
      } else {
        delete action.extraAttacks.manual;
      }
    } else {
      if (action.extraAttacks.formula?.count?.length || action.extraAttacks.manual?.length) {
        action.extraAttacks.type = "custom";
      }
    }

    // Delete unused data
    if (!action.extraAttacks.formula?.count) delete action.extraAttacks?.formula?.count;
    if (!action.extraAttacks.formula?.bonus) delete action.extraAttacks?.formula?.bonus;
    if (!action.extraAttacks.formula?.label) delete action.extraAttacks?.formula?.label;
    if (!(action.extraAttacks.manual?.length > 0)) delete action.extraAttacks?.manual;
  }

  if (foundry.utils.isEmpty(action.extraAttacks.formula)) {
    delete action.extraAttacks.formula;
  }
}

function _migrateItemChargeCost(item, updateData) {
  const toggle = item.system.uses?.autoDeductCharges;
  if (toggle !== undefined) {
    // Mimic old setting by setting cost to 0
    if (toggle === false) {
      updateData["system.uses.autoDeductChargesCost"] = "0";
    }
    updateData["system.uses.-=autoDeductCharges"] = null;
  }
  // Special handling for cantrips if the above didn't match
  else if (item.system.level === 0 && item.system.uses?.autoDeductChargesCost === undefined) {
    const defaultAction = item.system.actions?.[0];
    // Check for presence of obsoleted autoDeductCharges in first action
    if (
      defaultAction?.uses?.autoDeductCharges === true &&
      updateData["system.uses.autoDeductChargesCost"] === undefined
    ) {
      updateData["system.uses.autoDeductChargesCost"] = "0";
    }
  }
}

// Added with PF1 v10
function _migrateItemLimitedUses(itemData, updateData) {
  // Migrate unlimited to empty selection, as the two are identical in meaning
  if (itemData.system.uses?.per === "unlimited") {
    updateData["system.uses.per"] = "";
  }

  // Only physical items have single use, convert use cases to 1 charge
  const isPhysical = CONFIG.Item.documentClasses[itemData.type]?.isPhysical;
  if (!isPhysical) {
    if (itemData.system.uses?.per === "single") {
      updateData["system.uses.per"] = "charges";
      updateData["system.uses.maxFormula"] = "1";
    }
  }
}

/**
 * @param {object} action - Action data
 * @param {object} itemData - Parent item data
 */
function _migrateActionDamageType(action, itemData) {
  if (!action.damage) return;

  // Determine data paths using damage types
  const damageGroupPaths = ["parts", "critParts", "nonCritParts"];
  for (const damageGroupKey of damageGroupPaths) {
    if (!action.damage[damageGroupKey]) continue;

    // v9 migration of array damage format
    action.damage[damageGroupKey] = action.damage[damageGroupKey].map((part) => {
      if (Array.isArray(part)) {
        const [formula, type] = part;
        return { formula, type };
      }
      return part;
    });

    // Other old data format changes
    const damageGroup = action.damage[damageGroupKey];
    for (const damagePart of damageGroup) {
      const damageType = damagePart.type;
      if (!damageType) continue;

      // Convert damage types
      if (typeof damageType === "string") {
        damagePart.types = _Action_ConvertDamageType(damageType);
        if (!damagePart.types.length) damagePart.types = damageType.split(";");
        delete damagePart.type;
      }
      // Move existing array
      else if (damageType instanceof Array) {
        damagePart.types = damageType;
        delete damagePart.type;
      }
    }
  }
}

/**
 * @param {object} actionData - Action data
 * @param {object} itemData - Parent item data
 * @param {object} options
 * @param {pf1.components.ItemAction} [options.action]
 */
function _migrateActionConditionals(actionData, itemData, { action } = {}) {
  for (const conditional of actionData.conditionals ?? []) {
    // Create conditional ID
    if (!conditional._id) conditional._id = foundry.utils.randomID(16);

    conditional.modifiers ??= [];
    if (!Array.isArray(conditional.modifiers)) {
      conditional.modifiers = Object.values(conditional.modifiers);
    }

    for (const modifier of conditional.modifiers) {
      // Create modifier ID
      if (!modifier._id) modifier._id = foundry.utils.randomID(16);

      // Ensure subTarget exists
      modifier.subTarget ??= "";

      let reResult;
      // Convert modifier subtarget
      if ((reResult = modifier.subTarget.match(/^attack\.([0-9]+)/))) {
        modifier.subTarget = `attack_${reResult[1]}`;
      }

      // Remove excess sheet data that was previously incorrectly added
      delete modifier.targets;
      delete modifier.subTargets;
      delete modifier.conditionalModifierTypes;
      delete modifier.conditionalCritical;

      // Convert modifier damage type
      if (modifier.target === "damage" && !modifier.damageType?.length && modifier.type) {
        modifier.damageType = _Action_ConvertDamageType(modifier.type);
        if (!modifier.damageType.length) modifier.damageType = modifier.type.split(";");
        modifier.type = "";
      }
    }
  }

  // Filter through the datamodel
  actionData.conditionals = actionData.conditionals.map((c) =>
    new pf1.components.ItemConditional(c).toObject(true, true)
  );
}

function _migrateActorCR(ent, updateData) {
  // Migrate base CR
  const cr = foundry.utils.getProperty(ent.system, "details.cr");
  if (typeof cr === "number") {
    updateData["system.details.cr.base"] = cr;
  } else if (cr == null) {
    updateData["system.details.cr.base"] = 1;
  }

  // Remove derived data if present
  if (foundry.utils.getProperty(ent.system, "details.cr.total") !== undefined) {
    updateData["system.details.cr.-=total"] = null;
  }
}

function _migrateAttackAbility(ent, updateData) {
  const cmbAbl = foundry.utils.getProperty(ent.system, "attributes.cmbAbility");
  if (cmbAbl === undefined) updateData["system.attributes.cmbAbility"] = "str";

  const meleeAbl = foundry.utils.getProperty(ent.system, "attributes.attack.meleeAbility");
  if (meleeAbl === undefined) updateData["system.attributes.attack.meleeAbility"] = "str";

  const rangedAbl = foundry.utils.getProperty(ent.system, "attributes.attack.rangedAbility");
  if (rangedAbl === undefined) updateData["system.attributes.attack.rangedAbility"] = "dex";
}

function _migrateActorSpellbookUsage(ent, updateData) {
  const spellbookUsage = foundry.utils.getProperty(ent.system, "attributes.spells.usedSpellbooks");
  if (spellbookUsage !== undefined) {
    updateData["system.attributes.spells.-=usedSpellbooks"] = null;
  }
}

function _migrateActorNullValues(ent, updateData) {
  // Prepare test data
  const entries = { "system.attributes.energyDrain": foundry.utils.getProperty(ent.system, "attributes.energyDrain") };
  for (const [k, a] of Object.entries(ent.system.abilities || {})) {
    entries[`system.abilities.${k}.damage`] = a.damage;
    entries[`system.abilities.${k}.drain`] = a.drain;
    entries[`system.abilities.${k}.penalty`] = a.penalty;
  }

  // Set null values to 0
  for (const [k, v] of Object.entries(entries)) {
    if (v === null) {
      updateData[k] = 0;
    }
  }
}

function _migrateActorSpellbookDomainSlots(ent, updateData) {
  const spellbooks = foundry.utils.getProperty(ent.system, "attributes.spells.spellbooks") || {};

  for (const [k, b] of Object.entries(spellbooks)) {
    if (b.domainSlotValue !== undefined) continue;
    const key = `system.attributes.spells.spellbooks.${k}.domainSlotValue`;
    updateData[key] = 1;
  }
}

function _migrateActorStatures(ent, updateData) {
  const stature = foundry.utils.getProperty(ent.system, "traits.stature");

  if (stature === undefined) {
    updateData["system.traits.stature"] = "tall";
  }
}

// Migrate weapon proficiencies
// Converts sim and mar to simple and martial
// Added with PF1 v10
function _migrateActorProficiencies(actorData, updateData) {
  const wprofs = updateData["system.traits.weaponProf"] ?? actorData.system.traits?.weaponProf;
  if (!wprofs?.length) return;

  const wprofmap = {
    sim: "simple",
    mar: "martial",
  };

  if (wprofs.some((p) => !!wprofmap[p])) {
    const nwprofs = wprofs.map((p) => wprofmap[p] || p);
    updateData["system.traits.weaponProf"] = nwprofs;
  }
}

function _migrateActorDefenseAbility(ent, updateData) {
  const normalACAbl = foundry.utils.getProperty(ent.system, "attributes.ac.normal.ability");
  if (normalACAbl === undefined) updateData["system.attributes.ac.normal.ability"] = "dex";
  const touchACAbl = foundry.utils.getProperty(ent.system, "attributes.ac.touch.ability");
  if (touchACAbl === undefined) updateData["system.attributes.ac.touch.ability"] = "dex";

  // CMD
  const cmdDexAbl = foundry.utils.getProperty(ent.system, "attributes.cmd.dexAbility");
  if (cmdDexAbl === undefined) updateData["system.attributes.cmd.dexAbility"] = "dex";
  const cmdStrAbl = foundry.utils.getProperty(ent.system, "attributes.cmd.strAbility");
  if (cmdStrAbl === undefined) updateData["system.attributes.cmd.strAbility"] = "str";
}

function _migrateActorInitAbility(ent, updateData) {
  const abl = foundry.utils.getProperty(ent.system, "attributes.init.ability");

  if (abl === undefined) {
    updateData["system.attributes.init.ability"] = "dex";
  }
}

function _migrateActorCMBRevamp(ent, updateData) {
  if (foundry.utils.getProperty(ent.system, "attributes.cmb.total") !== undefined) {
    updateData["system.attributes.cmb.-=total"] = null;
  }
}

function _migrateActorChangeRevamp(ent, updateData) {
  // Skills
  Object.keys(ent.system.skills ?? {}).forEach((s) => {
    const path = `system.skills.${s}.`;
    if (foundry.utils.getProperty(ent, path + "changeBonus") !== undefined) {
      updateData[path + "-=changeBonus"] = null;
    }

    // Check for subskill
    Object.keys(foundry.utils.getProperty(ent, `system.skills.${s}.subSkills`) ?? {}).forEach((s2) => {
      const subPath = `system.skills.${s}.subSkills.${s2}.`;
      if (foundry.utils.getProperty(ent, subPath + "changeBonus") !== undefined) {
        updateData[subPath + "-=changeBonus"] = null;
      }
    });
  });

  // Remove derived data
  const derivedKeys = {
    "system.attributes.hp.max": "attributes.hp.-=max",
    "system.attributes.ac.normal.total": "attributes.ac.normal.-=total",
    "system.attributes.ac.touch.total": "attributes.ac.touch.-=total",
    "system.attributes.ac.flatFooted.total": "attributes.ac.flatFooted.-=total",
    "system.attributes.cmd.total": "attributes.cmd.-=total",
    "system.attributes.cmd.flatFootedTotal": "attributes.cmd.-=flatFootedTotal",
    "system.attributes.sr.total": "attributes.sr.-=total",
    "system.attributes.init.total": "attributes.init.-=total",
  };

  Object.entries(derivedKeys).forEach(([key, updateKey]) => {
    if (foundry.utils.getProperty(ent, key) !== undefined) {
      updateData["system." + updateKey] = null;
    }
  });
}

function _migrateActorInvaliddSkills(actor, updateData) {
  const skills = actor.system.skills;
  if (!skills) return;
  for (const [key, sklData] of Object.entries(skills)) {
    if (!sklData) {
      updateData[`system.skills.-=${key}`] = null;
      continue;
    }
    for (const [subKey, subSklData] of Object.entries(sklData.subSkills ?? {})) {
      if (!subSklData) {
        updateData[`system.skills.${key}.subSkills.-=${subKey}`] = null;
      }
    }
  }
}

/**
 * Migrate abnormal skill rank values to 0.
 * Primarily changing nulls to 0 to match new actors.
 *
 * @param ent
 * @param updateData
 */
function _migrateActorSkillRanks(ent, updateData) {
  const skills = ent.system.skills;
  if (!skills) return; // Unlinked with no skill overrides of any kind
  for (const [key, sklData] of Object.entries(skills)) {
    if (!sklData) continue;
    if (!Number.isFinite(sklData.rank)) updateData[`system.skills.${key}.rank`] = 0;
    for (const [subKey, subSklData] of Object.entries(sklData.subSkills ?? {})) {
      if (!subSklData) continue;
      if (!Number.isFinite(subSklData.rank)) updateData[`system.skills.${key}.subSkills.${subKey}.rank`] = 0;
    }
  }
}

function _migrateCarryBonus(ent, updateData) {
  if (foundry.utils.getProperty(ent.system, "details.carryCapacity.bonus.user") === undefined) {
    let bonus = foundry.utils.getProperty(ent.system, "abilities.str.carryBonus");
    if (bonus !== undefined) {
      bonus = bonus || 0;
      updateData["system.details.carryCapacity.bonus.user"] = bonus;
    }
    updateData["system.abilities.str.-=carryBonus"] = null;
  }
  if (foundry.utils.getProperty(ent.system, "details.carryCapacity.multiplier.user") === undefined) {
    let mult = foundry.utils.getProperty(ent.system, "abilities.str.carryMultiplier");
    if (mult !== undefined) {
      mult = mult || 1;
      updateData["system.details.carryCapacity.multiplier.user"] = mult - 1;
    }
    updateData["system.abilities.str.-=carryMultiplier"] = null;
  }
}

function _migrateBuggedValues(ent, updateData) {
  // Convert to integers
  const convertToInt = [
    "system.details.xp.value",
    "system.currency.pp",
    "system.currency.gp",
    "system.currency.sp",
    "system.currency.cp",
    "system.altCurrency.pp",
    "system.altCurrency.gp",
    "system.altCurrency.sp",
    "system.altCurrency.cp",
  ];
  for (const key of convertToInt) {
    const oldValue = foundry.utils.getProperty(ent, key),
      value = parseInt(oldValue ?? 0);
    if (oldValue !== value) {
      updateData[key] = value;
    }
  }
}

function _migrateSpellbookUsage(ent, updateData) {
  const usedSpellbooks = ent.items
    .filter((i) => i.type === "spell")
    .reduce((cur, i) => {
      if (!i.system.spellbook) return cur;
      if (cur.includes(i.system.spellbook)) return cur;
      cur.push(i.system.spellbook);
      return cur;
    }, []);

  for (const bookKey of usedSpellbooks) {
    const path = `system.attributes.spells.spellbooks.${bookKey}.inUse`;
    if (foundry.utils.getProperty(ent, path) !== true) {
      updateData[path] = true;
    }
  }
}

function _migrateActorHP(ent, updateData) {
  // Migrate HP, Wounds and Vigor values from absolutes to relatives, which is a change in 0.80.16
  for (const k of ["system.attributes.hp", "system.attributes.wounds", "system.attributes.vigor"]) {
    const value = foundry.utils.getProperty(ent, `${k}.value`);

    // Fill offset if missing
    if (foundry.utils.getProperty(ent, `${k}.offset`) == null) {
      const max = foundry.utils.getProperty(ent, `${k}.max`) ?? 0;
      updateData[`${k}.offset`] = (value ?? 0) - max;
    }
    // Value is no longer used if it exists

    if (value !== undefined) {
      updateData[`${k}.-=value`] = null;
    }
  }
}

function _migrateActorSenses(ent, updateData, token) {
  const oldSenses = ent.system.traits?.senses;

  if (!oldSenses) return;

  if (typeof oldSenses === "string") {
    const tokenData = token ?? ent.prototypeToken;

    updateData["system.traits.senses"] = {
      dv: { value: tokenData.brightSight },
      ts: { value: 0 },
      bs: { value: 0 },
      bse: { value: 0 },
      ll: {
        enabled: tokenData.flags?.pf1?.lowLightVision,
        multiplier: {
          dim: tokenData.flags?.pf1?.lowLightVisionMultiplier ?? 2,
          bright: tokenData.flags?.pf1?.lowLightVisionMultiplierBright ?? 2,
        },
      },
      sid: false,
      tr: { value: 0 },
      si: false,
      sc: { value: 0 },
      custom: oldSenses,
    };
  }

  // Migrate boolean Scent sense to number
  if (typeof oldSenses?.sc === "boolean") {
    updateData["system.traits.senses.sc"] = { value: oldSenses.sc ? 30 : 0 };
  }

  // Migrate boolean true seeing to number
  if (typeof oldSenses?.tr === "boolean") {
    updateData["system.traits.senses.tr"] = { value: oldSenses.tr ? 120 : 0 };
  }

  for (const id of ["dv", "ts", "ls", "bs", "bse", "sc", "tr"]) {
    const sense = oldSenses?.[id];
    if (foundry.utils.getType(sense) !== "Object") {
      updateData[`system.traits.senses.${id}`] = { value: sense || 0 };
    }
  }
}

function _migrateActorSkillJournals(ent, updateData) {
  const reOldJournalFormat = /^[a-zA-Z0-9]+$/;
  for (const [skillKey, sklData] of Object.entries(ent.system.skills ?? {})) {
    if (!sklData) continue;
    for (const [subSkillKey, subSklData] of Object.entries(sklData.subSkills ?? {})) {
      if (!subSklData) continue;
      if (subSklData.journal?.match(reOldJournalFormat)) {
        updateData[`system.skills.${skillKey}.subSkills.${subSkillKey}.journal`] = `JournalEntry.${subSklData.journal}`;
      }
    }

    if (sklData.journal?.match(reOldJournalFormat)) {
      updateData[`system.skills.${skillKey}.journal`] = `JournalEntry.${sklData.journal}`;
    }
  }
}

function _migrateActorSubskillData(actor, updateData) {
  for (const [skillId, skillData] of Object.entries(actor.system.skills ?? {})) {
    if (!skillData) continue;
    for (const [subSkillId, subSkillData] of Object.entries(skillData.subSkills ?? {})) {
      if (!subSkillData) continue;
      if (subSkillData.mod !== undefined) {
        // Remove permanently stored .mod which is derived value
        // Added with PF1 v9
        updateData[`system.skills.${skillId}.subSkills.${subSkillId}.-=mod`] = null;
      }
    }
  }
}

function _migrateActorDRandER(ent, updateData) {
  const oldDR = ent.system.traits?.dr;
  const oldER = ent.system.traits?.eres;

  if (typeof oldDR === "string") {
    updateData["system.traits.dr"] = {
      value: [],
      custom: oldDR,
    };
  }

  if (typeof oldER === "string") {
    updateData["system.traits.eres"] = {
      value: [],
      custom: oldER,
    };
  }
}

/**
 * @param trait
 * @since PF1 v11
 */
function _migrateAnyTrait(trait) {
  if (!trait) return null; // Missing
  if (Array.isArray(trait)) return null; // Already in final format

  const all = new Set();

  // Pull standard choices
  if (Array.isArray(trait.value) && trait.value.length) {
    trait.value.forEach((v) => all.add(v));
  }
  // Convert older plain string custom traits to array
  if (typeof trait.custom == "string") {
    trait.custom =
      trait.custom
        ?.split(pf1.config.re.traitSeparator)
        .map((x) => x.trim())
        .filter((x) => x) ?? [];
  }
  // Pull custom choices
  if (Array.isArray(trait.custom) && trait.custom.length) {
    trait.custom.forEach((v) => all.add(v));
  }

  return [...all];
}

function _migrateActorTraits(actor, updateData) {
  const keys = ["di", "dv", "ci", "languages", "armorProf", "weaponProf"];

  for (const key of keys) {
    const trait = actor.system.traits?.[key];
    if (!trait) continue; // Missing
    if (Array.isArray(trait)) continue; // Already in final format

    const selections = _migrateAnyTrait(trait);
    if (selections) updateData[`system.traits.${key}`] = selections;
  }
}

/**
 * @param itemData
 * @param updateData
 */
function _migrateItemTraits(itemData, updateData) {
  const keys = [
    "armorProf",
    "descriptors",
    "subschool",
    "languages",
    "weaponGroups",
    "weaponProf",
    "creatureTypes",
    "creatureSubtypes",
    "conditions",
  ];

  for (const key of keys) {
    const trait = itemData.system?.[key];
    if (!trait) continue; // Missing
    if (Array.isArray(trait)) continue; // Already in final format

    const selections = _migrateAnyTrait(trait);
    if (selections) updateData[`system.${key}`] = selections;
  }
}

/**
 * @param actorData
 * @param updateData
 * @since PF1 v10
 */
function _migrateActorFlags(actorData, updateData) {
  const flags = actorData.flags?.pf1;
  if (!flags) return;

  // visionPermission to visionSharing
  if (flags.visionPermission) {
    updateData["flags.pf1.visionSharing.default"] = flags.visionPermission.default === "yes" ? true : false;
    const mapping = {
      yes: true,
      no: false,
      default: null,
    };
    updateData["flags.pf1.visionSharing.users"] = Object.fromEntries(
      Object.entries(flags.visionPermission?.users ?? {}).map(([uid, data]) => [uid, mapping[data.level] ?? null])
    );
    updateData["flags.pf1.-=visionPermission"] = null;
  }
}

function _Action_ConvertDamageType(damageTypeString) {
  const separator = /(?:\s*\/\s*|\s+and\s+|\s+or\s+)/i;
  const damageTypeList = [
    {
      tests: ["b", "blunt", "bludgeoning"],
      result: "bludgeoning",
    },
    {
      tests: ["p", "pierce", "piercing"],
      result: "piercing",
    },
    {
      tests: ["s", "slash", "slashing"],
      result: "slashing",
    },
    {
      tests: ["f", "fire"],
      result: "fire",
    },
    {
      tests: ["cold", "c"],
      result: "cold",
    },
    {
      tests: ["e", "electric", "electricity", "electrical"],
      result: "electric",
    },
    {
      tests: ["a", "acid"],
      result: "acid",
    },
    {
      tests: ["sonic"],
      result: "sonic",
    },
    {
      tests: ["force"],
      result: "force",
    },
    {
      tests: ["neg", "negative"],
      result: "negative",
    },
    {
      tests: ["pos", "positive"],
      result: "positive",
    },
    {
      tests: ["u", "untyped", "untype"],
      result: "untyped",
    },
  ];

  const damageTypes = damageTypeString.split(separator).map((o) => o.toLowerCase());
  const result = [];
  for (const damageTest of damageTypeList) {
    for (const testString of damageTest.tests) {
      if (damageTypes.includes(testString)) {
        result.push(damageTest.result);
      }
    }
  }

  return result;
}

function _migrateContainerPrice(item, updateData) {
  if (item.type !== "container") return;

  // .basePrice was merged into .price with PF1 v9
  if (item.system.basePrice !== undefined) {
    updateData["system.price"] = item.system.basePrice;
    updateData["system.-=basePrice"] = null;
  }
  if (item.system.unidentified?.basePrice !== undefined) {
    updateData["system.unidentified.price"] = item.system.unidentified.basePrice;
    updateData["system.unidentified.-=basePrice"] = null;
  }
}

function _migrateContainerReduction(item, updateData) {
  if (item.type !== "container") return;
  if (item.system.weightReduction !== undefined) {
    updateData["system.weight.reduction.percent"] = item.system.weightReduction;
    updateData["system.-=weightReduction"] = null;
  }
}

function _migrateItemType(ent, updateData) {
  const type = ent.type;
  const oldType = ent.system[`${type}Type`];
  if (oldType == null) return;
  updateData["system.subType"] = oldType;
  updateData[`system.-=${type}Type`] = null;
}

/**
 * @param {object} itemData
 * @param {object} updateData
 * @since PF1 v10
 */
function _migrateItemFlags(itemData, updateData) {
  if (!itemData.flags?.pf1) return;

  if (itemData.flags.pf1.abundant !== undefined) {
    if (itemData.system.abundant === undefined) {
      updateData["system.abundant"] = Boolean(itemData.flags.pf1.abundant);
    }
    updateData["flags.pf1.-=abundant"] = null;
  }
}

function _migrateItemMaterials(itemData, updateData) {
  for (const materialPath of ["material", "armor.material"]) {
    const material = foundry.utils.getProperty(itemData.system, materialPath);
    if (!material) continue;

    // Convert incorrect material addon data
    const tg = "throneglass";
    // Weapon
    if (material?.addon) {
      let addon = material.addon;
      if (!Array.isArray(addon)) {
        updateData[`system.${material}.addon`] = Object.entries(addon)
          .filter(([_, chosen]) => chosen)
          .map(([key]) => key);
        addon = updateData[`system.${materialPath}.addon`];
      }
      // Convert Throneglass into main material
      // Since PF1 v10.3
      if (addon.includes(tg)) {
        updateData[`system.${materialPath}.addon`] = addon.filter((ma) => ma !== tg);
        if (!itemData.system.material?.normal?.value) {
          updateData[`system.${materialPath}.normal.value`] = tg;
        }
      }
    }

    // Convert material IDs
    // Since PF1 v10.5
    const addons = updateData[`system.${materialPath}.addon`] ?? material?.addon ?? [];
    if (addons?.some((ma) => !!materialChanges[ma])) {
      updateData[`system.${materialPath}.addon`] = addons.map((ma) => materialChanges[ma] ?? ma);
    }

    const newMat = materialChanges[material?.normal?.value];
    if (newMat) updateData[`system.${materialPath}.normal.value`] = newMat;
  }
}

/**
 * Migrate item held option.
 *
 * @param {object} itemData
 * @param {object} updateData
 * @since PF1 v11
 */
function _migrateItemHeld(itemData, updateData) {
  if (itemData.system.held === "normal") {
    updateData["system.held"] = "1h";
  }
}

/**
 * Removes data that the system has added to items that is now unused with no new location.
 *
 * @param item
 * @param updateData
 */
function _migrateItemUnusedData(item, updateData) {
  // .priceUnits was never used, removal added with PF1 v9
  if (item.system.priceUnits !== undefined) {
    updateData["system.-=priceUnits"] = null;
  }

  // .description.chat was never used
  if (item.system.description?.chat !== undefined) {
    updateData["system.description.-=chat"] = null;
  }

  // .identifiedName was made obsolete with PF1 v9
  if (item.system.identifiedName !== undefined) {
    updateData["system.-=identifiedName"] = null;
  }

  // Creating items in containers added typeName for no reason (in 0.82.5 and older)
  if (item.system.typeName !== undefined) {
    updateData["system.-=typeName"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.weaponData !== undefined) {
    updateData["system.-=weaponData"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.range !== undefined) {
    updateData["system.-=range"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.primaryAttack !== undefined) {
    updateData["system.-=primaryAttack"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.activation !== undefined) {
    updateData["system.-=activation"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.unchainedAction !== undefined) {
    updateData["system.-=unchainedAction"] = null;
  }

  // Data not used since 0.81.0
  if (item.system.measureTemplate !== undefined) {
    updateData["system.-=measureTemplate"] = null;
  }

  // useCustomTag not used since PF1 v10
  if (item.system.useCustomTag !== undefined) {
    updateData["system.-=useCustomTag"] = null;
    if (item.system.useCustomTag === false && item.system.tag !== undefined) {
      updateData["system.-=tag"] = null;
    }
  }

  // ammoType seems to have never been actually used, but it was stored in items
  if (item.system.ammoType !== undefined) {
    updateData["system.-=ammoType"] = null;
    // Move it anyway just in case, if missing
    if (!item.system.ammo?.type && item.system.ammoType) {
      updateData["system.ammo.type"] = item.system.ammoType;
    }
  }
}

/**
 * Migrate Active Effect data.
 * - Removes pf1_ status ID prefixes.
 *
 * Added with PF1 v10
 *
 * @param {object} actorData - Actor data
 * @param {object} updateData - Update data
 * @param {Actor} [actor] - Actor document
 * @param actor
 */
const _migrateActorActiveEffects = async (actorData, updateData, actor) => {
  // Migate Active Effects
  const effects = [];
  for (const ae of actorData.effects ?? []) {
    const aeUpdate = await migrateActiveEffectData(ae, actor);
    if (!foundry.utils.isEmpty(aeUpdate)) {
      aeUpdate._id = ae._id;
      effects.push(aeUpdate);
    }
  }

  if (effects.length) updateData.effects = effects;
};

function _migrateActorUnusedData(actor, updateData) {
  // Obsolete vision
  if (foundry.utils.getProperty(actor.system, "attributes.vision") !== undefined) {
    updateData["system.attributes.-=vision"] = null;
  }

  if (foundry.utils.getProperty(actor.prototypeToken, "flags.pf1.lowLightVision") !== undefined) {
    updateData["prototypeToken.flags.pf1.-=lowLightVision"] = null;
  }

  // XP max is purely derived value
  if (actor.system.details?.xp?.max !== undefined) {
    updateData["system.details.xp.-=max"] = null;
  }

  // Actor resources have always been derived data
  if (actor.system.resources !== undefined) {
    updateData["system.-=resources"] = null;
  }

  // Conditions no longer are permanently stored in actor data (since PF1 v10)
  if (actor.system.attributes?.conditions !== undefined) {
    updateData["system.attributes.-=conditions"] = null;
  }

  // details.level is derived data
  if (actor.system.details?.level !== undefined) {
    updateData["system.details.-=level"] = null;
  }
}

/**
 * Flatten item tuple arrays
 * Since PF1 v9
 *
 * @param item
 * @param updateData
 */
function _migrateItemTuples(item, updateData) {
  // Race subtypes
  if (item.type === "race") {
    if (item.system.subTypes?.length) {
      if (typeof item.system.subTypes[0] !== "string") {
        updateData["system.subTypes"] = item.system.subTypes.flat();
      }
    }
  }

  // Tags
  if (item.system.tags?.length) {
    if (typeof item.system.tags[0] !== "string") {
      updateData["system.tags"] = item.system.tags.flat();
    }
  }

  // Feat class associations
  const classAssociations = item.system.associations?.classes;
  if (classAssociations?.length) {
    if (typeof classAssociations[0] !== "string") {
      updateData["system.associations.classes"] = classAssociations.flat();
    }
  }
}

/**
 * Migrate Active Effect data
 *
 * @param {object} ae Active Effect data
 * @param {Actor} actor Actor
 */
const migrateActiveEffectData = async (ae, actor) => {
  if (!actor) return;

  const updateData = {};

  // Fix broken AE
  if (!ae.name) updateData.name = "No Name";

  /**
   * @param {string} origin Origin string
   * @returns {string|undefined} Relative UUID, if origin was found
   */
  const getNewRelativeOrigin = async (origin) => {
    if (typeof origin !== "string") return; // Invalid origin type, recorded by SBC?
    const newOrigin = await fromUuid(origin, { relative: actor });
    if (newOrigin instanceof Item && actor && newOrigin.actor === actor) {
      return newOrigin.getRelativeUUID(actor);
    }
  };

  // Convert no longer used flags.pf1.prigin to origin, if no origin is present
  const originFlag = ae.flags?.pf1?.origin;
  if (originFlag) {
    if (!ae.origin) {
      const newOrigin = await getNewRelativeOrigin(originFlag);
      if (newOrigin) updateData.origin = newOrigin;
    }
    updateData.flags ??= {};
    updateData.flags.pf1 ??= {};
    updateData.flags.pf1["-=origin"] = null;
  }

  // Convert origin to relative origin
  if (ae.origin) {
    const newOrigin = await getNewRelativeOrigin(ae.origin);
    // Avoid empty updates
    if (newOrigin && ae.origin !== newOrigin) {
      updateData.origin = newOrigin;
    }
  }

  // Remove pf1_ prefix from status effects
  if (ae.statuses.some((s) => s.startsWith("pf1_"))) {
    updateData.statuses = Array.from(new Set(ae.statuses.map((s) => s.replace(/^pf1_/, ""))));
  }

  return updateData;
};

/**
 * Migrate Roll JSON
 *
 * Creates clone of the roll object.
 *
 * @param {object} roll JSON
 * @param {boolean} inplace - Update in-place, modifying the original object
 * @returns {object|void} - Updated roll object or undefined if nothing was changed
 */
export function migrateRollData(roll, inplace = false) {
  roll = inplace ? roll : foundry.utils.deepClone(roll);
  let updated = false;

  // Fix corrupt roll class
  if (["RollPF2", "RollPF$1"].includes(roll.class)) {
    roll.class = "RollPF";
    updated = true;
  }

  // Delve deeper into the roll object
  for (const term of roll.terms ?? []) {
    // _number used in Die terms
    if (typeof term._number === "object" && "class" in term._number && "terms" in term._number) {
      const updatedRoll = migrateRollData(term._number, true);
      if (updatedRoll) updated = true;
    }
    // roll used in parenthetical terms
    if (typeof term.roll === "object" && "class" in term.roll && "terms" in term.roll) {
      const updatedRoll = migrateRollData(term.roll, true);
      if (updatedRoll) updated = true;
    }
  }

  return updated ? roll : undefined;
}

/**
 * Migrate Chat Message data
 *
 * @param {object} cmData - Chat message data (such as result of msg.toObject())
 * @returns {object} - Update data
 */
export function migrateMessageData(cmData) {
  const updateData = {};

  const flags = cmData.flags?.pf1;

  const metadata = flags?.metadata;
  if (metadata) {
    updateData.type = "action";

    updateData.flags ??= {};
    updateData.flags.pf1 ??= {};
    updateData.flags.pf1["-=metadata"] = null;
    updateData.system = metadata;

    if (typeof updateData.system.item !== "object") updateData.system.item = { id: updateData.system.item };
    if (typeof updateData.system.action !== "object") updateData.system.action = { id: updateData.system.action };
  }

  const ident = flags?.identifiedInfo;
  if (ident) {
    updateData.system ??= {};
    updateData.system.item ??= {};
    updateData.system.item.identified = ident.identified ?? true;
    updateData.system.item.name = ident.name;
    updateData.system.item.description = ident.description;
    updateData.system.action ??= {};
    updateData.system.action.name = ident.actionName;
    updateData.system.action.description = ident.actionDescription;

    updateData.flags ??= {};
    updateData.flags.pf1 ??= {};
    updateData.flags.pf1["-=identifiedInfo"] = null;
  }

  // Speed is moved to config
  if (metadata?.speed) {
    updateData.system ??= {};
    updateData.system.config ??= {};
    updateData.system.config.speed = metadata.speed;
  }

  // Skill rank reference is moved to config
  if (metadata?.skill) {
    updateData.system ??= {};
    updateData.system.config ??= {};
    updateData.system.config.rank = metadata.skill.rank;
  }

  if (flags?.subject) {
    updateData.system ??= {};
    updateData.system.subject = flags.subject;
    updateData.flags ??= {};
    updateData.flags.pf1 ??= {};
    updateData.flags.pf1["-=subject"] = null;
  }

  if (typeof updateData.system?.subject === "string") {
    updateData.system.subject = { info: updateData.system.subject };
  }

  // Migrate rolls
  if (cmData.rolls?.length) {
    let updated = false;
    const newRolls = [];
    for (let roll of cmData.rolls) {
      if (typeof roll !== "string") break; // Fail if the data is not in expected format
      roll = JSON.parse(roll);
      const newRoll = migrateRollData(roll);
      if (newRoll) {
        roll = newRoll;
        updated = true;
      }
      newRolls.push(JSON.stringify(roll));
    }
    if (updated) {
      updateData.rolls = newRolls;
    }
  }

  // TODO: System rolls
  /*
  if (cmData.system?.rolls) {
    for (const atk of cmData.system.rolls.attacks ?? []) {
      const updateData = migrateRollJson(atk.attack); // D20RollPF
      if (atk.damage) {
        for (const dmg of atk.damage) {
          const updateData = migrateRollJson(dmg); // DamageRoll
        }
      }
    }
  }
  */

  return updateData;
}

/**
 *
 * @param {ChatMessage} cm - Chat Message document
 * @param {object} [options] - Additional options
 * @param {boolean} [options.commit=true] - If false, no update is performed. Returns update data instead.
 * @param {MigrationState} [options.state] - State tragger. Internal use only.
 * @returns {Promise<ChatMessage|object|null>} - Returns null if no update is needed, promise if commit is true, object otherwise.
 */
export async function migrateMessage(cm, { commit = true, state } = {}) {
  const updateData = migrateMessageData(cm.toObject());

  if (!!updateData.type && updateData.type !== cm.type) {
    if (state) {
      state.typeChanges = true;
    } else console.warn("Document type changed! Refresh required!");
  }

  if (foundry.utils.isEmpty(updateData)) return null;

  if (commit) return cm.update(updateData);
  return updateData;
}

/**
 * Migrate chat messages.
 *
 * @param {object} options - Additional options
 * @param {boolean} [options.fast=true] - Skip already migrated documents.
 * @param {MigrationState} [options.state] - Internal only. State tracker.
 * @param {*} [options.dialog=null] - Dialog configuration.
 * @param {boolean} [options.noHooks=false]
 */
export async function migrateMessages({ fast, state, dialog = null, noHooks = false } = {}) {
  if (!noHooks) Hooks.callAll("pf1MigrationStarted", { scope: "chat" });

  // Locally generated state tracker
  const localState = !state;
  state = await initializeStateAndDialog(state, "PF1.Migration.Category.Chat", dialog);

  if (localState) state.start();

  const tracker = state.createCategory("chat", "PF1.Migration.Category.Chat", true);

  console.log("PF1 | Migration | Chat log starting...");
  tracker.start();

  tracker.setTotal(game.messages.size);
  tracker.setInvalid(game.messages.invalidDocumentIds.size);

  // HACK: Yield thread for smooth dialog experience. Without this the dialog seems to hitch.
  if (dialog) await new Promise((resolve) => setTimeout(resolve, 100));

  const updates = [];

  const applyUpdates = async () => {
    try {
      console.debug("PF1 | Migration | Chat Log | Applying updates to", updates.length, "chat message(s)");
      await ChatMessage.updateDocuments(updates);
    } catch (err) {
      console.error("Error migrating chat messages\n", err, "\n", { updates });
    }
    updates.length = 0;

    // HACK: Yield to keep UI somewhat responsive, without this it can become completely unresponsive for a long period of time
    // This issue probably can happen with the other document types, but is especially prone to happening with messages
    await new Promise((resolve) => setTimeout(resolve, 0));
  };

  // Do actual migration
  // One by one migration, this is slow but works
  for (const msg of game.messages) {
    if (fast && isMigrated(msg)) {
      tracker.ignoreEntry(msg);
      continue;
    }

    tracker.startEntry(msg);
    try {
      const updateData = migrateMessageData(msg.toObject());
      if (!foundry.utils.isEmpty(updateData)) {
        updateData._id = msg.id;
        updates.push(updateData);

        if (state && updateData.type && updateData.type !== msg.type) {
          state.typeChanges = true;
        }
      }
    } catch (err) {
      console.error(err, msg);
      tracker.recordError(msg, err);
    }
    tracker.finishEntry(msg);

    if (updates.length >= pf1.migrations.UPDATE_CHUNK_SIZE) await applyUpdates();
  }

  if (updates.length) await applyUpdates();

  // Migration complete
  console.log("PF1 | Migration | Chat log complete!");
  tracker.finish();
  if (localState) state.finish();

  if (!noHooks) Hooks.callAll("pf1MigrationFinished", { scope: "chat" });
}
