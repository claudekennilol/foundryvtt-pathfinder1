export function initializeSocket() {
  game.socket.on("system.pf1", runSocketFunction);
}

/**
 * Handle socket message
 *
 * @param {object} msg - Message data
 * @param {string} senderId - User ID
 */
async function runSocketFunction(msg, senderId) {
  const isFirstGM = game.users.activeGM?.isSelf;
  const sender = game.users.get(senderId);
  try {
    switch (msg.eventType) {
      case "currencyTransfer": {
        if (!isFirstGM) return;
        let source = await fromUuid(msg.data.sourceActor);
        let dest = await fromUuid(msg.data.destActor);

        if (msg.data.sourceContainer) source = source.items.get(msg.data.sourceContainer);
        if (msg.data.destContainer) dest = dest.items.get(msg.data.destContainer);
        const amount = msg.data.amount;

        pf1.applications.CurrencyTransfer.transfer(source, dest, amount, msg.data.sourceAlt, msg.data.destAlt, false);
        break;
      }
      case "alterChatTargetAttribute":
        if (isFirstGM) alterChatTargetAttribute(msg);
        break;
      case "giveItem": {
        if (!isFirstGM) return;
        const item = await fromUuid(msg.item);
        const sourceActor = item.actor;
        if (!sourceActor.testUserPermission(sender, "OWNER")) return;
        const targetActor = await fromUuid(msg.targetActor);
        const itemData = item.toObject();
        const newItem = await Item.implementation.create(itemData, { parent: targetActor });
        if (newItem) {
          await sourceActor.deleteEmbeddedDocuments("Item", [item.id]);
        }
        break;
      }
      case "refreshActorSheets":
        if (sender.hasPermission(CONST.USER_PERMISSIONS.SETTINGS_MODIFY)) {
          pf1.utils.refreshActors({ renderOnly: true });
        }
        break;
    }
  } catch (err) {
    console.log("PF1 | Socket Error:", err);
  }
}

export function alterChatTargetAttribute(args) {
  const message = game.messages.get(args.message);
  const contentHTML = $(message.content);

  // Alter saving throw
  if (args.save != null) {
    const targetElem = contentHTML.find(
      `div.attack-targets .target[data-uuid="${args.targetUuid}"] .saving-throws .${args.save}`
    );
    const valueElem = targetElem.find(".value");
    valueElem.html(`${args.value}`);

    // Add classes based off extra data
    if (args.isFailure) valueElem.addClass("failure");
    else valueElem.removeClass("failure");
    if (args.isSuccess) valueElem.addClass("success");
    else valueElem.removeClass("success");

    return message.update({
      content: contentHTML.prop("outerHTML"),
    });
  }
}
