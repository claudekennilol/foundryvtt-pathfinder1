import { getHighestChanges } from "@actor/utils/apply-changes.mjs";
import { RollPF } from "../dice/roll.mjs";
import { DamageRoll } from "../dice/damage-roll.mjs";

import { DocumentLikeModel } from "@models/abstract/document-like-model.mjs";
import { CompactingMixin } from "@models/mixins/compacting-mixin.mjs";
import { DamagePartModel } from "@models/action/damage-part-model.mjs";
import { ExtraAttackModel } from "@models/action/extra-attack-model.mjs";

import { ReplaceableSourceMixin } from "@models/mixins/replaceable-source.mjs";

import { FormulaField } from "@datafields/formula-field.mjs";

/**
 * Action pseudo-document
 */
export class ItemAction extends ReplaceableSourceMixin(CompactingMixin(DocumentLikeModel)) {
  /**
   * Default image if the action has no image and it is not correctly linked to an item
   *
   * @readonly
   */
  static FALLBACK_IMAGE = /** @type {const} */ ("systems/pf1/icons/skills/gray_04.jpg");

  /**
   * Metadata mimicking Foundry documents
   *
   * Required by {@link pf1.applications.ItemActionSheet#_createDocumentIdLink}
   *
   * @internal
   * @readonly
   */
  static metadata = Object.freeze({
    label: "PF1.Action",
  });

  /**
   * @param {object} data - Action data
   * @param {*} options - Data model options
   */
  constructor(data, options) {
    if (options instanceof Item) {
      foundry.utils.logCompatibilityWarning(
        "ItemAction constructor's second parameter as parent is deprecated. Please wrap it in options object like with datamodels.",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      options = { parent: options };
    }

    super(data, options);
  }

  /** @override */
  _configure(options) {
    super._configure(options);

    // Following prevent these definitions being lost on model reset()
    Object.defineProperties(this, {
      // Apps
      apps: {
        value: {},
        writable: false,
        enumerable: false,
      },
      // Sheet cache
      _sheet: {
        value: null,
        writable: true,
        enumerable: false,
      },
      // Conditionals collection cache to avoid conflicts with stored array
      _conditionals: {
        value: new Collection(),
        writable: false,
        enumerable: false,
      },
    });
  }

  /** @override */
  static defineSchema() {
    const fields = foundry.data.fields;
    const blankToNull = { nullable: true, blank: false };
    return {
      ...super.defineSchema({ name: () => game.i18n.localize("PF1.Action") }),
      img: new fields.FilePathField({ categories: ["IMAGE"] }),
      description: new fields.HTMLField(),
      tag: new fields.StringField({ blank: false, nullable: true }), // TODO: slug field
      activation: new fields.SchemaField({
        cost: new fields.NumberField({ initial: 1, nullable: true, min: 1, integer: true }),
        type: new fields.StringField({ initial: "nonaction" }), // pf1.config.abilityActivationTypes
        unchained: new fields.SchemaField({
          cost: new fields.NumberField({ initial: 1, nullable: true, min: 1, integer: true }),
          type: new fields.StringField({ initial: "nonaction" }), // pf1.config.abilityActivationTypes_unchained
        }),
      }),
      duration: new fields.SchemaField({
        value: new fields.StringField(),
        units: new fields.StringField(),
        dismiss: new fields.BooleanField({ initial: false }),
        concentration: new fields.BooleanField({ initial: false }),
      }),
      // Refactor into just .target string?
      target: new fields.SchemaField({
        value: new fields.StringField(),
      }),
      range: new fields.SchemaField({
        value: new fields.StringField({ ...blankToNull }),
        units: new fields.StringField({ ...blankToNull }),
        maxIncrements: new fields.NumberField({ integer: true, nullable: false, initial: 1 }),
        minValue: new fields.StringField({ ...blankToNull }),
        minUnits: new fields.StringField({ ...blankToNull }),
      }),
      uses: new fields.SchemaField({
        autoDeductChargesCost: new fields.StringField(),
        perAttack: new fields.BooleanField({ initial: false }),
        self: new fields.SchemaField({
          value: new fields.NumberField({ integer: true, min: 0, nullable: false, initial: 0 }),
          maxFormula: new FormulaField(),
          per: new fields.StringField(),
        }),
      }),
      measureTemplate: new fields.SchemaField({
        type: new fields.StringField(),
        size: new FormulaField(),
        color: new fields.ColorField(),
        texture: new fields.FilePathField({ categories: ["IMAGE", "VIDEO"] }),
      }),
      bab: new FormulaField(),
      attackName: new fields.StringField(),
      actionType: new fields.StringField({ initial: "other" }), // pf1.config.itemActionTypes
      attackBonus: new FormulaField(),
      critConfirmBonus: new FormulaField(),
      damage: new fields.SchemaField({
        parts: new fields.ArrayField(new fields.EmbeddedDataField(DamagePartModel)),
        critParts: new fields.ArrayField(new fields.EmbeddedDataField(DamagePartModel)),
        nonCritParts: new fields.ArrayField(new fields.EmbeddedDataField(DamagePartModel)),
      }),
      extraAttacks: new fields.SchemaField({
        type: new fields.StringField(), // pf1.config.extraAttacks
        manual: new fields.ArrayField(new fields.EmbeddedDataField(ExtraAttackModel)),
        formula: new fields.SchemaField({
          count: new FormulaField(),
          bonus: new FormulaField(),
          label: new fields.StringField(),
        }),
      }),
      ability: new fields.SchemaField({
        attack: new fields.StringField(), // ability key
        damage: new fields.StringField(), // ability key
        max: new fields.NumberField({ integer: true, nullable: true }),
        damageMult: new fields.NumberField({ initial: null }),
        critRange: new fields.NumberField({ initial: 20, nullable: true }), // null => 0
        critMult: new fields.NumberField({ initial: 2, nullable: true }), // null => 1
      }),
      save: new fields.SchemaField({
        dc: new FormulaField(),
        type: new fields.StringField(), // pf1.config.savingThrows
        description: new fields.StringField(),
        harmless: new fields.BooleanField({ initial: false }),
      }),
      notes: new fields.SchemaField({
        effect: new fields.ArrayField(new fields.StringField()),
        footer: new fields.ArrayField(new fields.StringField()),
      }),
      soundEffect: new fields.FilePathField({ categories: ["AUDIO"] }),
      powerAttack: new fields.SchemaField({
        multiplier: new fields.NumberField({ min: 0, initial: null, nullable: true }),
        damageBonus: new fields.NumberField({ min: 0, initial: 2, integer: true }),
        critMultiplier: new fields.NumberField({ min: 1, initial: 1, integer: true }),
      }),
      naturalAttack: new fields.SchemaField({
        primary: new fields.BooleanField({ initial: true }),
        secondary: new fields.SchemaField({
          attackBonus: new FormulaField(),
          damageMult: new fields.NumberField({ initial: 0.5, positive: true }),
        }),
      }),
      held: new fields.StringField({ choices: () => pf1.config.weaponHoldTypes, initial: undefined, blank: true }),
      nonlethal: new fields.BooleanField({ initial: false }),
      splash: new fields.BooleanField({ initial: false }),
      touch: new fields.BooleanField({ initial: false }),
      ammo: new fields.SchemaField({
        type: new fields.StringField(), // pf1.config.ammoTypes
        cost: new fields.NumberField({ integer: true, min: 0, initial: 1 }),
        misfire: new fields.NumberField({ integer: true, min: 0, initial: null, nullable: true }),
      }),
      effect: new fields.StringField(),
      area: new fields.StringField(),
      conditionals: new fields.ArrayField(new fields.ObjectField()), // TODO
      enh: new fields.SchemaField({
        value: new fields.NumberField({ integer: true, min: 0, initial: null, nullable: true }),
      }),
      material: new fields.SchemaField({
        normal: new fields.SchemaField({
          value: new fields.StringField(),
          custom: new fields.BooleanField({ initial: false }),
        }),
        addon: new fields.SetField(new fields.StringField({ nullable: false, blank: false })),
      }),
      // Trinary alignments to allow inheriting from item and to explicitly disabling alignments
      alignments: new fields.SchemaField({
        lawful: new fields.BooleanField({ nullable: true, initial: null }),
        chaotic: new fields.BooleanField({ nullable: true, initial: null }),
        good: new fields.BooleanField({ nullable: true, initial: null }),
        evil: new fields.BooleanField({ nullable: true, initial: null }),
      }),
    };
  }

  /** @override */
  static migrateData(source) {
    if (typeof source !== "object") return;

    source.activation ??= {};
    source.activation.unchained ??= {};

    // Added with v?
    // .unchainedAction.activation to .activation.unchained
    if (source.unchainedAction?.activation) {
      source.activation.unchained = source.unchainedAction.activation;
    }

    // Inconsistent held option (PF1 v11)
    if (source.held === "normal") source.held = "1h";

    // Migrate invalid string activation cost to 1.
    if (typeof source.activation.cost === "string") source.activation.cost = 1;
    if (typeof source.activation.unchained?.cost === "string") source.activation.unchained.cost = 1;

    if (source.enh !== undefined) {
      if (typeof source.enh !== "object") {
        source.enh = { value: source.enh ?? null };
      }
      // Set to null if disabled.
      if (source.enh.override === false) {
        source.enh.value = null;
      }
      // Reset odd values to null, too.
      else if (source.enh.value !== null && typeof source.enh.value !== "number") {
        source.enh.value = null;
      }
    }

    if (source.uses?.autoDeductCharges === false) {
      source.uses.autoDeductChargesCost = "0";
    } else if (source.uses?.autoDeductCharges === true) {
      source.uses.autoDeductChargesCost = "1";
    }

    // Added with v10
    source.actionType ||= "other";
    source.area ||= source.spellArea;

    // Migrate unlimited to empty selection, as the two are identical in meaning
    if (source.uses?.self?.per === "unlimited") {
      delete source.uses.self.per;
    }

    const mt = source.measureTemplate;
    if (mt) {
      mt.color ||= mt.customColor;
      mt.texture ||= mt.customTexture;
    }

    // Added with v11
    if (source.range?.maxIncrements === null || source.range?.maxIncrements < 1) source.range.maxIncrements = 1;
    if (source.spellEffect && !source.effect) source.effect = source.spellEffect;
    if (source.naturalAttack?.primaryAttack !== undefined && source.naturalAttack?.primary === undefined) {
      source.naturalAttack.primary = source.naturalAttack?.primaryAttack;
    }
    source.notes ??= {};
    if (source.effectNotes && !source.notes.effect) source.notes.effect = source.effectNotes;
    if (source.attackNotes && !source.notes.footer) source.notes.footer = source.attackNotes;

    if (source.range?.units === "none") delete source.range.units;

    //if (data.ability?.critMult === null) data.ability.critMult = 1;
    //if (data.ability?.critRange === null) data.ability.critRange = 0;

    // Since v9, convert old damage array format of [formula, types] tuple into object
    // DamagePartModel.migrateData can't handle this as DataModel trashes it before it reaches it
    // See: https://github.com/foundryvtt/foundryvtt/issues/12125
    if (source.damage) {
      for (const key of ["parts", "critParts", "nonCritParts"]) {
        const parts = source.damage[key] ?? [];
        if (parts.some((p) => Array.isArray(p))) {
          source.damage[key] = parts.map((p) => {
            if (Array.isArray(p)) {
              const [formula, type] = p;
              return { formula, type };
            }
            return p;
          });
        }
      }
    }

    return super.migrateData(source);
  }

  /**
   * Retrieve UUID
   *
   * Relies on {@link pf1.documents.item.ItemBasePF.getEmbeddedDocument | Item#getEmbeddedDocument()} override to function with fromUuid() and similar functions.
   *
   * @type {string}
   */
  get uuid() {
    return this.parent.uuid + `.Action.${this.id}`;
  }

  /**
   * Data preparation
   *
   * @internal
   */
  prepareData() {
    // Default action type to other if undefined.
    // Optimally this would be in constructor only, but item action handling can cause that to be lost
    this.actionType ||= "other";

    this.img ||= this.item?.img || this.constructor.FALLBACK_IMAGE;

    this.tag ||= pf1.utils.createTag(this.name);

    // DEPRECATIONS
    if (this.naturalAttack) {
      Object.defineProperty(this.naturalAttack, "primaryAttack", {
        get() {
          foundry.utils.logCompatibilityWarning(
            "ItemAction.naturalAttack.primaryAttack is deprecated in favor of ItemAction.naturalAttack.primary",
            {
              since: "PF1 v11",
              until: "PF1 v12",
            }
          );
          return this.primary;
        },
      });
    }

    // Prepare ammo
    const ammoType = this.ammo?.type;
    this.ammo.type = ammoType === "none" ? null : ammoType || this.item?.system.ammo?.type || null;

    if (this.ammo.type) this.ammo.cost ??= 1;
    else this.ammo.cost = 0; // Force zero if no type defined

    // Override activation
    if (game.settings.get("pf1", "unchainedActionEconomy")) {
      this.activation = this.activation.unchained;
    }

    this._prepareConditionals();

    // Nothing more if there's no parent. Temporary Action?
    if (!this.item) return;

    const rollData = this.getRollData();

    if (this.item.type === "spell") {
      // Early transform key ability reference to spellbook ability
      // TODO: Support key ability via class-association
      const keyAbility = this.item.spellbook?.ability;
      if (keyAbility) {
        if (this.ability?.damage === "_key") {
          this.ability.damage = keyAbility;
        }
        if (this.ability?.attack === "_key") {
          this.ability.attack = keyAbility;
        }
      }
    }

    if (this.ability?.attack === "_default") {
      let defaultAbility;
      switch (this.actionType) {
        case "mwak":
        case "msak":
        case "mcman":
          defaultAbility = this.actor?.system.attributes?.attack?.meleeAbility || "str";
          break;
        case "rwak":
        case "twak":
        case "rsak":
        case "rcman":
          defaultAbility = this.actor?.system.attributes?.attack?.rangedAbility || "dex";
          break;
      }
      this.ability.attack = defaultAbility;
    }

    // Prepare max personal charges
    if (this.uses.self?.per) {
      const maxFormula = this.uses.self.per === "single" ? "1" : this.uses.self.maxFormula;
      const maxUses = RollPF.safeRollSync(maxFormula, rollData).total ?? 0;
      this.uses.self.max = maxUses;
    }

    // Remove enhancement bonus override, if wrong type
    if (this.enh?.value != null && !["weapon", "attack"].includes(this.item.type)) {
      this.enh.value = null;
    }

    // Initialize default damageMult if missing (for things that can't inherit it from item)
    if (!Number.isFinite(this.ability?.damageMult)) {
      let canHold = this.item.isPhysical || this.item.isQuasiPhysical || false;
      if (!this.hasAttack) canHold = false;
      if (!canHold) this.ability.damageMult = 1;
    }
  }

  /**
   * Normal material
   *
   * @remarks - Null if no material is configured in action or item.
   *
   * @type {string|null}
   */
  get normalMaterial() {
    return this.material.normal.value || this.item.normalMaterial || null;
  }

  /**
   * Addon materials
   *
   * @type {Array<string>}
   */
  get addonMaterial() {
    const addons = this.material.addon || this.item.addonMaterial || [];
    return addons.filter((o) => !!o);
  }

  /**
   * Returns whether this action is a combat maneuver
   *
   * @type {boolean}
   */
  get isCombatManeuver() {
    return ["mcman", "rcman"].includes(this.actionType);
  }

  /**
   * Creates an action.
   *
   * @param {object[]} data - Data to initialize the action(s) with.
   * @param {object} context - An object containing update context information.
   * @param {ItemPF} [context.parent] - The parent entity to create the action within.
   * @throws {Error} - If the action has no parent
   * @returns {Promise<ItemAction[]>} - The resulting actions
   */
  static async create(data, context = {}) {
    const { parent, ...updateContext } = context;

    if (!(parent instanceof Item)) throw new Error("No parent declared");

    // Prepare new data
    data = data.map((dataObj) => new this(dataObj).toObject());

    // Update parent
    const actions = parent.toObject().system.actions || [];
    actions.push(...data);
    await parent.update({ "system.actions": actions }, updateContext);

    // Return resulting actions
    return data.map((o) => parent.actions.get(o._id));
  }

  /**
   * Parent item
   *
   * @type {ItemPF|undefined}
   */
  get item() {
    return this.parent;
  }

  /**
   * Parent actor of the parent item
   *
   * @type {ActorPF|undefined}
   */
  get actor() {
    return this.parent?.actor;
  }

  /**
   * Action ID
   *
   * @type {string}
   */
  get id() {
    return this._id;
  }

  /**
   * Can this action be used?
   *
   * Returns false if any known criteria for use limitation fails. Calls owning item's canUse functionality also.
   *
   * @see {@link pf1.documents.item.ItemBasePF.canUse}
   *
   * @type {boolean}
   */
  get canUse() {
    const item = this.item;
    if (!item.canUse) return false;

    if (this.isSelfCharged) {
      if ((this.uses.self?.value ?? 0) <= 0) return false;
    }

    if (item.isPhysical) {
      if (item.system.quantity <= 0) return false;
    }

    if (this.isCharged) {
      const cost = this.getChargeCostSync({ maximize: true })?.total ?? 0;
      const charges = item.charges;
      if (cost > 0) {
        if (cost > charges) return false;
      }
    }

    const ammo = this.ammo.type;
    if (ammo) {
      // Check if actor has any relevant ammo, regardless if they're set to default
      if (
        this.actor?.itemTypes.loot.filter(
          (i) => i.subType === "ammo" && i.system.extraType === ammo && i.system.quantity > 0
        ).length === 0
      )
        return false;
    }

    return true;
  }

  /**
   * Includes attack roll?
   *
   * @type {boolean}
   */
  get hasAttack() {
    return ["mwak", "rwak", "twak", "msak", "rsak", "mcman", "rcman"].includes(this.actionType);
  }

  /**
   * Has potentially multiple attacks
   *
   * @type {boolean}
   */
  get hasMultiAttack() {
    if (!this.hasAttack) return false;
    const exAtk = this.extraAttacks ?? {};
    return exAtk.manual?.length > 0 || !!exAtk.type;
  }

  /**
   * Consumes charges on use?
   *
   * @type {boolean}
   */
  get autoDeductCharges() {
    return this.getChargeCostSync({ maximize: true })?.total > 0;
  }

  /**
   * Does parent item have charges?
   *
   * @type {boolean}
   */
  get isCharged() {
    return this.item.isCharged ?? false;
  }

  /**
   * Action has charges of its own?
   *
   * @type {boolean}
   */
  get isSelfCharged() {
    return !!this.uses?.self?.per;
  }

  /**
   * @param {object} [options] - Additional options to configure behavior.
   * @param {object} [options.rollData=null] - Pre-determined roll data to pass for determining the charge cost.
   * @param {boolean} [options.minimize=false]
   * @param {boolean} [options.maximize=false]
   * @param {boolean} [options.interactive=false]
   * @returns {Promise<Roll|null>} - Cost in charges for this action. Null if not charged.
   */
  async getChargeCost({ minimize = false, maximize = false, rollData = null, interactive = false } = {}) {
    if (!this.isCharged) return null;

    const isSpell = this.item.type === "spell";
    const isSpellpointSpell = isSpell && this.item.useSpellPoints();

    let formula = !isSpellpointSpell ? this.uses.autoDeductChargesCost : this.uses.spellPointCost;
    if (!formula) {
      formula = this.item.getDefaultChargeFormula();
    } else if (typeof formula !== "string") {
      console.warn(this.item.name, "action", this.name, "has invalid charge formula:", formula, this);
      formula = this.item.getDefaultChargeFormula();
    }

    rollData ??= this.getRollData();

    const roll = await RollPF.safeRoll(formula, rollData, undefined, undefined, {
      maximize,
      minimize,
      allowInteractive: interactive,
    });

    // Clamp single use
    if (this.item.isSingleUse) roll._total = Math.clamp(roll._total, -1, 1);

    return roll;
  }

  /**
   * @param {object} [options] - Additional options to configure behavior.
   * @param {object} [options.rollData=null] - Pre-determined roll data to pass for determining the charge cost.
   * @param {boolean} [options.minimize=false]
   * @param {boolean} [options.maximize=false]
   * @returns {Roll|null} - Cost in charges for this action. Null if not charged.
   */
  getChargeCostSync({ minimize = false, maximize = false, rollData = null } = {}) {
    if (!this.isCharged) return null;

    const isSpell = this.item.type === "spell";
    const isSpellpointSpell = isSpell && this.item.useSpellPoints();

    let formula = !isSpellpointSpell ? this.uses.autoDeductChargesCost : this.uses.spellPointCost;
    if (!formula) {
      formula = this.item.getDefaultChargeFormula();
    } else if (typeof formula !== "string") {
      console.warn(this.item.name, "action", this.name, "has invalid charge formula:", formula, this);
      formula = this.item.getDefaultChargeFormula();
    }

    rollData ??= this.getRollData();

    if (!maximize && !minimize) maximize = true; // Enforce maximization if neither is called in case this is a die
    const roll = RollPF.safeRollSync(formula, rollData, undefined, undefined, { maximize, minimize });

    // Clamp single use
    if (this.item.isSingleUse) roll._total = Math.clamp(roll._total, -1, 1);

    return roll;
  }

  /**
   * The action's first increment range (in system configured units)
   *
   * @type {number}
   */
  get rangeIncrement() {
    return this.getRange({ type: "single" });
  }

  /**
   * The action's exclusive minimum range.
   *
   * @type {number}
   */
  get minRange() {
    return this.getRange({ type: "min" });
  }

  /**
   * The action's maximum range (range multiplied by maximum range increments).
   *
   * @type {number}
   */
  get maxRange() {
    return this.getRange({ type: "max" });
  }

  /**
   * @param {object} [options] - Additional options to configure behavior.
   * @param {"single"|"min"|"max"} [options.type="single"] - What type of range to query. Either "single" (for a single range increment), "max" or "min".
   * @param {object} [options.rollData=null] - Specific roll data to pass.
   * @returns {number|null} The given range, in system configured units, or `null` if no range is applicable.
   */
  getRange({ type = "single", rollData = null } = {}) {
    const baseRange = this.range.units;
    const range = type === "min" ? this.range.minValue : this.range.value;
    let rangeType = type === "min" ? this.range.minUnits : baseRange;

    // Special case of ignoring min range for invalid range types
    if (type === "min" && !["reach", "ft", "mi", "seeText"].includes(baseRange)) return 0;

    if (!rangeType) {
      if (type !== "min") return null;
      // Special handling for reach minimum range to account for natural reach when no explicit minimum range is defined
      if (baseRange === "reach") rangeType = "natural";
      else return 0;
    }

    rollData ??= this.getRollData();
    const singleIncrementRange = pf1.utils.calculateRange(range, rangeType, rollData)[0];

    if (["single", "min"].includes(type)) return singleIncrementRange;
    return singleIncrementRange * this.range.maxIncrements;
  }

  /**
   * Has measured template?
   *
   * @type {boolean}
   */
  get hasTemplate() {
    const { type, size } = this.measureTemplate;
    return !!type && !!size;
  }

  /**
   * Does the action implement a damage roll as part of its usage
   *
   * @type {boolean}
   */
  get hasDamage() {
    return !!this.damage.parts?.length;
  }

  /**
   * Effective critical range when accounting for broken status and action type.
   *
   * @type {number}
   */
  get critRange() {
    if (this.item.isBroken || this.isCombatManeuver) return 20;
    return this.ability?.critRange || 20;
  }

  /**
   * Misfire threshold
   *
   * @remarks - Zero if action does not misfire.
   *
   * @type {number}
   */
  get misfire() {
    const misfire = this.ammo?.misfire ?? null;
    if (Number.isFinite(misfire)) return misfire;
    return this.item.system.ammo?.misfire ?? 0;
  }

  /**
   * Get power attack, deadly aim or piranha strike multiplier.
   *
   * @param {object} [options] - Additional options
   * @param {object} [options.rollData=null] - Roll data instance
   * @returns {number} - Effective multiplier
   */
  getPowerAttackMult({ rollData = null } = {}) {
    rollData ??= this.getRollData();

    const held = rollData.action?.held || rollData.item?.held || "1h";

    let mult = rollData.action?.powerAttack?.multiplier;
    // Use defined override
    if (mult) return mult;

    // Determine default based on attack type and held option
    mult = 1;
    if (this.item.subType === "natural") {
      // Primary
      if (rollData.action.naturalAttack?.primary) {
        const ablDmgMult = rollData.action.ability?.damageMult ?? 1;
        // Primary attack gets +50% damage like with two-handing if ability score multiplier is 1.5x or higher
        if (ablDmgMult >= 1.5) mult = 1.5;
      }
      // Secondary
      else {
        mult = 0.5;
      }
    } else {
      if (held === "2h") mult = 1.5;
      else if (held === "oh") mult = 0.5;
    }

    return mult;
  }

  /**
   * Does the item have range defined.
   *
   * @type {boolean}
   */
  get hasRange() {
    const units = this.range?.units;
    if (!units) return false;
    return !!units;
  }

  /* -------------------------------------------- */

  /**
   * Does the item provide an amount of healing instead of conventional damage?
   *
   * @returns {boolean}
   */
  get isHealing() {
    return this.actionType === "heal" && this.hasDamage;
  }

  get hasEffect() {
    return this.hasDamage || this.notes.effect.length > 0;
  }

  /**
   * Does the Item implement a saving throw as part of its usage
   *
   * @type {boolean}
   */
  get hasSave() {
    return !!this.save?.type;
  }

  /**
   * @param {object} [rollData] - Data to pass to the roll. If none is given, get new roll data.
   * @returns {number} The Difficulty Class for this action.
   */
  getDC(rollData = null) {
    rollData ??= this.getRollData();
    let result = 10;

    // Get conditional save DC bonus
    const dcBonus = rollData.dcBonus ?? 0;

    if (this.item.type === "spell") {
      const spellbook = this.item.spellbook;
      if (spellbook) {
        let formula = spellbook.baseDCFormula;

        const data = rollData.action;
        if (data.save.dc) formula += ` + ${data.save.dc}`;

        const dcSchoolBonus = rollData.attributes.spells?.school?.[this.item.system.school]?.dc ?? 0;
        const universalDCBonus = rollData.attributes?.spells?.school?.all?.dc ?? 0;

        return RollPF.safeRollSync(formula, rollData).total + dcBonus + dcSchoolBonus + universalDCBonus;
      } else {
        // Assume standard base formula for spells with minimum required abilty score
        const level = this.item.system.level ?? 1;
        const minAbl = Math.floor(level / 2);
        return 10 + level + minAbl + dcBonus;
      }
    } else {
      const dcFormula = this.save.dc?.toString() || "0";
      result = RollPF.safeRollSync(dcFormula, rollData).total + dcBonus;
      return result;
    }
  }

  /**
   * Effective enhancement bonus
   *
   * Action's enhancement override or what is inherited from item.
   *
   * @remarks
   * - Null if not enhanced.
   *
   * @type {number|null}
   */
  get enhancementBonus() {
    return this.enh?.value ?? this.item.system.enh ?? null;
  }

  /**
   * Is ranged rather than melee.
   *
   * @type {boolean}
   */
  get isRanged() {
    return ["rwak", "twak", "rsak", "rcman"].includes(this.actionType);
  }

  /**
   * Is spell-like action
   *
   * @remarks
   * - Action type is set to melee/ranged spell attack or spell save.
   *
   * @type {boolean}
   */
  get isSpell() {
    return ["rsak", "msak", "spellsave"].includes(this.actionType);
  }

  /**
   * An array of changes affecting this action's damage
   *
   * @type {ItemChange[]}
   */
  get damageSources() {
    // Build damage context
    const contexts = [pf1.const.actionTypeToContext[this.actionType] ?? "damage"];
    if (this.isRanged) contexts.push("rdamage");
    else contexts.push("mdamage");
    if (this.item.subType === "natural") contexts.push("ndamage");

    const changes = this.item.getContextChanges(contexts);
    if (changes.length == 0) return [];
    return getHighestChanges(changes, { ignoreTarget: true });
  }

  /**
   * All relevant Changes the action's damage.
   *
   * @type {ItemChange[]}
   */
  get allDamageSources() {
    const conds = this.conditionals
      .filter((c) => c.default)
      .filter((c) => c.modifiers.find((m) => m.target === "damage"));
    const rollData = this.getRollData();

    if (!rollData) return [];

    const mods = Object.keys(pf1.config.bonusTypes);

    // Turn relevant conditionals into structure accepted by getHighestChanges
    const fakeCondChanges = [];
    for (const c of conds) {
      for (const m of c.modifiers) {
        if (m.target !== "damage") continue;
        let roll;
        let isDeterministic;
        try {
          roll = new RollPF(m.formula, rollData);
          isDeterministic = roll.isDeterministic;
          if (isDeterministic) roll.evaluateSync();
        } catch (err) {
          // This is very noisy but needed to help track down the problem
          console.error(
            "Invalid conditional formula:",
            { formula: m.formula, conditional: c, action: this, item: this.item },
            "\n",
            err
          );
        }
        const isModifier = mods.includes(m.type);
        fakeCondChanges.push({
          flavor: c.name,
          value: isDeterministic ? roll.total : m.formula,
          modifier: isModifier ? m.type : "untyped", // Turn unrecognized types to untyped
          type: isModifier ? undefined : m.type, // Preserve damage type if present
          formula: m.formula,
        });
      }
    }

    const allChanges = [...this.damageSources, ...fakeCondChanges];

    // Add enhancement bonus
    const enh = this.enhancementBonus;
    if (enh) {
      allChanges.push({
        flavor: game.i18n.localize("PF1.EnhancementBonus"),
        value: enh,
        type: "enh",
        formula: enh.toString(),
      });
    }

    // Add special cases specific to the item
    // Broken
    if (this.item.isBroken) {
      allChanges.push({
        flavor: game.i18n.localize("PF1.Broken"),
        value: -2,
        type: "untyped",
        formula: "-2",
      });
    }

    return getHighestChanges(allChanges, { ignoreTarget: true });
  }

  /**
   * @param {object} [options]
   * @internal
   * @returns {object}
   */
  getRollData(options) {
    const item = this.item;
    const result = item?.getRollData({ ...options, cache: false }) ?? {};

    result.action = pf1.utils.deepClone(this);
    result.dc = this.hasSave ? this.getDC(result) : 0;

    if (item?.type === "spell") {
      // Add per school CL bonus
      // TODO: Move to item roll data generation?
      result.cl += result.attributes?.spells?.school?.[item.system.school]?.cl ?? 0;
    }

    // Determine size bonus
    if (this.hasAttack) {
      const size = result.size || 4;
      result.sizeBonus = Object.values(this.isCombatManeuver ? pf1.config.sizeSpecialMods : pf1.config.sizeMods)[size];
    }

    // BAB override
    if (result.action.bab) {
      const bab = RollPF.safeRollSync(result.action.bab, result).total;
      foundry.utils.setProperty(result, "attributes.bab.total", bab || 0);
    }

    // Add @bab alias
    result.bab = result.attributes?.bab?.total || 0;

    if (Hooks.events["pf1GetRollData"]?.length > 0) Hooks.callAll("pf1GetRollData", this, result);

    return result;
  }

  /**
   * Replace conditionals array with collection
   *
   * @internal
   */
  _prepareConditionals() {
    const collection = this._conditionals;
    /** @type {Collection<pf1.components.ItemConditional>} */
    const prior = new Collection(collection.entries());
    collection.clear(); // TODO: Remove specific entries after the loop instead of full clear here

    for (const condData of this._source.conditionals) {
      /** @type {pf1.components.ItemConditional} */
      let conditional;
      if (prior && prior.has(condData._id)) {
        conditional = prior.get(condData._id);
        conditional.replaceSource(condData);
      } else {
        conditional = new pf1.components.ItemConditional(condData, { parent: this });
      }

      collection.set(conditional.id, conditional);
    }

    /** @type {Collection<pf1.components.ItemConditional>} */
    this.conditionals = collection;
  }

  /**
   * Delete this action
   *
   * @returns {Promise<Item>} - Updated parent item document.
   */
  async delete() {
    const actions = this.item.toObject().system.actions;
    actions.findSplice((a) => a._id == this.id);

    // Preemptively close applications
    const promises = [];
    for (const app of Object.values(this.apps)) {
      promises.push(app.close({ pf1: { action: "delete" }, submit: false, force: true }));
    }
    await Promise.all(promises);

    // Delete action
    return this.item.update({ "system.actions": actions });
  }

  /**
   * Update the action
   *
   * TODO: BROKEN
   *
   * @param {object} updateData - Update data
   * @param {object} context - Update context
   */
  async update(updateData, context = {}) {
    updateData = foundry.utils.expandObject(updateData);

    delete updateData._id; // Prevent ID drift
    this.updateSource(updateData);

    const updates = this.item.actions.map((a) => a.toObject());

    await this.item.update({ "system.actions": updates }, context);
  }

  /* -------------------------------------------- */
  /*  Chat Data Generation												*/
  /* -------------------------------------------- */

  /**
   * Generates {@link ChatData} for this action's parent item, but with this action's data,
   * regardless of whether it is the first action or not.
   *
   * @see {@link ItemPF#getChatData}
   * @param {object} [chatDataOptions] - Options passed to {@link ItemPF#getChatData} affecting the chat data
   * @returns {Promise<import("../documents/item/item-pf.mjs").ChatData>} Chat data for this action's parent and this action
   */
  async getChatData(chatDataOptions = {}) {
    return this.item.getChatData({ ...chatDataOptions, actionId: this.id });
  }

  /**
   * Returns labels related to this particular action
   *
   * @param {object} [options]
   * @param {object} [options.rollData] - Pre-determined roll data. If not provided, finds the action's own roll data.
   * @param {boolean} [options.isolated] - Are these labels generated for isolated information (that is, need more details)
   * @returns {Record<string, string>} This action's labels
   */
  getLabels({ rollData, isolated = false } = {}) {
    const labels = {};
    rollData ??= this.getRollData();

    const hasActor = !!this.actor;

    // Activation method
    if (this.activation) {
      const activation = this.activation;
      if (activation) {
        const isUnchainedActionEconomy = game.settings.get("pf1", "unchainedActionEconomy");
        const activationTypes = isUnchainedActionEconomy
          ? pf1.config.abilityActivationTypes_unchained
          : pf1.config.abilityActivationTypes;
        const activationTypesPlural = isUnchainedActionEconomy
          ? pf1.config.abilityActivationTypesPlurals_unchained
          : pf1.config.abilityActivationTypesPlurals;

        const activationType = activation.type || "nonaction";
        if (activation.type === "special") {
          labels.activation = activationTypes.special;
        } else if (activation.cost > 1 && !!activationTypesPlural[activationType]) {
          labels.activation = [activation.cost.toString(), activationTypesPlural[activationType]].filterJoin(" ");
        } else {
          labels.activation = [
            ["minute", "hour", "action"].includes(activationType) && activation.cost ? activation.cost.toString() : "",
            activationTypes[activationType],
          ].filterJoin(" ");
        }
      }
    }

    // Duration
    // Set duration label
    const duration = this.duration;
    switch (duration?.units) {
      case "spec":
        labels.duration = duration.value;
        break;
      case "seeText":
      case "inst":
      case "perm":
        labels.duration = pf1.config.timePeriods[duration.units];
        break;
      case "turn": {
        const unit = pf1.config.timePeriods[duration.units];
        labels.duration = game.i18n.format("PF1.Time.Format", { value: 1, unit });
        break;
      }
      case "round":
      case "minute":
      case "hour":
      case "day":
      case "month":
      case "year":
        if (duration.value) {
          const unit = pf1.config.timePeriods[duration.units];
          labels.durationFormula = duration.value;
          labels.variableDuration = /@\w/.test(duration.value);
          let roll, value;
          try {
            roll = new RollPF(duration.value, rollData);
            // Outside of actor
            if (isolated) {
              const _rollData = foundry.utils.deepClone(rollData);
              const minroll = new RollPF(duration.value, _rollData);
              minroll.evaluateSync({ minimize: true });
              // Spoof level 20
              _rollData.cl = 20;
              foundry.utils.setProperty(_rollData, "attributes.hd.total", 20);
              foundry.utils.setProperty(_rollData, "attributes.bab.total", 20);
              _rollData.bab = 20;
              foundry.utils.setProperty(_rollData, "class.level", 20);
              const maxroll = new RollPF(duration.value, _rollData);
              maxroll.evaluateSync({ maximize: true });
              if (minroll.total !== maxroll.total) value = `${minroll.total} - ${maxroll.total}`;
              else value = `${minroll.total}`;
            }
            // On actor
            else {
              if (roll.isDeterministic) {
                roll.evaluateSync();
                value = roll.total;
              } else {
                let formula = pf1.utils.formula.unflair(duration.value || "0");
                formula = RollPF.replaceFormulaData(formula || "0", rollData);
                value = pf1.utils.formula.compress(pf1.utils.formula.simplify(formula));
              }
            }

            labels.duration = game.i18n.format("PF1.Time.Format", { value, unit });
          } catch (err) {
            console.error(
              "Error in duration formula:",
              { formula: duration.value, rollData, roll },
              roll?.err ?? err,
              this
            );
          }
        }
        break;
    }

    // Dismissable, but only if special duration isn't used
    // TODO: Better i18n support
    if (labels.duration && duration.units !== "spec") {
      if (duration.dismiss) labels.duration += " " + game.i18n.localize("PF1.DismissableMark");
      if (duration.concentration)
        labels.duration = game.i18n.format("PF1.ConcentationDuration", { duration: labels.duration });
    }

    // Difficulty Class
    if (this.hasSave) {
      const totalDC = rollData.dc + (rollData.dcBonus ?? 0);
      labels.save = game.i18n.format("PF1.DCThreshold", { threshold: totalDC });
    }

    // Range
    if (this.hasRange) {
      const rangeUnit = this.range.units;
      const rangeValue = this.range.value;
      const rangeLabel = pf1.config.distanceUnits[rangeUnit];
      labels.range = rangeLabel;
      if (rangeUnit === "spec") {
        labels.range = rangeValue || labels.range;
      } else if (["personal", "touch", "melee", "reach"].includes(rangeUnit)) {
        // Display as is
      } else {
        const range = this.getRange({ type: "single", rollData });
        if (range > 0) {
          const usystem = pf1.utils.getDistanceSystem();
          const rangeUnit = usystem === "metric" ? "m" : "ft";
          const lrange = new Intl.NumberFormat(undefined).format(range);
          labels.range = `${lrange} ${rangeUnit}`;
        }
        if (["close", "medium", "long"].includes(rangeUnit)) {
          labels.range += ` (${rangeLabel})`;
        }
      }

      // Special formatting when no actor present
      if (!hasActor) {
        const units = pf1.utils.getDistanceSystem();
        switch (rangeUnit) {
          case "close":
            labels.range = `${rangeLabel} (${game.i18n.localize(
              units == "metric" ? "PF1.SpellRangeShortMetric" : "PF1.SpellRangeShort"
            )})`;
            break;
          case "medium":
            labels.range = `${rangeLabel} (${game.i18n.localize(
              units == "metric" ? "PF1.SpellRangeMediumMetric" : "PF1.SpellRangeMedium"
            )})`;
            break;
          case "long":
            labels.range = `${rangeLabel} (${game.i18n.localize(
              units == "metric" ? "PF1.SpellRangeLongMetric" : "PF1.SpellRangeLong"
            )})`;
            break;
        }
      }
    }

    // Targets
    const targets = this.target?.value;
    if (targets) labels.targets = targets;

    // Set area label
    if (this?.area) labels.area = this.area;

    // Action type
    labels.actionType = pf1.config.itemActionTypes[this.actionType];

    return labels;
  }

  // -----------------------------------------------------------------------

  /**
   * Get all appropriate context changes for attack rolls.
   *
   * @see {@link ItemPF.getContextChanges}
   */
  get attackSources() {
    const contexts = ["~attackCore"];
    if (this.isCombatManeuver) contexts.push("cmb");
    if (this.isRanged) contexts.push("rattack");
    else contexts.push("mattack");

    // TODO: Query from item itself
    const isNatural = this.item.subType === "natural";
    if (isNatural) contexts.push("nattack");

    switch (this.actionType) {
      case "twak":
        contexts.push("tattack");
        if (!isNatural) contexts.push("wattack");
        break;
      case "mwak":
      case "rwak":
        if (!isNatural) contexts.push("wattack");
        break;
      case "msak":
      case "rsak":
        contexts.push("sattack");
        break;
    }

    return this.item.getContextChanges(contexts);
  }

  /**
   * Place an attack roll using an item (weapon, feat, spell, or equipment)
   *
   * @param {object} [options] - Options
   * @param {object} [options.data] - Roll data
   * @param {Array<string>} [options.extraParts] - Additional attack parts
   * @param {string} [options.bonus] - Additional attack bonus
   * @param {boolean} [options.primary=true] - Treat as primary natural attack
   * @param {pf1.components.ItemChange[]} [options.extraChanges] - Additional changes
   * @returns {Promise<D20RollPF>}
   */
  async rollAttack({ data = null, extraParts = [], extraChanges = [], bonus = null, primary = true, ...options } = {}) {
    if (typeof options.primaryAttack === "boolean") {
      foundry.utils.logCompatibilityWarning(
        "ItemAttack.rollAttack()'s `primaryAttack` option is deprecated in favor of `primary`",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      primary = options.primaryAttack;
    }
    const rollData = data ?? this.getRollData();
    const itemData = rollData.item;
    const actionData = rollData.action;

    const config = {};

    itemData.primaryAttack = primary;

    // Add misc bonuses/penalties
    itemData.proficiencyPenalty = -4;

    // Determine ability score modifier
    const abl = actionData.ability.attack;

    // Define Roll parts
    const parts = [];

    // Add size bonus
    if (rollData.sizeBonus !== 0) parts.push(`@sizeBonus[${game.i18n.localize("PF1.Size")}]`);

    const ability = rollData.abilities?.[abl];
    // Add ability modifier
    if (ability && ability?.mod !== 0) {
      parts.push(`@abilities.${abl}.mod[${pf1.config.abilities[abl]}]`);
    }

    // Get relevant changes
    const changes = this.attackSources;
    changes.push(...extraChanges);

    // Add masterwork bonus to changes (if applicable)
    if (["mwak", "rwak", "twak", "mcman", "rcman"].includes(this.actionType) && this.item.system.masterwork) {
      changes.push(
        new pf1.components.ItemChange({
          formula: "1",
          operator: "add",
          target: "attack",
          type: "enh",
          value: 1,
          flavor: game.i18n.localize("PF1.EnhancementBonus"),
        })
      );
    }

    // Add enhancement bonus to changes
    if (this.enhancementBonus) {
      changes.push(
        new pf1.components.ItemChange({
          formula: this.enhancementBonus.toString(),
          operator: "add",
          target: "attack",
          type: "enh",
          value: this.enhancementBonus,
          flavor: game.i18n.localize("PF1.EnhancementBonus"),
        })
      );
    }

    // Add bonus parts
    parts.push(...extraParts);
    // Add attack bonus
    if (typeof actionData.attackBonus === "string" && !["", "0"].includes(actionData.attackBonus)) {
      parts.push(actionData.attackBonus);
    }
    // Backwards compatibility
    else if (typeof actionData.attackBonus === "number") {
      itemData.attackBonus = actionData.attackBonus;
      parts.push(`@item.attackBonus[${game.i18n.localize("PF1.AttackRollBonus")}]`);
    }

    // Add proficiency penalty
    try {
      config.proficient = this.item.getProficiency(true);
    } catch (error) {
      // Treat as proficient if there's proficiency incompatibility.
      config.proficient = true;
    }

    // Add secondary natural attack penalty
    const isNatural = this.item.subType === "natural";
    const isNaturalSecondary = isNatural && primary === false;
    config.secondaryPenalty = isNaturalSecondary ? pf1.config.naturalAttacks.secondary.penalty : "0";

    // Add bonus
    rollData.bonus = 0;
    if (bonus) {
      // TODO: Do not pre-roll
      const roll = await RollPF.safeRoll(bonus, rollData);
      rollData.bonus = roll.total;
    }

    // Options for D20RollPF
    const rollOptions = {
      critical: this.critRange,
    };

    if (this.ammo.type && this.ammo.cost > 0) {
      const misfire = this.misfire;
      if (misfire > 0) rollOptions.misfire = misfire;
    }

    // call pre attack hook before changes are filtered and before specific [parts] from config and roll data are created
    Hooks.call("pf1PreAttackRoll", this, config, rollData, rollOptions, parts, changes);

    // Get attack bonus
    const hightest = getHighestChanges(
      changes.filter((c) => {
        c.applyChange(this.actor);
        return c.operator !== "set";
      }),
      { ignoreTarget: true }
    );

    for (const c of hightest) {
      let value = c.value;
      // BAB override
      if (actionData.bab && c._id === "_bab") {
        value = RollPF.safeRollSync(c.formula, data).total || 0;
      }
      if (value == 0) continue;
      parts.push(`${value}[${RollPF.cleanFlavor(c.flavor)}]`);
    }

    // Convert config to roll part
    if (config.secondaryPenalty != 0) {
      const terms = RollPF.parse(config.secondaryPenalty);
      if (terms.length > 1) config.secondaryPenalty = `(${config.secondaryPenalty})`;
      parts.push(`${config.secondaryPenalty}[${game.i18n.localize("PF1.SecondaryAttack")}]`);
    }

    if (rollData.bonus != 0) {
      parts.push(`@bonus[${game.i18n.localize("PF1.SituationalBonus")}]`);
    }

    if (!config.proficient) {
      parts.push(`@item.proficiencyPenalty[${game.i18n.localize("PF1.Proficiency.Penalty")}]`);
    }

    const roll = new pf1.dice.D20RollPF(
      [rollData.d20 || pf1.dice.D20RollPF.standardRoll, ...parts.filter((p) => !!p)].join("+"),
      rollData,
      rollOptions
    );

    try {
      await roll.evaluate();
    } catch (err) {
      throw new Error(`Attack roll evaluation failed:\n${roll.formula}\n`, { cause: err });
    }

    // Cleanup roll data that was altered here.
    delete rollData.bonus;

    Hooks.call("pf1AttackRoll", this, roll, config);

    return roll;
  }

  /* -------------------------------------------- */

  /**
   * Roll damage for an action.
   *
   * @param {object} [options] - Options configuring the damage roll
   * @param {object | null} [options.data=null] - rollData to be used
   * @param {boolean} [options.critical=false] - Whether to roll critical damage
   * @param {string[]} [options.extraParts] - Additional strings added to the roll formula
   * @param {object} [options.conditionalParts=[]] - Conditional data sets
   * @param {boolean} [options.primaryAttack] - Whether this is the primary attack
   * @returns {Promise<DamageRoll[]>} Created damage rolls, one roll per damage part
   */
  async rollDamage({
    data = null,
    critical = false,
    extraParts = [],
    conditionalParts = {},
    primaryAttack = true,
  } = {}) {
    const rollData = data ?? this.getRollData();
    const itemData = rollData.item;
    const actionData = rollData.action;

    if (!this.hasDamage) {
      throw new Error("You may not make a Damage Roll with this Action.");
    }

    const isNatural = this.item.subType === "natural";

    // Determine critical multiplier
    rollData.critMult = 1;
    if (critical) rollData.critMult = actionData.ability.critMult;
    // Determine ability multiplier
    if (rollData.ablMult == null) {
      const held = actionData?.held || itemData?.held || "1h";
      rollData.ablMult =
        actionData?.ability.damageMult ?? (isNatural ? null : pf1.config.abilityDamageHeldMultipliers[held]) ?? 1;
    }

    // Define Roll parts
    const parts = [];
    const addParts = (property, type) => {
      parts.push(
        ...(this.damage[property]?.map((damage) => ({
          base: damage.formula,
          extra: [],
          damageType: damage.types,
          type,
        })) ?? [])
      );

      // add typed conditionals
      conditionalParts[`damage.${type}`]?.forEach((p) => {
        const [base, damageType, isExtra] = p;
        isExtra ? parts[0].extra.push(base) : parts.push({ base, extra: [], damageType, type });
      });
    };

    addParts("parts", "normal");
    if (critical) addParts("critParts", "crit");
    else addParts("nonCritParts", "nonCrit");

    /**
     * Initialize changes to empty array so mods can still add changes for healing "attacks" via the pre-roll hook below
     *
     * @type {ItemChange[]}
     */
    let changes = [];
    if (!this.isHealing) {
      // Gather changes
      changes = this.damageSources;

      // Add enhancement bonus to changes
      if (this.enhancementBonus) {
        changes.push(
          new pf1.components.ItemChange({
            formula: this.enhancementBonus.toString(),
            operator: "add",
            target: "damage",
            type: "enh",
            value: this.enhancementBonus,
            flavor: game.i18n.localize("PF1.EnhancementBonus"),
          })
        );
      }

      // Add broken penalty
      if (this.item.isBroken) {
        const label = game.i18n.localize("PF1.Broken");
        parts[0].extra.push(`-2[${label}]`);
      }
    }

    // call pre damage hook before changes are filtered and before specific [parts] from roll data are created
    Hooks.call("pf1PreDamageRoll", this, rollData, parts, changes);

    // Get damage bonus
    getHighestChanges(
      changes.filter((c) => {
        c.applyChange(this.actor);
        return c.operator !== "set";
      }),
      { ignoreTarget: true }
    ).forEach((c) => {
      let value = c.value;
      // Put in parenthesis if there's a chance it is more complex
      if (/[\s+-?:]/.test(value)) value = `(${value})`;
      parts[0].extra.push(`${value}[${c.flavor}]`);
    });

    // Determine ability score modifier
    const abl = actionData.ability.damage;
    const ability = rollData.abilities?.[abl];
    if (ability) {
      // Determine ability score bonus
      const max = actionData.ability?.max ?? Infinity;
      if (ability.mod < 0) rollData.ablDamage = Math.min(max, ability.mod);
      else rollData.ablDamage = Math.floor(Math.min(max, ability.mod) * rollData.ablMult);

      // Determine ability score label
      const ablLabel = pf1.config.abilities[abl];

      // Add ability score
      parts[0].extra.push(`@ablDamage[${ablLabel}]`);
    }

    // Ensure previous data does not infect things here
    delete rollData.previous;
    delete rollData.base;

    // Create roll
    const rolls = [];
    for (let a = 0; a < parts.length; a++) {
      const part = parts[a];
      let rollParts = [];
      if (a === 0) rollParts = [...part.extra, ...extraParts];
      const formula = [part.base, ...rollParts].join(" + ");
      // Skip empty formulas instead of erroring on them
      if (formula.length == 0) continue;
      let roll;
      try {
        roll = await new DamageRoll(formula, rollData, {
          damageType: part.damageType,
          type: part.type,
        }).evaluate();
        // Add to result
        rolls.push(roll);
      } catch (err) {
        console.error("Error with damage formula:", formula, this);
        throw err;
      }

      const firstDie = roll.dice[0];

      // Add first normal damage instance as @base damage data in roll data
      if (a === 0 && !critical) {
        rollData.base = {
          total: roll.total,
          dice: firstDie?.expression,
          die: { size: firstDie?.faces, count: firstDie?.number },
        };
      }

      // Store some details as @previous
      rollData.previous = {
        total: roll.total,
        dice: firstDie?.expression,
        die: { size: firstDie?.faces, count: firstDie?.number },
      };
    }

    return rolls;
  }

  /**
   * Generates a list of targets this modifier can have.
   *
   * @returns {Record<string, string>} Target ID to label mapping.
   */
  getConditionalTargets() {
    const hasAttack = this.hasAttack,
      hasDamage = this.hasDamage,
      hasSave = this.hasSave;

    const results = [
      { id: "attack", sort: 1_000, label: pf1.config.conditionalTargets.attack._label, disabled: !hasAttack },
      {
        id: "critMult",
        sort: 2_000,
        label: pf1.config.conditionalTargets.critMult._label,
        disabled: !hasAttack,
        simple: true,
      },
      { id: "damage", sort: 3_000, label: pf1.config.conditionalTargets.damage._label, disabled: !hasDamage },
      { id: "size", sort: 4_000, label: pf1.config.conditionalTargets.size._label, disabled: !hasDamage, simple: true },
      { id: "dc", sort: 5_000, label: game.i18n.localize("PF1.DC"), disabled: !hasSave, simple: true },
      {
        id: "effect",
        sort: 6_000,
        label: pf1.config.conditionalTargets.effect._label,
        choices: {},
        get disabled() {
          return foundry.utils.isEmpty(this.choices);
        },
      },
      {
        id: "misc",
        sort: 7_000,
        label: pf1.config.conditionalTargets.misc._label,
        choices: {},
        get disabled() {
          return foundry.utils.isEmpty(this.choices);
        },
      },
      {
        id: "charges",
        sort: 8_000,
        label: game.i18n.localize("PF1.ChargeCost"),
        disabled: !this.isCharged,
        simple: true,
      },
    ];

    // Add additional item-specific targets
    this.item.getConditionalTargets(results);

    // Fill in sub-targets
    for (const entry of results) {
      this.getConditionalSubTargets(entry);
    }

    // Convert results into its final form
    const result = {};
    for (const entry of results.sort((a, b) => a.sort - b.sort)) {
      if (entry.disabled) continue;
      if (!entry.simple && foundry.utils.isEmpty(entry.choices)) continue;
      result[entry.id] = entry;
    }

    return result;
  }

  /**
   * Generates lists of conditional sub-targets this action can have.
   *
   * @param {object} entry - The target entry
   * @param {*} _results - Unused. Deprecated option
   * @returns {object} - Same as the target entry parameter with added info.
   */
  getConditionalSubTargets(entry, _results) {
    // @deprecated - Remove after PF1 v12
    if (typeof entry === "string" || _results)
      throw new Error("ItemAction#getConditionalSubTargets() called with invalid arguments");

    entry.choices ??= {};

    const targetId = entry.id;

    // Add static targets
    const subTargets = pf1.config.conditionalTargets[targetId];
    if (subTargets) {
      for (const [key, label] of Object.entries(subTargets)) {
        if (!key.startsWith("_") && !key.startsWith("~")) entry.choices[key] = label;
      }
    }

    // Add subtargets depending on attacks
    if (["attack", "damage"].includes(targetId)) {
      // Add specific attacks
      if (this.hasAttack) {
        entry.choices["attack_0"] = `${game.i18n.localize("PF1.Attack")} 1`;

        const exAtk = this.extraAttacks;
        if (exAtk?.manual?.length) {
          exAtk.manual.forEach((part, index) => {
            entry.choices[`attack_${index + 1}`] = part.name;
          });
        }
      } else {
        delete entry.choices["rapidShotDamage"];
      }
    }

    this.item.getConditionalSubTargets?.(entry);

    return entry;
  }

  /* Generates lists of conditional modifier bonus types applicable to a formula.
   * @param {string} target - The target key as defined in PF1.conditionTargets.
   * @returns {Object.<string, string>} A list of bonus types.
   * */
  getConditionalModifierTypes(target) {
    const result = {};
    if (target === "attack" || target === "damage") {
      // Add types from pf1.config.bonusTypes
      for (const [k, v] of Object.entries(pf1.config.bonusTypes)) {
        result[k] = v;
      }
    }
    if (target === "damage") {
      for (const damageType of pf1.registry.damageTypes) {
        result[damageType.id] = damageType.name;
      }
    }
    return result;
  }

  /* Generates a list of critical applications for a given formula target.
   * @param {string} target - The target key as defined in PF1.conditionalTargets.
   * @returns {Object.<string, string>} A list of critical applications.
   * */
  getConditionalCritical(target) {
    let result = {};
    // Attack bonuses can only apply as critical confirm bonus
    if (target === "attack") {
      result = { ...result, normal: "PF1.Normal", crit: "PF1.CriticalConfirmBonus" };
    }
    // Damage bonuses can be multiplied or not
    if (target === "damage") {
      result = { ...result, normal: "PF1.Normal" };
      if (this.hasAttack) {
        result = { ...result, crit: "PF1.OnCritBonusFormula", nonCrit: "PF1.NonMultBonusFormula" };
      }
    }
    return result;
  }

  /**
   * Generate attacks.
   *
   * @param {object} [options] - Options
   * @param {boolean} [options.full=true] - Full attack
   * @param {object} [options.rollData] - Roll data
   * @param {boolean} [options.resolve=false] - If the bonuses are to be resolved directly.
   * @param {boolean} [options.conditionals=false] - Include conditional modifications. Requires `resolve` to be enabled.
   * @param {boolean} [options.bonuses=false] - Include other bonuses. Requires `resolve` to be enabled.
   * @returns {Array<object>} - Array of attack data
   */
  getAttacks({ full = true, rollData, resolve = false, conditionals = false, bonuses = false } = {}) {
    rollData ||= this.getRollData();

    const exAtkCfg = pf1.config.extraAttacks[this.extraAttacks?.type] ?? {};

    const bonusToAll = exAtkCfg.modToAll;

    /**
     * Counter for unnamed or other numbered attacks, to be incremented with each usage.
     * Starts at 1 to account for the base attack.
     */
    let unnamedAttack = 0;
    const unnamedAttackNames = new Set();
    const getUniqueName = (name, template) => {
      if (template && template.indexOf("{0}") == -1) template = null;
      let label = name;
      while (unnamedAttackNames.has(label) || !label) {
        unnamedAttack += 1;
        if (template) {
          if (game.i18n.has(template)) label = game.i18n.format(template, { 0: unnamedAttack });
          else label = template.replace("{0}", unnamedAttack);
        } else label = game.i18n.format("PF1.ExtraAttacks.Formula.LabelDefault", { 0: unnamedAttack });
      }
      unnamedAttackNames.add(label);
      return label;
    };

    rollData.attackCount = 0;

    // Replace roll data that won't be available after
    const replaceSpecificRollData = (formula, data) => {
      return formula.replace(/@\w+\b/, (m) => {
        const p = m.slice(1);
        if (p in data) return data[p];
        return m;
      });
    };

    const _rollData = {
      attackCount: 0,
      attackSetCount: 0,
      formulaicAttack: 0,
    };

    const flavor = game.i18n.localize(exAtkCfg.flavor || "");
    let formula = `(${exAtkCfg.bonus || "0"} + ${bonusToAll || "0"})` + (flavor ? `[${flavor}]` : "");
    formula = replaceSpecificRollData(formula, _rollData);
    formula = pf1.utils.formula.simplify(formula); // Reduce (0 + 0) to 0 or other similar cases
    const attacks = [{ bonus: formula, label: getUniqueName(this.attackName) }];

    // Extra attacks
    if (full) {
      rollData.fullAttack = 1;
      const unchainedEconomy = game.settings.get("pf1", "unchainedActionEconomy");

      let attackCount = 0;

      const parseAttacks = async (countFormula, bonusFormula, label, bonusLabel) => {
        // Soft assign zero formula to deal with empty string causing parameter init to fail
        countFormula ||= "0";
        bonusFormula ||= "0";
        if (!countFormula || countFormula == "0") return;

        const exAtkCount =
          RollPF.safeRollSync(countFormula, rollData, undefined, undefined, { minimized: true })?.total ?? 0;
        if (exAtkCount <= 0) return;

        try {
          for (let i = 0; i < exAtkCount; i++) {
            const _rollData = {
              attackCount: (attackCount += 1),
              attackSetCount: i,
              formulaicAttack: i + 1, // Add and update attack counter
            };

            let formula = bonusFormula;
            if (bonusToAll) formula += ` + ${bonusToAll}`;
            formula = replaceSpecificRollData(formula, _rollData);

            const alabel = game.i18n.has(label) ? game.i18n.format(label, { 0: i + 1 }) : label?.replace("{0}", i + 1);

            attacks.push({
              bonus: bonusLabel ? `(${formula})[${bonusLabel}]` : `(${formula})`,
              formula,
              flavor: bonusLabel,
              // Continue counting if similar to initial attack name
              // If formulaic attacks have a non-default name, number them with their own counter; otherwise, continue unnamed attack numbering
              label: getUniqueName(alabel, label),
              rollData: _rollData,
            });
          }
        } catch (err) {
          console.error(err);
        }
      };

      if (exAtkCfg.iteratives && !unchainedEconomy) {
        parseAttacks(
          pf1.config.iterativeExtraAttacks,
          pf1.config.iterativeAttackModifier,
          null,
          game.i18n.localize("PF1.Iterative")
        );
      }

      // Add attacks defined by configuration
      if (exAtkCfg.count) {
        parseAttacks(exAtkCfg.count, exAtkCfg.bonus, exAtkCfg.attackName, flavor);
      }

      // Add manually entered explicit extra attacks
      if (exAtkCfg.manual) {
        const extraAttacks = this.extraAttacks?.manual ?? [];
        for (const { name, formula } of extraAttacks) {
          if (name) unnamedAttackNames.add(name);
          attacks.push({
            bonus: formula,
            // Use defined label, or fall back to continuously numbered default attack name
            label: name || getUniqueName(),
          });
        }
      }

      // Add custom extra attack formula
      if (exAtkCfg.formula) {
        const formulaCfg = this.extraAttacks.formula ?? {};
        parseAttacks(formulaCfg.count, formulaCfg.bonus, formulaCfg.label);
      }
    }

    // TODO: Move this to be part of the output data as formulas
    if (resolve) {
      const condBonuses = new Array(attacks.length).fill(0);
      if (conditionals) {
        // Conditional modifiers
        const conds = this.conditionals.filter((c) => c.default && c.modifiers.find((sc) => sc.target === "attack"));
        for (const c of conds) {
          for (const cc of c.modifiers) {
            const bonusRoll = RollPF.safeRollSync(cc.formula, rollData, undefined, undefined, { minimize: true });
            if (bonusRoll.total == 0) continue;
            if (cc.subTarget?.match(/^attack\.(\d+)$/)) {
              const atk = parseInt(RegExp.$1, 10);
              if (atk in condBonuses) condBonuses[atk] += bonusRoll.total;
            }
          }
        }
      }

      let totalBonus = 0;
      if (bonuses) {
        const sources = this.item.getAttackSources(this.id, { rollData });
        totalBonus = sources.reduce((total, src) => {
          let value = src.value;
          if (typeof value === "string") {
            const roll = RollPF.safeRollSync(value, rollData, undefined, undefined, { minimize: true });
            value = roll.total;
          }
          return total + value;
        }, 0);
      }

      attacks.forEach((atk, i) => {
        rollData.attackCount = i;
        const roll = RollPF.safeRollSync(atk.bonus, rollData, undefined, undefined, { minimize: true });
        atk.bonus = roll.total + totalBonus + condBonuses[i];
        delete rollData.attackCount;
      });
    }

    return attacks;
  }

  /**
   * Use action.
   *
   * Wrapper for {@link pf1.documents.item.ItemPF.use() ItemPF.use()}
   *
   * @param {object} options - Options passed to `ItemPF.use()`.
   * @returns {Promise<void>} - Returns what `ItemPF.use()` returns.
   */
  async use(options = {}) {
    options.actionId = this.id;

    // TODO: ItemPF.use() and this.use() relation needs to be flipped.

    return this.item.use(options);
  }

  /**
   * Prune data
   *
   * @internal
   * @param {object} data - Raw data (e.g. product of {@link toObject()})
   */
  static pruneData(data) {
    // Aggressive data size reduction

    if (!data.img) delete data.img;
    if (!data.tag) delete data.tag;
    if (!data.bab) delete data.bab;
    if (!data.attackName) delete data.attackName;
    if (!data.attackBonus) delete data.attackBonus;
    if (!data.critConfirmBonus) delete data.critConfirmBonus;
    if (!data.measureTemplate?.type) delete data.measureTemplate;
    if (!data.extraAttacks?.type) delete data.extraAttacks;
    if (data.extraAttacks) {
      if (!data.extraAttacks.formula) delete data.extraAttacks.formula;
      if (!data.extraAttacks.count) delete data.extraAttacks.count;
      if (!data.extraAttacks.label) delete data.extraAttacks.label;
      if (data.extraAttacks.manual?.length == 0) delete data.extraAttacks.manual;
    }
    if (!data.uses?.self?.per) delete data.uses?.self;
    if (data.save && !data.save.type) {
      // Preserve description even if there's no save (TODO: Maybe cull it anyway?)
      if (!data.save.description) delete data.save.description;
      // RAW preserving harmless=true here is pointless if there's no save.
      if (data.save.harmless === false) delete data.save.harmless;
      if (data.save.harmless !== true && !data.save.description) delete data.save;
      else {
        delete data.save.type;
        delete data.save.dc;
      }
    }
    if (!data.held) delete data.held;
    if (!data.duration?.units) delete data.duration;
    if (data.duration?.dismiss === false) delete data.duration.dismiss;
    if (data.duration?.concentration === false) delete data.duration.concentration;
    if (!data.target?.value) delete data.target;
    if (!data.uses?.autoDeductChargesCost) delete data.uses?.autoDeductChargesCost;
    if (data.uses?.perAttack === false) delete data.uses.perAttack;

    if (data.ability?.max === null) delete data.ability.max;

    if (!data.area) delete data.area;
    if (!data.effect) delete data.effect;
    if (data.notes?.effect) {
      if (data.notes.effect.length === 0) delete data.notes.effect;
    }
    if (data.notes?.footer) {
      if (data.notes.footer.length === 0) delete data.notes.footer;
    }

    if (!data.range?.units) delete data.range;
    else {
      if (!data.range?.minUnits) delete data.range?.minValue;
      if (data.range?.maxIncrements === 1) delete data.range?.maxIncrements;
    }

    if (data.damage) {
      if (data.damage.parts?.length == 0) delete data.damage.parts;
      if (data.damage.critParts?.length == 0) delete data.damage.critParts;
      if (data.damage.nonCritParts?.length == 0) delete data.damage.nonCritParts;
      if (Object.keys(data.damage).length == 0) delete data.damage;
    }

    if (data.material) {
      if (!data.material.normal?.value) delete data.material?.normal;
      if (!(data.material.addon?.length > 0)) delete data.material?.addon;
      if (Object.keys(data.material).length == 0) delete data.material;
    }

    // Diff based cleanup (don't do this for everything, to avoid defaults changing causing problems)
    const defaults = new this().toObject(true, false);
    const diff = foundry.utils.diffObject(defaults, data);
    if (!diff.naturalAttack) delete data.naturalAttack;
    if (!diff.alignments) delete data.alignments;

    // Prune child models
    // HACK: .toObject() only clones _source and thus never calls .toObject() on the child models
    if (data.damage) {
      for (const parts of Object.values(data.damage)) {
        for (const part of parts) {
          DamagePartModel.pruneData(part);
        }
      }
    }

    if (data.extraAttacks?.manual?.length) {
      for (const exAtk of data.extraAttacks.manual) {
        ExtraAttackModel.pruneData(exAtk);
      }
    }
  }

  /**
   * Current sheet.
   *
   * @remarks
   * - Initializes a new sheet if one doesn't exist yet.
   *
   * @type {pf1.applications.component.ItemActionSheet}
   */
  get sheet() {
    this._sheet ??= new pf1.applications.component.ItemActionSheet(this);
    return this._sheet;
  }

  /**
   * Render all connected application instances.
   *
   * @param {boolean} [force=false] - Force rendering
   * @param {object} [context={}] - Optional context
   */
  render(force = false, context = {}) {
    // TODO: Support AppV2
    Object.values(this.apps).forEach((app) => app.render(force, context));
  }

  /* DEPRECATIONS */

  /** @deprecated - No replacement */
  static get defaultData() {
    foundry.utils.logCompatibilityWarning("ItemAction.defaultData has been deprecated with no replacement.", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    return new this().toObject(undefined, false);
  }

  /**
   * @deprecated - Use the action directly instead
   * @returns {this}
   */
  get data() {
    foundry.utils.logCompatibilityWarning(
      "ItemAction.data has been deprecated. Use the data directly on the action instead.",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );

    return this;
  }

  /** @deprecated - No replacement */
  static get defaultDamageType() {
    foundry.utils.logCompatibilityWarning("ItemAction.defaultDamageType is deprecated with no replacement.", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    return [];
  }

  /**
   * Has sound effect?
   *
   * @deprecated - Test `!!action.soundEffect` instead.
   * @type {boolean}
   */
  get hasSound() {
    foundry.utils.logCompatibilityWarning(
      "ItemAction.hasSound is deprecated with no replacement. Test !!action.soundEffect instead.",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );
    return !!this.soundEffect;
  }

  /**
   * Effective ammo type.
   *
   * @deprecated - Use `action.ammo.type` instead.
   * @type {string|null}
   */
  get ammoType() {
    foundry.utils.logCompatibilityWarning("ItemAction.ammoType is deprecated in favor of ItemAction.ammo.type", {
      since: "PF1 v11",
      until: "PF1 v12",
    });
    return this.ammo.type;
  }

  /**
   * Effective per-attack ammo cost.
   *
   * @deprecated - Use `action.ammo.cost` instead.
   * @type {number}
   */
  get ammoCost() {
    foundry.utils.logCompatibilityWarning("ItemAction.ammoCost is deprecated in favor of ItemAction.ammo.cost", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    return this.ammo.cost;
  }

  /**
   * @deprecated - Use `action.effect` instead
   */
  get spellEffect() {
    foundry.utils.logCompatibilityWarning("ItemAction.spellEffect is deprecated in favor of ItemAction.effect", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    return this.effect;
  }

  /** @deprecated - Use `action.notes.footer` instead */
  get attackNotes() {
    foundry.utils.logCompatibilityWarning("ItemAction.attackNotes is deprecated in favor of ItemAction.notes.footer", {
      since: "PF1 v11",
      until: "PF1 v12",
    });
    return this.notes?.footer;
  }

  /** @deprecated - Use `action.notes.effect` instead */
  get effectNotes() {
    foundry.utils.logCompatibilityWarning("ItemAction.effectNotes is deprecated in favor of ItemAction.notes.effect", {
      since: "PF1 v11",
      until: "PF1 v12",
    });
    return this.notes?.effect;
  }
}
