import { ActorSheetPF } from "./actor-sheet.mjs";

/**
 * An Actor sheet for Vehicle type characters in the game system.
 * Extends the base ActorSheetPF class.
 */
export class ActorSheetPFVehicle extends ActorSheetPF {
  /**
   * Define default rendering options for the NPC sheet
   *
   * @returns {object} The default rendering options
   */
  static get defaultOptions() {
    const options = super.defaultOptions;
    return {
      ...options,
      classes: [...options.classes, "vehicle"],
      width: 800,
      height: 680,
      tabs: [{ navSelector: "nav.tabs", contentSelector: "section.primary-body", initial: "summary" }],
      scrollY: [".tab.summary"],
    };
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   *
   * @type {string}
   */
  get template() {
    if (this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "systems/pf1/templates/actors/vehicle-sheet.hbs";
  }

  /* -------------------------------------------- */

  /** @override */
  static EDIT_TRACKING = ["system.details.description", "system.details.notes"];

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   *
   * @override
   * @returns {Promise<object>} The data object to render the sheet with
   */
  async getData() {
    const isOwner = this.document.isOwner;
    const context = {
      owner: isOwner,
      appId: this.appId,
      system: this.actor.system,
      limited: this.document.limited,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: pf1.config,
      isGM: game.user.isGM,
      labels: {
        currency: `PF1.Currency.Inline.${this.itemValueDenomination}`,
      },
      isLootSheet: true, // inventory include unwanted data otherwise,
      fields: this.actor.system.schema.fields,
    };

    context.vehicleSizes = Object.fromEntries(
      Object.entries(pf1.config.vehicles.size).map(([key, data]) => [key, data.label])
    );

    // Enrich descriptions
    const enrichHTMLOptions = {
      secrets: isOwner,
      rollData: context.rollData,
      relativeTo: this.actor,
    };

    const noDesc = "<p>" + game.i18n.localize("PF1.NoDescription") + "</p>";

    const desc = context.system.details?.description;
    const pDesc = desc ? pf1.utils.enrichHTMLUnrolled(desc, enrichHTMLOptions) : Promise.resolve();
    pDesc.then((html) => (context.descriptionHTML = html || noDesc));
    const notes = context.system.details?.notes;
    const pNotes = notes ? pf1.utils.enrichHTMLUnrolled(notes, enrichHTMLOptions) : Promise.resolve();
    pNotes.then((html) => (context.notesHTML = html));
    await Promise.all([pDesc, pNotes]);

    // The Actor and its Items
    context.actor = this.actor;
    context.token = this.token;
    context.items = this.document.items
      .map((item) => this._prepareItem(item))
      .sort((a, b) => (a.sort || 0) - (b.sort || 0));

    // Prepare owned items
    this._prepareItems(context);

    //context.sellMultiplier = this.actor.getFlag("pf1", "sellMultiplier");

    const baseCurrency = this.actor.getTotalCurrency({ inLowestDenomination: true });
    context.hasCurrency = true; // Never fade currency field for this

    // Get total value
    const cpValue = this.calculateTotalItemValue({ inLowestDenomination: true, recursive: true }) + baseCurrency;
    const cpSellValue = this.calculateSellItemValue({ inLowestDenomination: true, recursive: true }) + baseCurrency;

    context.totalValue = pf1.utils.currency.split(cpValue, { pad: true });
    context.sellValue = pf1.utils.currency.split(cpSellValue, { pad: true });
    context.labels.totalValue = game.i18n.format("PF1.Containers.TotalValue", context.totalValue);
    context.labels.sellValue = game.i18n.format("PF1.Containers.SellValue", context.sellValue);

    // Compute encumbrance
    context.encumbrance = this._computeEncumbrance();

    // Compute Driver data
    context.driver = this._computeDriverData(context.system.driver.uuid, context.system.driver.skill);
    context.driver.driverSkill = context.system.driver.skill;
    if (!context.driver.uuid) {
      context.driver.skills = pf1.utils.deepClone(context.config.skills);
      if (context.driver.driverSkill && !context.driver.skills[context.driver.driverSkill]) {
        context.driver.skills[context.driver.driverSkill] = context.driver.driverSkill;
      }
      context.driver.skillBonus = 0;
    }

    context._editorState = pf1.applications.utils.restoreEditState(this, context);

    return context;
  }

  /**
   * @protected
   * @param {string} fullId - Target ID
   * @param {object} context - Context object to store data into
   * @throws {Error} - If provided ID is invalid.
   * @override
   * @returns {Promise<void>}
   */
  async _getTooltipContext(fullId, context) {
    /** @type {ActorPF} */
    const actor = this.actor,
      system = actor.system;

    // Lazy roll data
    const lazy = {
      get rollData() {
        this._cache ??= actor.getRollData();
        return this._cache;
      },
    };

    const getNotes = async (context, all = true) =>
      (await actor.getContextNotesParsed(context, { all, rollData: lazy.rollData, roll: false })).map((n) => n.text);

    let header, subHeader;
    const details = [];
    const paths = [];
    const sources = [];
    let notes;

    const re = /^(?<id>[\w-]+)(?:\.(?<detail>.*))?$/.exec(fullId);
    const { id, detail } = re?.groups ?? {};

    switch (id) {
      case "hp": {
        paths.push(
          { path: "@attributes.hp.max", value: lazy.rollData.attributes.hp.max },
          { path: "@attributes.hp.base", value: lazy.rollData.attributes.hp.base || 0 },
          { path: "@attributes.hp.value", value: lazy.rollData.attributes.hp.value },
          { path: "@attributes.hp.bonus", value: lazy.rollData.attributes.hp.bonus }
        );

        sources.push({ sources: actor.getSourceDetails("system.attributes.hp.max"), untyped: true });
        break;
      }
      case "ac": {
        if (!system.attributes.ac[detail]) return;

        paths.push(
          { path: "@attributes.ac.normal.total", value: lazy.rollData.attributes.ac.normal.total },
          { path: "@attributes.ac.touch.total", value: lazy.rollData.attributes.ac.touch.total }
        );
        if (lazy.rollData.traits.type === "sea") {
          paths.push({ path: "@attributes.ac.stopped.total", value: lazy.rollData.attributes.ac.stopped.total });
        }

        sources.push({
          sources: actor.getSourceDetails(`system.attributes.ac.${detail}.total`),
          untyped: true,
        });

        notes = await getNotes("ac");
        break;
      }
      case "cmb": {
        paths.push({
          path: "@attributes.cmb.total",
          value: system.attributes.cmb.total,
        });

        sources.push({
          sources: [
            {
              name: game.i18n.localize("PF1.Size"),
              value: Object.values(pf1.config.sizeSpecialMods)[system.traits.size.value],
            },
          ],
        });

        sources.push(
          { sources: actor.getSourceDetails("system.attributes.attack.general") },
          { sources: actor.getSourceDetails("system.attributes.cmb.bonus") },
          { sources: actor.getSourceDetails("system.attributes.attack.shared") }
        );

        notes = [...(await getNotes("attack")), ...(await getNotes("melee")), ...(await getNotes("cmb"))];
        break;
      }
      case "material": {
        paths.push({ path: "@material.base", value: system.material.base });
        break;
      }
      case "hardness": {
        paths.push(
          { path: "@attributes.hardness.total", value: lazy.rollData.attributes.hardness.total },
          { path: "@attributes.hardness.base", value: system.attributes.hardness.base },
          { path: "@attributes.hardness.bonus", value: lazy.rollData.attributes.hardness.bonus }
        );

        sources.push({ sources: actor.getSourceDetails("system.attributes.hardness.total"), untyped: true });
        break;
      }
      case "save": {
        if (!system.attributes.savingThrows.save) return;

        paths.push(
          {
            path: `@attributes.savingThrows.save.total`,
            value: lazy.rollData.attributes.savingThrows.save.total,
          },
          {
            path: `@attributes.savingThrows.save.base`,
            value: lazy.rollData.attributes.savingThrows.save.base,
          }
        );

        sources.push({
          sources: actor.getSourceDetails(`system.attributes.savingThrows.save.total`),
          untyped: true,
        });

        notes = await getNotes("savingThrows");
        break;
      }
      case "acceleration": {
        sources.push({ sources: actor.getSourceDetails(`system.details.acceleration`) });

        // Add base speed
        const speed = system.details.acceleration;
        const [tD] = pf1.utils.convertDistance(speed);

        const isMetricDist = pf1.utils.getDistanceSystem() === "metric";
        const tU = isMetricDist ? pf1.config.measureUnitsShort.m : pf1.config.measureUnitsShort.ft;
        paths.push({ path: `@details.acceleration`, value: tD, unit: tU });

        break;
      }
      case "currentSpeed": {
        sources.push({ sources: actor.getSourceDetails(`system.details.currentSpeed`) });

        // Add current speed
        const speed = system.details.currentSpeed;
        const speedOverland = system.details.currentSpeedOverland;
        const [tD] = pf1.utils.convertDistance(speed);

        const isMetricDist = pf1.utils.getDistanceSystem() === "metric";
        const tU = isMetricDist ? pf1.config.measureUnitsShort.m : pf1.config.measureUnitsShort.ft;
        paths.push({ path: `@details.currentSpeed`, value: tD, unit: tU });

        // Add overland speed
        const [oD] = pf1.utils.convertDistance(speedOverland.speed);
        const oU = isMetricDist ? pf1.config.measureUnitsShort.km : pf1.config.measureUnitsShort.mi;
        paths.push({ path: `@details.currentSpeedOverland`, value: oD, unit: oU });

        break;
      }
      case "maxSpeed": {
        sources.push({ sources: actor.getSourceDetails(`system.details.maxSpeed`) });

        // Add base speed
        const speed = system.details.maxSpeed;
        const speedOverland = system.details.maxSpeedOverland;
        const [tD] = pf1.utils.convertDistance(speed);

        const isMetricDist = pf1.utils.getDistanceSystem() === "metric";
        const tU = isMetricDist ? pf1.config.measureUnitsShort.m : pf1.config.measureUnitsShort.ft;
        paths.push({ path: `@details.maxSpeed`, value: tD, unit: tU });

        // Add overland speed
        const [oD] = pf1.utils.convertDistance(speedOverland.speed);
        const oU = isMetricDist ? pf1.config.measureUnitsShort.km : pf1.config.measureUnitsShort.mi;
        paths.push({ path: `@details.maxSpeedOverland`, value: oD, unit: oU });

        break;
      }
      default:
        return super._getTooltipContext(fullId, context);
    }

    context.header = header;
    context.subHeader = subHeader;
    context.details = details;
    context.paths = paths;
    context.sources = sources;
    context.notes = notes ?? [];
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   *
   * @param {JQuery} jq The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(jq) {
    super.activateListeners(jq);

    const html = jq[0];

    // Saving Throw
    html
      .querySelector(".defenses .rollable[data-dtype='saving-throws']")
      .addEventListener("click", this._onRollSavingThrow.bind(this));

    // Display defenses
    html.querySelector(".defenses .rollable[data-dtype='defenses']").addEventListener("click", (ev) => {
      ev.preventDefault();
      this.actor.displayDefenseCard({ token: this.token });
    });

    // Roll Driver skill
    html.querySelector(".driver-skill .rollable[data-dtype='driverSkill']").addEventListener("click", (ev) => {
      ev.preventDefault();
      const driver = fromUuidSync(this.actor.system.driver.uuid);
      const skill = this.actor.system.driver.skill;
      if (skill) {
        driver.rollSkill(skill, { token: this.token });
      } else {
        driver.rollAbilityTest("wis", { token: this.token });
      }
    });

    // Hook onto the Driver Select to assign a driver.
    html.querySelector(".select-driver")?.addEventListener("click", async (_ev) => {
      const targetActorId = await pf1.utils.dialog.getActor({
        actors: game.actors.filter(
          (a) =>
            ["character", "npc"].includes(a.type) &&
            a.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED)
        ),
      });

      const targetActor = game.actors.get(targetActorId);
      if (!targetActor) return;

      await this.actor.update({ "system.driver.uuid": targetActor.uuid });
    });

    // Driver controls
    html.querySelector(".driver-info .edit")?.addEventListener("click", async (_ev) => {
      fromUuidSync(this.actor.system.driver.uuid).sheet.render(true);
    });

    html.querySelector(".driver-info .delete")?.addEventListener("click", async (_ev) => {
      await this.actor.update({ "system.driver.uuid": "" });
    });
  }

  /**
   * Organize and classify Owned Items - We just need attacks
   *
   * @param {object} context The rendering context
   * @private
   * @override
   */
  _prepareItems(context) {
    const attacks = context.items.filter((i) => i.type === "attack");

    const attackSections = Object.values(pf1.config.sheetSections.combatlite)
      .map((data) => ({ ...data }))
      .sort((a, b) => a.sort - b.sort);
    for (const i of attacks) {
      const section = attackSections.find((section) => this._applySectionFilter(i, section));
      if (section) {
        section.items ??= [];
        section.items.push(i);
      } else {
        console.warn("Could not find a sheet section for", i.name);
      }
    }

    context.attacks = attackSections;

    // Categorize items as inventory, spellbook, features, and classes
    const inventory = Object.values(pf1.config.sheetSections.inventory)
      .map((data) => ({ ...data }))
      .sort((a, b) => a.sort - b.sort);

    // Alter inventory columns
    for (const section of inventory) {
      section.interface = { ...section.interface, value: true, actions: false, noEquip: true };
    }

    const items = context.items.filter((i) => i.isPhysical);

    // Organize Inventory
    for (const i of items) {
      const section = inventory.find((section) => this._applySectionFilter(i, section));
      if (section) {
        section.items ??= [];
        section.items.push(i);
      }
    }

    // Apply user filters
    const invCat = { key: "inventory", sections: inventory };
    const set = this._filters.sections.inventory ?? new Set();
    for (const section of inventory) {
      this._filterSection(invCat, section, set);
    }

    context.inventory = inventory;
  }

  /** @type {CoinType} */
  get itemValueDenomination() {
    return "gp";
  }

  _updateObject(event, formData) {
    formData = foundry.utils.expandObject(formData);

    // Convert distances back to backend imperial format
    const convertibleKeys = ["currentSpeed", "maxSpeed", "acceleration"];
    for (const key of convertibleKeys) {
      const value = formData.system.details[key];
      if (Number.isFinite(value)) {
        formData.system.details[key] = pf1.utils.convertDistanceBack(value)[0];
      }
    }

    return super._updateObject(event, formData);
  }

  _computeDriverData(driverId, skillId) {
    if (!driverId || this.actor.pack) return {};

    /** @type {ActorPF} */
    const driver = fromUuidSync(driverId);
    if (!driver) return {};

    const rollData = driver.getRollData();
    let mod = rollData.abilities.wis.mod;
    try {
      mod = driver.getSkillInfo(skillId).mod;
    } catch {
      // error silently, as we default to Wisdom Modifier
    }

    const skills = {};
    for (const skillId of driver.allSkills) {
      const skill = driver.getSkillInfo(skillId);
      skills[skillId] = skill.fullName;
    }

    return {
      name: driver.name,
      img: driver.img,
      uuid: driver.uuid,
      skills: skills,
      skillBonus: mod,
    };
  }

  /**
   * Roll saving throw
   *
   * @protected
   * @param {Event} event - The originating click event
   */
  async _onRollSavingThrow(event) {
    event.preventDefault();

    /** @type {SaveId} */
    const saveId = await foundry.applications.api.DialogV2.wait({
      window: { title: game.i18n.localize("PF1.Dialog.VehicleSave.Title") },
      content: `<p>${game.i18n.localize("PF1.Dialog.VehicleSave.Message")}</p>`,
      buttons: [
        {
          label: game.i18n.localize("PF1.SavingThrowFort"),
          action: "fort",
        },
        {
          label: game.i18n.localize("PF1.SavingThrowRef"),
          action: "ref",
        },
      ],
    });

    this.actor.rollSavingThrow(saveId, { token: this.token });
  }
}
