/**
 * No Autocomplete mixin
 *
 * Ref: https://github.com/foundryvtt/foundryvtt/issues/12008
 *
 * @todo
 * - Review continued need for this with Foundry v13
 *
 * @param {*} Base
 */
export const NoAutocomplete = (Base) =>
  class extends Base {
    /** @override */
    async _renderFrame(options) {
      const frame = await super._renderFrame(options);
      frame.autocomplete = "off";
      return frame;
    }
  };
