import { RegistryEntry } from "./base-registry.mjs";

export {};

declare module "./sources.mjs" {
  interface Source extends RegistryEntry {
    /**
     * Abbreviation
     */
    abbr?: string;
    date?: number;
    /**
     * Page count
     */
    pages?: number;
    isbn?: string;
    /**
     * @defaultValue Paizo
     */
    publisher?: string;
    /**
     * Starting level of the adventure
     */
    level?: number;
    tieIn?: string;
    url?: string;
    /** D&D 3.5 material */
    legacy?: boolean;
    type?: typeof Source.TYPES;
  }
}
