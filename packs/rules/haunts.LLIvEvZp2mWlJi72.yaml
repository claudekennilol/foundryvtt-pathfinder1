_id: LLIvEvZp2mWlJi72
_key: '!journal!LLIvEvZp2mWlJi72'
_stats:
  coreVersion: '12.331'
name: Haunts
pages:
  - _id: oYXZvJqBQgvIdKUO
    _key: '!journal.pages!LLIvEvZp2mWlJi72.oYXZvJqBQgvIdKUO'
    _stats:
      coreVersion: '12.331'
    name: Haunts
    sort: 100000
    text:
      content: >-
        <p>The distinction between a trap and an undead creature blurs when you
        introduce a haunt—a hazardous region created by unquiet spirits that
        react violently to the presence of the living. The exact conditions that
        cause a haunt to manifest vary from case to case—but haunts always arise
        from a source of terrific mental or physical anguish endured by living,
        tormented creatures. A single, source of suffering can create multiple
        haunts, or multiple sources could consolidate into a single haunt. The
        relative power of the source has little bearing on the strength of the
        resulting haunt—it’s the magnitude of the suffering or despair that
        created the haunt that decides its power. Often, undead inhabit regions
        infested with haunts—it’s even possible for a person who dies to rise as
        a ghost (or other undead) and trigger the creation of numerous haunts. A
        haunt infuses a specific area, and often multiple haunted areas exist
        within a single structure. The classic haunted house isn’t a single
        haunt, but usually a dozen or more haunted areas spread throughout the
        structure.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: WHKY9yvkhGta3TOZ
    _key: '!journal.pages!LLIvEvZp2mWlJi72.WHKY9yvkhGta3TOZ'
    _stats:
      coreVersion: '12.331'
    name: Haunt Rules
    sort: 200000
    text:
      content: >-
        <p>Although haunts function like traps, they are difficult to detect
        since they cannot be easily observed until the round in which they
        manifest. @UUID[Compendium.pf1.spells.Item.aa0w7tk852iqn3ni]{Detect
        undead} or detect alignment spells of the appropriate type (like
        @UUID[Compendium.pf1.spells.Item.tr7m97npkbgm4wp7]{Detect Evil}) allow
        an observer a chance to notice a haunt even before it manifests
        (allowing that character the appropriate check to notice the haunt, but
        at a –4 penalty).</p><p>A haunt can infuse a maximum area with a 5-foot
        radius per point of CR possessed by the haunt, but the actual area is
        usually limited by the size of the room in which the haunt is
        located.</p><p>When a haunt is triggered, its effects manifest at
        initiative rank 10 in a surprise round. All characters in the haunt’s
        proximity can attempt to notice the haunt at the start of this surprise
        round (by making a notice check). All haunts detect life sources and
        trigger as a result of the approach of or contact with living creatures,
        but some haunts can be tricked by effects like
        @UUID[Compendium.pf1.spells.Item.rekl0ikgnorz60jc]{hide from undead} or
        @UUID[Compendium.pf1.spells.Item.oylikodnyku2zewu]{invisibility}.</p><p>On
        the surprise round in which a haunt manifests, positive energy applied
        to the haunt (via channeled energy, cure spells, and the like) can
        damage the haunt’s hit points (a haunt never gains a Will save to lessen
        the damage done by such effects, and attacks that require a successful
        attack roll to work must strike AC 10 in order to affect the haunt and
        not merely the physical structure it inhabits). Unless the haunt has an
        unusual weakness, no other form of attack can reduce its hit points. If
        the haunt is reduced to 0 hit points by positive energy, it is
        neutralized— if this occurs before the haunt takes its action at
        initiative rank 10, its effect does not occur.</p><p>A haunt can have
        virtually any effect identical to an existing spell effect, but often
        with different—and distinctly more frightening or unnerving—sensory or
        physical features than that spell effect normally has. (A haunt that has
        an effect not identical to an existing spell is certainly possible, but
        this requires designing a new spell effect.) A haunt might cause a room
        to explode into flames (duplicating fireball or fire storm), infuse a
        chamber with fear (duplicating cause fear, scare, or fear), or try to
        frighten a target to death (duplicating phantasmal killer or slay
        living). How the haunt’s effects manifest are left to you to
        determine.</p><p>A neutralized haunt is not destroyed, and can manifest
        again after a period of time—to destroy a haunt, a specific action must
        be taken in the region to end the effect forever (such as burning a
        haunted house to the ground or burying the bones of the slaves who died
        on the site to create the haunt). This specific act is different for
        every haunt (although a number of nearby haunts often share the same
        destruction act).</p><p>Some haunts are persistent, and their immediate
        effects continue beyond the surprise round into actual full rounds.
        Persistent haunts continue to trigger their haunt effects once per round
        on their initiative rank until destroyed or they no longer have a
        target.</p><p>All primary effects created by a haunt are mind-affecting
        fear effects, even those that actually produce physical effects.
        Immunity to fear grants immunity to a haunt’s direct effects, but not to
        secondary effects that arise as a result of the haunt’s attack.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: Wf2ais9rkJieufdu
    _key: '!journal.pages!LLIvEvZp2mWlJi72.Wf2ais9rkJieufdu'
    _stats:
      coreVersion: '12.331'
    name: Haunt Statistics
    sort: 300000
    text:
      content: >-
        <p>Haunts are presented in the following format.</p><p><strong>Haunt
        Name</strong>: The haunt’s name is followed by its
        CR.</p><p><strong>XP</strong>: This is the amount of XP to award the PCs
        for surviving the haunt, as determined by its
        CR.</p><p><strong>Alignment and Area</strong>: This line gives the
        haunt’s alignment and the dimensions of the area it infuses (up to 5
        feet per CR). If a haunt is persistent, this is noted here as
        well.</p><p><strong>Caster Level</strong>: This is the haunt’s effective
        caster level for the purposes of dispelling any ongoing effects with
        dispel magic, and for determining the results of spell effects it
        creates.</p><p><strong>Notice</strong>: This indicates the skill check
        and DC required to notice the haunt in the surprise round before it
        manifests. The sensory input for what a successful check notices— such
        as a faint ghostly wailing, a smell of burning flesh, or fresh blood
        oozing from the walls—is listed in parentheses after the
        DC.</p><p><strong>hp</strong>: This lists the haunt’s effective hit
        points for the purposes of resolving positive energy damage. A haunt’s
        hit points are equal to twice its CR, except in the case of a persistent
        haunt, in which case its hit points are equal to its CR × 4.5 (round
        fractions down).</p><p><strong>Weakness</strong>: Any weaknesses the
        haunt might have, such as for haunts that can be tricked by effects like
        hide from undead or can be damaged by effects other than positive
        energy, are listed here.</p><p><strong>Trigger</strong>: The conditions
        that can cause the haunt to manifest are given here. Proximity-triggered
        haunts occur as soon as a creature enters the haunt’s area. A haunt
        triggered by touch does not activate until a living creature touches a
        specific object or location in its area, but it can sense (and thus
        target with its effects) any creature in its
        area.</p><p><strong>Reset</strong>: This is the amount of time that must
        pass before a haunt can attempt to reset. Until it is destroyed, a haunt
        can reset after this period by succeeding on a DC 10 caster level
        check—failure indicates the haunt must wait that amount of time again
        before making another attempt to reset.</p><p><strong>Effect</strong>:
        This details the haunt’s exact effects, including a description of how
        the haunt manifests.</p><p><strong>Destruction</strong>: This describes
        the act needed to permanently destroy the haunt.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: rrA11eyrC0HPrgwr
    _key: '!journal.pages!LLIvEvZp2mWlJi72.rrA11eyrC0HPrgwr'
    _stats:
      coreVersion: '12.331'
    name: Creating a Haunt
    sort: 400000
    text:
      content: >-
        <p>To make a haunt like the example below, follow these
        steps.</p><p><strong>Step 1—Determine Base CR</strong>: A haunt’s base
        CR is equal to 1 + the level of the spell it
        duplicates.</p><p><strong>Step 2—Determine Actual CR</strong>: Select
        the elements you want the haunt to have and add up the adjustments to
        its CR to arrive at the haunt’s final CR (see CR Modifiers for
        Haunts).</p><p><strong>Step 3—Determine Caster Level</strong>: A haunt’s
        caster level is equal to its actual CR score.</p><p><strong>Step
        4—Determine Hit Points</strong>: A haunt’s hit points are equal to twice
        its CR (or equal to its CR × 4.5 if the haunt is
        persistent).</p><p><strong>Step 5—Calculate Attacks and Save
        DCs</strong>: A haunt’s attack modifier (if one is needed) is equal to
        its CR. If a haunt’s spell effect allows a saving throw to resist or
        negate the effect, the save DC is equal to 10 + the level of the spell +
        the ability modifier of the minimum ability score needed to cast that
        level of spell.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: wFG8C4cGgb0WYwIh
    _key: '!journal.pages!LLIvEvZp2mWlJi72.wFG8C4cGgb0WYwIh'
    _stats:
      coreVersion: '12.331'
    name: CR Modifiers for Haunts
    sort: 500000
    text:
      content: >-
        <h2>CR Modifiers of Haunts</h2><table><tbody><tr><td><p><strong>Feature
        Type</strong></p></td><td><p><strong>CR
        Modifier</strong></p></td></tr><tr><td><p>Persistent</p></td><td><p>+2</p></td></tr><tr><td><p><strong>Notice
        DC</strong></p></td><td><p><strong>CR
        Modifier</strong></p></td></tr><tr><td><p>15 or
        lower</p></td><td><p>-1</p></td></tr><tr><td><p>16-20</p></td><td><p>-</p></td></tr><tr><td><p>21-25</p></td><td><p>+1</p></td></tr><tr><td><p>26-29</p></td><td><p>+2</p></td></tr><tr><td><p>30
        or higher</p></td><td><p>+3</p></td></tr><tr><td><p><strong>Reset
        Time</strong></p></td><td><p><strong>CR
        Modifier</strong></p></td></tr><tr><td><p>1
        minute</p></td><td><p>+2</p></td></tr><tr><td><p>1
        hour</p></td><td><p>+1</p></td></tr><tr><td><p>1
        day</p></td><td><p>+0</p></td></tr><tr><td><p>1
        week</p></td><td><p>-1</p></td></tr><tr><td><p><strong>Example
        Weaknesses</strong></p></td><td><p><strong>CR
        Modifier</strong></p></td></tr><tr><td><p>Slow (manifests at Initiative
        rank 0)</p></td><td><p>-2</p></td></tr><tr><td><p>Susceptible to an
        additional type of damage type</p></td><td><p>-1 per additional
        type</p></td></tr><tr><td><p>Tricked by
        @UUID[Compendium.pf1.spells.Item.rekl0ikgnorz60jc]{hide from
        undead}</p></td><td><p>-2</p></td></tr><tr><td><p>Tricked by
        @UUID[Compendium.pf1.spells.Item.oylikodnyku2zewu]{invisibility}</p></td><td><p>-1</p></td></tr><tr><td><p>Tricked
        by Stealth*</p></td><td><p>-3</p></td></tr><tr><td><p>Triggered by
        touch</p></td><td><p>-2</p></td></tr></tbody></table><p>* The haunt
        makes a caster level check instead of a Perception check to notice
        someone using Stealth.</p><hr /><p>While haunts can be complex
        antagonists, they are versatile tools that are well suited to portray
        the drama and atmosphere of occult games. This section presents new
        haunt rules and clarifications on previous rules. <br
        /></p><h2>Additional Haunt
        Elements</h2><table><tbody><tr><td><p><strong>Type</strong></p></td><td><p><strong>CR
        Modifier</strong></p></td></tr><tr><td><p>Belligerent (hit points equal
        to CR × 6 )</p></td><td><p>+3</p></td></tr><tr><td><p>Item-bound (bound
        to item)</p></td><td><p>-1</p></td></tr><tr><td><p>Chained (bound to
        ghost)</p></td><td><p>-1</p></td></tr><tr><td><p>Fast (manifests on
        initiative rank
        20)</p></td><td><p>+2</p></td></tr><tr><td><p>Free-roaming (gains
        movement speed: fly 10 ft.
        [good])</p></td><td><p>+1</p></td></tr><tr><td><p>Increased area (double
        radius to 10 ft. per
        CR)</p></td><td><p>+1</p></td></tr><tr><td><p>Possessing (bound to
        creature)</p></td><td><p>+1</p></td></tr><tr><td><p>Spiteful (caster
        level and save DCs increase by
        2)</p></td><td><p>+1</p></td></tr><tr><td><p>Vaporous (AC = 10 + CR and
        gains incorporeal
        quality)</p></td><td><p>+1</p></td></tr></tbody></table>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: ViD7x7D2AEIUp2dQ
    _key: '!journal.pages!LLIvEvZp2mWlJi72.ViD7x7D2AEIUp2dQ'
    _stats:
      coreVersion: '12.331'
    name: Beligerent
    sort: 700000
    text:
      content: <div><p>The haunt's HP is equal to 6 x CR.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: GgK76mqrJm8iPitI
    _key: '!journal.pages!LLIvEvZp2mWlJi72.GgK76mqrJm8iPitI'
    _stats:
      coreVersion: '12.331'
    name: Item-Bound
    sort: 800000
    text:
      content: >-
        <div><p>This haunt is bound to an item.</p><p>Some haunts are tied to
        special objects or creatures. Such haunts take normal damage from
        positive energy, and follow the normal reset rules for haunts of their
        type. @UUID[Compendium.pf1.spells.Item.sg3eq6xpsum65fgm]{Dispel evil}
        can eject a haunting presence if the spell is cast quickly; the caster
        must succeed at a caster level check with a DC equal to 10 + the haunt’s
        CR + 1 for each month that the creature or object has been
        possessed.</p><p>Bound haunts possess items when created, and gain
        mobility at the cost of having their tragic fates tied to physical
        objects that are more easily destroyed. These haunts spontaneously
        manifest at scenes of great terror, as the psychic residue of tragic
        events seeps into items tied to the events. Once bound to an item, an
        item-bound haunt uses all of the normal rules for haunts, with the
        radius of its effects centered on the haunted object. Some effects may
        have special triggers based on the item’s nature, such as haunted
        instruments being played or weapons being used. The haunting presence
        adds 5 to the break DC for its possessed item, and doubles the item’s
        hardness and hit points.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: xRk4w3d0k9a3XfoR
    _key: '!journal.pages!LLIvEvZp2mWlJi72.xRk4w3d0k9a3XfoR'
    _stats:
      coreVersion: '12.331'
    name: Chained
    sort: 900000
    text:
      content: >-
        <div><p>This haunt is bound to a ghost.</p><p>Some haunts are
        intrinsically connected with incorporeal undead entities (most commonly
        ghosts) and manifest as displays of the associated creatures’ fractured
        psyches. Chained haunts can be destroyed only by bringing final rest to
        their connected entities. Chained haunts can be used to illustrate and
        emphasize a ghost’s tragic story. For example, a series of chained
        haunts could be spread across the site of a ruined mansion: while the
        linked creature—a ghost— dwells in the attic where it was murdered, a
        downstairs bedroom might manifest a bleeding walls haunt to emphasize
        the scene of a tragic loss pertinent to the ghost’s history; a demanding
        dead haunt might cause a trespasser to dig up a shallow grave in the
        garden where the ghost’s corpse is buried; and the murder weapon might
        roam the halls of the mansion, manifesting as a malignant weapon
        haunt.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: FeZDtjPSyxxqtvSP
    _key: '!journal.pages!LLIvEvZp2mWlJi72.FeZDtjPSyxxqtvSP'
    _stats:
      coreVersion: '12.331'
    name: Elusive
    sort: 1000000
    text:
      content: >-
        <div><p><span id="MainContent_DetailedOutput">A typical haunt can be
        harmed within the area of its manifestation, but an elusive haunt’s
        source is in a separate location. An elusive haunt can be damaged only
        at its source, and can manifest far away from that source, up to 100
        feet per point of CR. The elusive element typically increases a haunt’s
        CR by 1. If the haunt is also persistent, the elusive property increases
        the haunt’s CR by 2.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: gOUBR4eNOqKZrQbe
    _key: '!journal.pages!LLIvEvZp2mWlJi72.gOUBR4eNOqKZrQbe'
    _stats:
      coreVersion: '12.331'
    name: Fast
    sort: 1100000
    text:
      content: >-
        <div><p>This haunt manifests at initiative rank 20, instead of
        10.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: 70TeCwEEWNTqet9j
    _key: '!journal.pages!LLIvEvZp2mWlJi72.70TeCwEEWNTqet9j'
    _stats:
      coreVersion: '12.331'
    name: Free-roaming
    sort: 1200000
    text:
      content: >-
        <div><p>This haunt gains the ability to move with a fly speed of 10 feet
        and good maneuverability.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: 7bujYFyTIngbWcC7
    _key: '!journal.pages!LLIvEvZp2mWlJi72.7bujYFyTIngbWcC7'
    _stats:
      coreVersion: '12.331'
    name: Increased Area
    sort: 1300000
    text:
      content: >-
        <div><p>The haunt's area doubles, normally becoming up to 10 feet per
        CR.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: LBvgVviZKigP49TT
    _key: '!journal.pages!LLIvEvZp2mWlJi72.LBvgVviZKigP49TT'
    _stats:
      coreVersion: '12.331'
    name: Latent
    sort: 1400000
    text:
      content: >-
        <div><p><span id="MainContent_DetailedOutput">A latent haunt’s effects
        are subtle and come into effect only if a creature who fails a save
        against the haunt fulfills a particular condition, such as visiting a
        certain location or performing a specific action. For example, a latent
        haunt may rest among the graves of victims of a serial killer, and only
        demonstrate its effects if the affected creature enters the killer’s
        manor. Latent haunts affecting a creature treat that creature as their
        source, and can be detected and damaged by any means that would detect
        or damage the haunt. A latent haunt works best if the DC of the skill
        check to notice it is unusually high for its CR.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: UJplPnsAXORwc0wG
    _key: '!journal.pages!LLIvEvZp2mWlJi72.UJplPnsAXORwc0wG'
    _stats:
      coreVersion: '12.331'
    name: Persistent
    sort: 1500000
    text:
      content: >-
        <div><p><span id="MainContent_DetailedOutput">Some haunts are
        persistent, and their immediate effects continue beyond the surprise
        round into actual full rounds. Persistent haunts continue to trigger
        their haunt effects once per round on their initiative rank until
        destroyed or they no longer have a target.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: POoYy22lWnkNtlrG
    _key: '!journal.pages!LLIvEvZp2mWlJi72.POoYy22lWnkNtlrG'
    _stats:
      coreVersion: '12.331'
    name: Possessing
    sort: 1600000
    text:
      content: >-
        <div><p>This haunt is bound to a creature.</p><p><span
        id="MainContent_DetailedOutput">Malevolent spirits may similarly haunt
        creatures rather than items, following the subjects wherever they go and
        causing strange occurrences and poltergeist-like activity around the
        subject in revenge for a perceived trespass or involvement in the tragic
        events that created the haunt. While they sometimes seem beneficial to
        their hosts at first, such haunts inevitably seek their hosts’
        destruction. Individuals possessed by such haunts must always take a
        standard action to retrieve stored items, unless it would normally take
        longer. In addition, any item the host drops lands 10 feet away in a
        random direction. A possessing haunt uses all normal rules for haunts,
        with the radius of its effects centered on the haunted subject, who
        takes a –2 penalty on all saving throws against the haunt’s effects.
        Subject to the GM’s discretion, haunted creatures may suffer tormenting
        dreams that cause 1 point of drain each day to an ability score
        appropriate for the haunt.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: bt61t3hOagcnoXzF
    _key: '!journal.pages!LLIvEvZp2mWlJi72.bt61t3hOagcnoXzF'
    _stats:
      coreVersion: '12.331'
    name: Spiteful
    sort: 1700000
    text:
      content: <div><p>The haunt's caster level and save DCs increase by 2.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: NQsW2HGccyOeVFt0
    _key: '!journal.pages!LLIvEvZp2mWlJi72.NQsW2HGccyOeVFt0'
    _stats:
      coreVersion: '12.331'
    name: Tenacious
    sort: 1800000
    text:
      content: >-
        <div><p><span id="MainContent_DetailedOutput">A tenacious haunt clings
        desperately to its existence. When the haunt is required to attempt a
        saving throw, instead of automatically failing, it can attempt a saving
        throw with a bonus equal to 2 + its CR.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: F9mLugVUBaIUtU0g
    _key: '!journal.pages!LLIvEvZp2mWlJi72.F9mLugVUBaIUtU0g'
    _stats:
      coreVersion: '12.331'
    name: Unyielding
    sort: 1900000
    text:
      content: >-
        <div><p><span id="MainContent_DetailedOutput">An unyielding haunt has
        all of the properties of a tenacious haunt, except when it succeeds at a
        saving throw against a spell or effect that would normally deal it
        damage, it instead takes no damage.</span></p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: eEur8JYjiDBCcpv6
    _key: '!journal.pages!LLIvEvZp2mWlJi72.eEur8JYjiDBCcpv6'
    _stats:
      coreVersion: '12.331'
    name: Vaporous
    sort: 2000000
    text:
      content: >-
        <div><p>The haunt's AC becomes 10 + its CR, and it gains the incorporeal
        quality.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: wo4WlQIj0hd4zYgn
    _key: '!journal.pages!LLIvEvZp2mWlJi72.wo4WlQIj0hd4zYgn'
    _stats:
      coreVersion: '12.331'
    name: Variant
    sort: 2100000
    text:
      content: >-
        <div><p>Haunts that are not tied to undeath are not vulnerable to
        positive energy, and cannot be detected with spells such as
        @UUID[Compendium.pf1.spells.Item.aa0w7tk852iqn3ni]{detect undead} or
        evaded with spells such as
        @UUID[Compendium.pf1.spells.Item.rekl0ikgnorz60jc]{hide from undead}.
        They have their own sets of vulnerabilities and defenses.</p></div>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
