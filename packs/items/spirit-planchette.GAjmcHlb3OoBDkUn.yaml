_id: GAjmcHlb3OoBDkUn
_key: '!items!GAjmcHlb3OoBDkUn'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/items/inventory/dice.jpg
name: Spirit planchette
system:
  aura:
    school: div
  cl: 9
  description:
    value: >-
      <p><strong>Slot</strong> none; <strong>Price</strong> 4,000 gp (Brass),
      10,000 gp (Cold Iron), 18,000 gp (Silver)</p><h3>Description</h3><p>A
      spirit planchette is typically found in a wooden case along with a thin
      wooden board printed with numerous letters and numbers. Nonmagical
      versions of these divination tools can be purchased in curiosity shops
      (typically costing 25 gp); while these items can be used as alternative
      components for augury spells, only magical spirit planchettes allow users
      to communicate with the other side. Three types of spirit planchettes
      exist — brass, cold iron, and silver. Each in turn allows an increasingly
      potent form of divination effect to be utilized. A spirit planchette
      requires a board to move upon, but this "board" can be made up of letters
      scribed upon any smooth surface — it need not be a prepared board for a
      spirit planchette to work.</p><p>To use a spirit planchette, you must rest
      your fingers lightly upon the planchette's surface and then concentrate on
      the planchette (as if maintaining a spell with a duration of
      concentration) for 2d6 rounds while the planchette attunes itself to the
      ambient spirits of the area. After this time, the planchette begins to
      slowly slide in random patterns across the board — at this point,
      questions may be asked of the spirits by any of the individuals involved
      in the séance. The consequences of each question asked of the spirits
      depends upon what type of planchette is used for the divination, as
      summarized on the table below. The spirits reply in a language understood
      by the character who asked the question, but resent such contact and give
      only brief answers to the questions. All questions are answered with
      "yes," "no," or "maybe," or by spelling out a single word from the letters
      arranged on the board. The spirits answer each question either in the same
      round the question is asked (in the case of a yes, no, or maybe answer) or
      at a rate of one letter per round (in the case of a single word being
      spelled out). A spirit planchette may be used once per day — the maximum
      number of questions you can ask with it depends on the type of planchette
      being used (as detailed on the table below).</p><p>Communication with
      spirits can be a dangerous task, for many spirits are jealous or hateful
      of the living. Every time a spirit planchette is used, the user must
      succeed on a Will save to avoid being temporarily possessed and harmed by
      the angry spirits. In some areas where the spirits are particularly
      violent or hateful (such as in Harrowstone), this Will save takes a -2
      penalty. The DC of this save depends on the type of spirit planchette
      being used. Anyone who fails the Will save becomes confused for a number
      of rounds (depending on the type of planchette being used), and no answer
      is received. The spirits in the area are not omniscient — the GM should
      decide whether or not the spirits would actually know the answer to the
      question asked, and if they do not, the answer granted is automatically
      "maybe." If the GM determines that the spirits are knowledgeable about the
      answer, roll d% to determine whether the spirits speak truthfully or
      whether they lie.</p><table><tbody><tr><td><p>Planchette
      Type</p></td><td><p>Questions per Use</p></td><td><p>Will Save
      DC</p></td><td><p>Confusion Duration</p></td><td><p>True
      Answer</p></td><td><p>Lie</p></td></tr><tr><td><p>Brass</p></td><td><p>1</p></td><td><p>11</p></td><td><p>1
      round</p></td><td><p>01-60</p></td><td><p>61-100</p></td></tr><tr><td><p>Cold
      Iron</p></td><td><p>3</p></td><td><p>15</p></td><td><p>2
      rounds</p></td><td><p>01-75</p></td><td><p>76-100</p></td></tr><tr><td><p>Silver</p></td><td><p>5</p></td><td><p>19</p></td><td><p>3
      rounds</p></td><td><p>01-90</p></td><td><p>91-100</p></td></tr></tbody></table><h3>Construction</h3><p><strong>Requirements</strong>
      Craft Wondrous Item, contact other planes, speak with dead;
      <strong>Cost</strong> 2,000 gp (brass spirit planchette), 5,000 gp (cold
      iron spirit planchette), 9,000 gp (silver spirit planchette)</p>
  equipped: false
  hp:
    base: 10
  price: 4000
  sources:
    - id: PZO9043
      pages: '19'
  subType: gear
  weight:
    value: 5
type: loot
