_id: 2sa3881har33geo1
_key: '!items!2sa3881har33geo1'
_stats:
  coreVersion: '12.331'
folder: EmIafjKl7hvbinLh
img: systems/pf1/icons/misc/magic-swirl.png
name: Polymorph Any Object
system:
  actions:
    - _id: q8pisy864re2h6b1
      actionType: spellsave
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        units: seeText
      name: Use
      range:
        units: close
      save:
        description: Fortitude negates (object); see text
        type: fort
      target:
        value: one creature, or one nonmagical object of up to 100 cu. ft./level
  components:
    divineFocus: 2
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>This spell functions like
      @UUID[Compendium.pf1.spells.t44p3oeq43jwcgwg]{Greater Polymorph}<em>,</em>
      except that it changes one object or creature into another. You can use
      this spell to transform all manner of objects and creatures into new
      forms-you aren't limited to transforming a living creature into another
      living form. The duration of the spell depends on how radical a change is
      made from the original state to its transmuted state. The duration is
      determined by using the following guidelines.</p>

      <table>

      <tbody>

      <tr>

      <th>Changed Subject is...</th>

      <th>Increase to duration Factor*</th>

      </tr>

      <tr>

      <td>Same kingdom (animal, vegetable, mineral)</td>

      <td>+5</td>

      </tr>

      <tr>

      <td>Same class (mammals, fungi, metals, etc.)</td>

      <td>+2</td>

      </tr>

      <tr>

      <td>Same size</td>

      <td>+2</td>

      </tr>

      <tr>

      <td>Related (twig is to tree, wolf fur is to wolf, etc.)</td>

      <td>+2</td>

      </tr>

      <tr>

      <td>Same or lower Intelligence</td>

      <td>+2</td>

      </tr>

      <tr>

      <td colspan="2">*Add all that apply. Look up the total on the next
      table.</td>

      </tr>

      </tbody>

      </table>

      <table>

      <tbody>

      <tr>

      <th>Duration Factor</th>

      <th>Duration</th>

      <th>Example</th>

      </tr>

      <tr>

      <td>0</td>

      <td>20 minutes</td>

      <td>Pebble to human</td>

      </tr>

      <tr>

      <td>2</td>

      <td>1 hour</td>

      <td>Marionette to human</td>

      </tr>

      <tr>

      <td>4</td>

      <td>3 hours</td>

      <td>Human to marionette</td>

      </tr>

      <tr>

      <td>5</td>

      <td>12 hours</td>

      <td>Lizard to manticore</td>

      </tr>

      <tr>

      <td>6</td>

      <td>2 days</td>

      <td>Sheep to wool coat</td>

      </tr>

      <tr>

      <td>7</td>

      <td>1 week</td>

      <td>Shrew to manticore</td>

      </tr>

      <tr>

      <td>9+</td>

      <td>Permanent</td>

      <td>Manticore to shrew</td>

      </tr>

      </tbody>

      </table>

      <p>If the target of the spell does not have physical ability scores
      (Strength, Dexterity, or Constitution), this spell grants a base score of
      10 to each missing ability score. If the target of the spell does not have
      mental ability scores (Intelligence, Wisdom, or Charisma), this spell
      grants a score of 5 to such scores. Damage taken by the new form can
      result in the injury or death of the polymorphed creature. In general,
      damage occurs when the new form is changed through physical force. A
      nonmagical object cannot be made into a magic item with this spell. Magic
      items aren't affected by this spell.</p>

      <p>This spell cannot create material of great intrinsic value, such as
      copper, silver, gems, silk, gold, platinum, mithral, or adamantine. It
      also cannot reproduce the special properties of cold iron in order to
      overcome the damage reduction of certain creatures.</p>

      <p>This spell can also be used to duplicate the effects of baleful
      polymorph, <em>greater polymorph,</em> flesh to stone, stone to flesh,
      transmute mud to rock, transmute metal to wood, or transmute rock to
      mud.</p>
  learnedAt:
    bloodline:
      Impossible: 8
      Protean: 8
    class:
      arcanist: 8
      sorcerer: 8
      wizard: 8
    subDomain:
      Construct: 8
  level: 8
  materials:
    value: mercury, gum arabic, and smoke
  school: trs
  sources:
    - id: PZO1110
      pages: '323'
  subschool:
    - polymorph
type: spell
